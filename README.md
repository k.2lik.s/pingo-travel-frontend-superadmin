# Pingo.travel
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

## WEB

Стек основных веб-технологий в проекте и использованные open-source решения

- [React](https://ru.reactjs.org/) 
- [Redux](https://redux.js.org/) 
- [Immer](https://immerjs.github.io/immer/docs/introduction)
- [Grommet](https://v2.grommet.io/) 


Организация стуктуры проекта по фичам (и сущностям предметной области). На примере того же приложения выглядит так:

```
src
├── modules
│   ├── auth
│   │   ├── actions.js
│   │   ├── api.js
│   │   ├── config.js
│   │   ├── constants.js
│   │   ├── reducer.js
│   │   └── utils/
│       └── pages/
│   └── purchases
│       ├── actions.js
│       ├── api.js
│       ├── config.js
│       ├── constants.js
│       ├── reducer.js
│       └── utils/
│       └── pages/


    
```

При таком подходе любые технические аспекты, относящиеся к одной фиче, лежат рядом друг с другом и их не нужно искать по всему приложению. Сами фичи становятся изолированными модулями, предоставляющими наружу какой-то API, при этом детали их реализации и конкретные используемые библиотеки остаются под капотом.

