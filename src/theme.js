import { FormCheckmark } from "grommet-icons";

export const theme = {
  global: {
    font: {
      family: "open_sansregular",
      style: "normal",
      weight: "normal",
    },
    colors: {
      brand: "#040849",
      secondary: "#0099FF",
      "blue-1": "#2F80ED",
      selected:"#2F80ED"
    },
    focus: {
      border: {
        color: "transparent",
      },
    },
  },
  heading: {
    level: {
      2: {
        small: {
          size: "24px",
          height: "32px",
          color: "#333333",
          font: {
            weight: "600",
          },
        },
        medium: {
          size: "32px",
          height: "40px",
          color: "#333333",
          font: {
            weight: "600",
          },
        },
        large: {
          size: "40px",
          height: "48px",
          color: "#333333",
          font: {
            weight: "bold",
          },
        },
      },
    },
  },
  select: {
    control: {
      extend: "border:1px solid #2F80ED; border-radius:8px; color:#2F80ED",
    },
    icons: {
      color: "#2F80ED",
    },
  },
  button: {
    extend: `border-radius: 8px; font-size: 18px;
    line-height: 24px; font-weight: bold;
    `,

    border: {
      radius: undefined,
      color: "#2F80ED",
    },
    primary: {
      color: "#2F80ED",

      extend: `height:56px;`,
    },
  },
  text: {
    small: {
      size: "16px",
      height: "24px",
      color: "#828282",
    },
  },
  calendar: {
    large: {
      daySize: "60px",
      fontSize: "20px",
      lineHeight: "32px",
    },
    day: {
      extend: ({ isSelected }) => `
        font-size: 20px;
line-height: 32px;
         background-color: ${isSelected ? "#2F80ED" : undefined}`,
    },
  },
  formField: {
    label: {
      color: "#333333",
      size: "18px",
      margin: { vertical: "0", bottom: "xsmall", left: "0" },
      weight: 600,
    },
    border: false,
    margin: "0",
  },
  table: {
    body: {
      extend: () => `border:1px solid #F2F2F2;  color: #333333;`,
      pad: "medium",
    },
    extend: () => `border:1px solid #F2F2F2; color: #333333;`,

    header: {
      pad: "medium",
      extend: () => `border:1px solid #F2F2F2; background:#fff; font-weight:bold; text-transform:uppercase; text-align:left; font-size: 18px;
      line-height: 24px;       cursor:default;
      `,
    },
  },
  checkBox: {
    border: {
      color: {
        light: "#BEBEBE",
      },
    },
    check: {
      extend: ({ theme, checked }) => `
        ${checked && `background-color: #2F80ED;`}
        `,
    },
    color: {
      light: "neutral-3",
      dark: "neutral-3",
    },
    hover: {
      border: {
        color: undefined,
      },
    },
    icon: {
      size: "18px",
      extend: "stroke: white;",
    },
    icons: {
      checked: FormCheckmark,
      indeterminate: FormCheckmark,
    },
    size: "18px",
  },
};
