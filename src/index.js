import React, { Suspense } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { AuthProvider } from "modules/auth/AuthProvider";
import reportWebVitals from "./reportWebVitals";
import { Grommet } from "grommet";
import ScrollToTop from "modules/common/components/ScrollToTop";
import Home from "./home";
import { theme } from "./theme";
import store from "./store";

import "draft-js/dist/Draft.css";
import "./index.scss";
import './i18n';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Grommet theme={theme} >
        <AuthProvider>
          <BrowserRouter>
            <ScrollToTop>
              <Suspense fallback="loading">
                <Home />
              </Suspense>
            </ScrollToTop>
          </BrowserRouter>
        </AuthProvider>
      </Grommet>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
