import axios from "axios";
import { API } from "./urls";

const { CancelToken } = axios;
const source = CancelToken.source();
const options = {};

const axiosInstance = axios.create({
  baseURL: API,
  timeout: 15000,
  headers: {
    credentials: "same-origin",
    "Accept-Language": "ru-RU",
  },
  ...options,
  cancelToken: source.token,
});

axiosInstance.interceptors.request.use((config) => {
  const token = sessionStorage.getItem("token");
  config.headers.Authorization = `Bearer ${token}`;
  return config;
});

export default axiosInstance;
