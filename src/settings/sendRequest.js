import axiosInstance from "./axios";
import { API } from './urls';

export default async function sendRequest(path, options = {}) {
  const headers = {
    ...(options.headers || {}),
    "Content-type": "application/json; charset=UTF-8",
    "x-api-key": "25eIv2VqttcQi6SnC4f0BedOCDjNxzzrYORNc21G4N2mHfi7WMH972V8QaTrxdem",
    "Accept-Language": "ru-RU"
  };

  return axiosInstance({
    method: "get",
    url: API + path,
    ...options,
    headers,
  })

}
