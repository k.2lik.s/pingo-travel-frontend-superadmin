import { useState } from "react";
import { login } from "modules/auth/api";
import {
  Box,
  Button,
  Heading,
  TextInput,
} from "grommet";
import {  Secure } from "grommet-icons";

const Auth = () => {
  const [form, setForm] = useState({
    email: "",
    password: "",
  });
  const [submiting, setSubmiting] = useState(false);

  const handleChange = (event) => {
    setForm({
      ...form,
      [event.target.name]: event.target.value,
    });
  };

  const handleSubmit = () => {
    setSubmiting(true)
    login(form).then(({ data }) => {
      sessionStorage.setItem("token", data.access_token);
      window.location = "/";
    }).catch(error => {
      setSubmiting(false)
      alert(error)

    });
  };
  return (
    <Box direction="row"  height={{ min: "100vh" }} justify="between" >
      <Box background="brand-2" elevation="medium" basis="30%"   >
        <Box margin={{horizontal: "medium", vertical: "large"}}>
          <Heading textAlign="center" level="3">
            <Secure size="large" color="#2F80ED" />
            <br />
            Добро пожаловать!

          </Heading>

          <form
            onSubmit={(event) => {
              event.preventDefault();
              handleSubmit();
            }}
          >
            <TextInput
              name="email"
              type="email"
              autocomplete="email"
              placeholder="Email"
              value={form.email}
              onChange={handleChange}
            />
            <Box margin={{ top: "small" }} />

            <TextInput
              name="password"
              type="password"
              autocomplete="password"
              placeholder="Пароль"
              value={form.password}
              onChange={handleChange}
            />

            <br />
            <Box align="center" justify="center">
              <Button
                size="large"
                disabled={submiting}
                fill
                primary
                type="submit"
                label="Войти"
              ></Button>
            </Box>
          </form>
        </Box>
      </Box>
      <Box
        basis="full"
   
        background={{
          image:
            "url(//source.unsplash.com/1600x900/?hotel,tourism,trip,cruise)",
          size: "cover",
          position: "center",
        }}
      ></Box>
    </Box>
  );
};

export default Auth;
