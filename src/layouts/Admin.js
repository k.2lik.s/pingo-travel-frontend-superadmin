import { useState, useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import {useLocation, Route, useHistory, useRouteMatch} from "react-router-dom";
import { useTranslation } from "react-i18next";
import Cookies from 'js-cookie';
import styled from "styled-components";
import {
  Box,
  Grid,
  TextInput,
  Stack,
  Text,
  Select,
  Avatar,
  Button,
} from "grommet";
import { Hide, View } from "grommet-icons";
import { useAuthState } from "modules/auth/AuthProvider";
import { updateProfile } from "modules/auth/api";
import Modal from "ui/Common/Modal";
import DefaultAvatar from "../ui/Common/pingoDefaultAva.png";
import {
  HeaderBalanceIcon,
  NotificationIcon,
} from "ui/Common/Icon";
import useModal from "ui/Common/Modal/useModal";
import { languages } from "./mock";

import Sidebar from "ui/Sidebar";

import { adminRoutes } from "../routes";
import {format} from "date-fns";

const RouteWithSubRoutes = (route) => {
  return (
    <Route
      path={route.layout + route.path}
      exact={route.exact}
      render={(props) => <route.component {...props} routes={route.routes} />}
    />
  );
};

export const RenderRoutes = ({ routes }) => {
  return routes.map((route, key) => {
    return <RouteWithSubRoutes key={key} {...route} />;
  });
};

const AdminLayout = () => {
  // const { t } = useTranslation('common');
  const { i18n, ready } = useTranslation('');
  const { user } = useAuthState();
  const history = useHistory();

  const dispatch = useDispatch();
  const location = useLocation();
  let { path, url } = useRouteMatch();

  const [activeLang, setActiveLang] = useState(i18n.language);
  const [surname, setSurname] = useState(user?.surname);
  const [name, setName] = useState(user?.name);

  const [oldPassword, setOldPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [repeatNewPassword, setRepeatNewPassword] = useState('');
  const [revealOld, setRevealOld] = useState(false);
  const [revealNew, setRevealNew] = useState(false);
  const [revealRep, setRevealRep] = useState(false);
  const [toggleProfileModal, setToggleProfileModal] = useState(true);
  const [search, setSearch] = useState(null);

  const [isShowing, toggle] = useModal();
  const [isShowingNotification, toggleNotification] = useModal();
  const HideProfileModal = () => {
    toggle();
    setToggleProfileModal(true);
  };


  useEffect(() => {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
  }, [location]);

  const handleSubmitProfile = (event) => {
    event.preventDefault();
    updateProfile({
      name: name,
      surname: surname,
      oldPassword: oldPassword,
      newPassword: newPassword,
      repeatNewPassword: repeatNewPassword,
    })
        .then(() => {
          toggle();
          setName(name);
          setSurname(surname);
        })
        .catch((err) => {
          console.info(err);
        });
  };


  useEffect(() => {
    if (ready) {
      Cookies.set('i18next', i18n.language)
    }
  }, [ready, i18n.language])

  return (
      <>
        <Grid
            areas={[
              { name: "nav", start: [0, 0], end: [0, 0] },
              { name: "main", start: [1, 0], end: [1, 0] },
            ]}
            columns={["320px", "auto"]}
            rows={["flex"]}
        >
          <SideBarBox
              gridArea="nav"
              background="brand"
              animation={[
                { type: "fadeIn", duration: 300 },
                { type: "slideRight", size: "xlarge", duration: 300 },
              ]}
          >
            <Sidebar routes={adminRoutes}/>
          </SideBarBox>

          <Box
              gridArea="main"
              background="#FAFAFA"
              animation={[{ type: "fadeIn", duration: 300 }]}
          >
            <HeaderBox
                justify="end"
                background="white"
                gridArea="header"
                align="center"
                direction="row"
                height="100px"
                border={{ color: "#E0E0E0", side: "bottom" }}
            >
              <Box width="xsmall">
                <Select
                    id="select"
                    name="select"
                    value={activeLang}
                    options={languages}
                    onChange={(lang) => {
                      i18n.changeLanguage(lang.value).then(() => Cookies.set('i18next', lang.value));
                      setActiveLang(lang.value);
                    }}
                />
              </Box>
              <Box width="300px" margin={{left: '40px'}} onClick={toggle} align="center" justify="center" height="100%" border={{style: 'solid', side: 'left', color: '#E0E0E0'}}>
                <Box direction="row" align="center">
                  <Box pad={{right: '12px'}}>
                    <Text size="small">
                      {name} {surname}
                    </Text>
                    <Text size="xsmall" textAlign="end">{user.email}</Text>
                  </Box>
                  <Box>
                    {user?.avatar
                      ? <Avatar size="medium" src={user?.avatar} round="small" />
                      : <Avatar size="medium" src={DefaultAvatar} round="small" />
                    }
                  </Box>
                </Box>
              </Box>
            </HeaderBox>

            <Box margin="medium" height={{ min: "100vh" }}>
              <RenderRoutes routes={adminRoutes} />
            </Box>
          </Box>
        </Grid>
        <Modal
            isShowing={isShowing}
            hide={HideProfileModal}
            background="#000"
            opacity=".5"
            width={'480px'}
            margin="4.75rem auto"
        >
          <Box>
            {toggleProfileModal
                  ? <>
                    <Box>
                      <ModalTitle>
                        Профиль
                      </ModalTitle>
                    </Box>
                    <Box align="center">
                      <ModalAvatar size="192px" src={DefaultAvatar} round="small" />
                    </Box>
                    <ModalInputBlock>
                      <Text size="medium">

                      </Text>
                      <ModalFieldTitle>
                        <b>Фамилия</b>
                        <Text color="red">*</Text>
                      </ModalFieldTitle>
                      <TextInput
                          type="text"
                          placeholder="Напишите фамилию"
                          value={surname}
                          onChange={(e) => setSurname(e.target.value)}
                      />
                    </ModalInputBlock>
                    <ModalInputBlock>
                      <Text size="medium">

                      </Text>
                      <ModalFieldTitle>
                        <b>Имя</b>
                        <Text color="red">*</Text>
                      </ModalFieldTitle>
                      <TextInput
                          type="text"
                          placeholder="Напишите имя"
                          value={name}
                          onChange={(e) => setName(e.target.value)}
                      />
                    </ModalInputBlock>
                    <ModalInputBlock>
                      <Text size="medium">

                      </Text>
                      <ModalFieldTitle>
                        <b>Почта</b>
                      </ModalFieldTitle>
                      <TextInput
                          type="text"
                          value={user.email}
                          disabled
                      />
                    </ModalInputBlock>
                    <ToChangePasswordButton
                      onClick={() => setToggleProfileModal(!toggleProfileModal)}
                    >
                      Изменить пароль
                    </ToChangePasswordButton>
                  </>
                : <>
                  <ModalInputBlock>
                    <Text size="medium">

                    </Text>
                    <ModalFieldTitle>
                      <b>Старый пароль</b>
                      <Text color="red">*</Text>
                    </ModalFieldTitle>
                    <Box
                        direction="row"
                        align="center"
                        border
                    >
                      <TextInput
                          plain
                          type={revealOld ? 'text' : 'password'}
                          placeholder="Напишите старый пароль"
                          value={oldPassword}
                          onChange={(e) => setOldPassword(e.target.value)}
                      />
                      <Button
                          icon={revealOld ? <View size="medium" /> : <Hide size="medium" />}
                          onClick={() => setRevealOld(!revealOld)}
                      />
                    </Box>
                  </ModalInputBlock>
                  <ModalInputBlock>
                    <Text size="medium">

                    </Text>
                    <ModalFieldTitle>
                      <b>Новый пароль</b>
                      <Text color="red">*</Text>
                    </ModalFieldTitle>
                    <Box
                        direction="row"
                        align="center"
                        border
                    >
                      <TextInput
                          plain
                          type={revealNew ? 'text' : 'password'}
                          placeholder="Напишите новый пароль"
                          value={newPassword}
                          onChange={(e) => setNewPassword(e.target.value)}
                      />
                      <Button
                          icon={revealNew ? <View size="medium" /> : <Hide size="medium" />}
                          onClick={() => setRevealNew(!revealNew)}
                      />
                    </Box>
                  </ModalInputBlock>
                  <ModalInputBlock>
                    <Text size="medium">

                    </Text>
                    <ModalFieldTitle>
                      <b>Повторите новый пароль</b>
                      <Text color="red">*</Text>
                    </ModalFieldTitle>
                    <Box
                        direction="row"
                        align="center"
                        border
                    >
                      <TextInput
                          plain
                          type={revealRep ? 'text' : 'password'}
                          placeholder="Повторите новый пароль"
                          value={repeatNewPassword}
                          onChange={(e) => setRepeatNewPassword(e.target.value)}
                      />
                      <Button
                          icon={revealRep ? <View size="medium" /> : <Hide size="medium" />}
                          onClick={() => setRevealRep(!revealRep)}
                      />
                    </Box>
                  </ModalInputBlock>
                  <ToChangeProfileButton
                    onClick={() => setToggleProfileModal(!toggleProfileModal)}
                  >
                    Назад
                  </ToChangeProfileButton>
                </>
            }

            <ModalButton onClick={(e) => handleSubmitProfile(e)}>
              Сохранить
            </ModalButton>
          </Box>
        </Modal>
      </>
  );
};

const HeaderBox = styled(Box)`
  position: sticky;
  position: -webkit-sticky;
  top: 0;
  z-index: 10;
`;

const SideBarBox = styled(Box)`
  position: fixed;
  width: 100%;
  height: 100vh;
`;

const SearchInput = styled(TextInput)`
  font-weight: normal;
  font-size: 14px;
  line-height: 19px;
  border: 1px solid #BDBDBD;
  border-radius: 8px;
`;


const ToChangeProfileButton = styled(Box)`
  font-weight: 600;
  font-size: 18px;
  line-height: 24px;
  text-align: center;
  text-decoration-line: underline;
  color: #BDBDBD;
  margin-top: 24px;
`;

const ToChangePasswordButton = styled(Box)`
  font-weight: 600;
  font-size: 18px;
  line-height: 24px;
  text-align: center;
  text-decoration-line: underline;
  color: #2F80ED;
  margin-top: 24px;
`;

const ModalInputBlock = styled.div`
  margin: 24px 32px 0 32px;
`;

const ModalButton = styled(Button)`
  margin: 24px 32px 32px 32px;
  background: #2F80ED;
  border-radius: 8px;
  min-height: 64px;
  text-align: center;
  color: white;
`;

const ModalFieldTitle = styled(Text)`
  font-weight: 600;
  font-size: 16px;
  line-height: 24px;
  padding-bottom: 8px;
  margin: 0;
  color: #333333; 
`;

const ModalTitle = styled(Text)`
  font-weight: bold;
  font-size: 32px;
  line-height: 40px;
  color: #333333;
  padding-left: 32px;
  margin-bottom: 32px;
  padding-bottom: 32px;
  border-bottom: 1px solid #E0E0E0;
`;

const ModalAvatar = styled(Avatar)`
  margin-top: 32px;
`;

export default AdminLayout;
