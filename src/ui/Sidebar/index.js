import React from "react";
import { useLocation, NavLink, useHistory } from "react-router-dom";
import styled from "styled-components";
import {
  HouseIcon,
  WalletIcon,
  RoomIcon,
  SalesIcon,
  HotelIcon,
  MenuIcon,
  BookingIcon,
  Logo,
  HelpIcon,
  PhoneIcon,
  EmailIcon,
  LocationIcon,
} from "ui/Common/Icon";
import { Button, Box, Sidebar, Nav, Heading, Text } from "grommet";
import { Logout } from "grommet-icons";
import { Menu } from "ui/Common/Menu";
import { Button as GrommetButton } from "grommet";
import { useAuthState } from "modules/auth/AuthProvider";
import { ManagersIcon, VendorsIcon } from "../Common/Icon";
import Modal from "ui/Common/Modal";
import useModal from "ui/Common/Modal/useModal";

const SidebarBox = styled(Box)`
  background-color: ${(props) =>
    props.activeRoute && "rgba(255, 255, 255, 0.1)"};
  button {
    color: ${(props) =>
      props.activeRoute ? "rgb(255, 255, 255)" : "rgba(255, 255, 255, 0.5)"};
  }
`;
const src = "//s.gravatar.com/avatar/b7fb138d53ba0f573212ccce38a7c43b?s=80";

const icons = (activeRoute) => {
  return {
    home: (
        <HouseIcon
            color={`${
                activeRoute ? "rgb(255, 255, 255)" : "rgba(255, 255, 255, 0.5)"
            }`}
            width={24}
            height={24}
        />
    ),
    wallet: (
        <WalletIcon
            color={`${
                activeRoute ? "rgb(255, 255, 255)" : "rgba(255, 255, 255, 0.5)"
            }`}
            width={24}
            height={24}
        />
    ),
    room: (
        <RoomIcon
            color={`${
                activeRoute ? "rgb(255, 255, 255)" : "rgba(255, 255, 255, 0.5)"
            }`}
            width={24}
            height={24}
        />
    ),
    sales: (
        <SalesIcon
            color={`${
                activeRoute ? "rgb(255, 255, 255)" : "rgba(255, 255, 255, 0.5)"
            }`}
            width={24}
            height={24}
        />
    ),
    hotel: (
        <HotelIcon
            color={`${
                activeRoute ? "rgb(255, 255, 255)" : "rgba(255, 255, 255, 0.5)"
            }`}
            width={24}
            height={24}
        />
    ),
    booking: (
        <BookingIcon
            color={`${
                activeRoute ? "rgb(255, 255, 255)" : "rgba(255, 255, 255, 0.5)"
            }`}
            width={24}
            height={24}
        />
    ),
    vendor: (
      <VendorsIcon
        color={`${
          activeRoute ? "rgb(255, 255, 255)" : "rgba(255, 255, 255, 0.5)"
        }`}
        width={24}
        height={24}
      />
    ),
    manager: (
      <ManagersIcon
        color={`${
          activeRoute ? "rgb(255, 255, 255)" : "rgba(255, 255, 255, 0.5)"
        }`}
        width={24}
        height={24}
      />
    ),
  };
};

const SidebarHeader = ({ isAdmin, currentVendor, vendorList, selectVendor }) => (
  <Box
    align="center"
    direction="row"
    height="100px"
    border={{ side: "bottom", color: "rgba(255,255,255,0.5)" }}
    pad={{horizontal: 'medium', vertical: 'small'}}
    margin={{ bottom: "medium" }}
  >
    { isAdmin ? (
        <>
          <Box margin={{right: '30px'}}>
            <MenuIcon />
          </Box>
          <Logo />
        </>
      ) : (
        <Menu
          pad="0"
          margin="0"
          dropProps={{ elevation: "none" }}
          label={
            <Heading level={4} pad="0" margin="0">
              {currentVendor.name}
            </Heading>
          }
          items={vendorList.map((vendor) => ({
            label: vendor.name,
            id: vendor.id,
            onClick: () => selectVendor(vendor),
          }))}
        />
      )}
  </Box>
);

const SidebarButton = ({ icon, label, activeRoute, onClick, ...rest }) => (
  <SidebarBox
    pad="medium"
    margin="0"
    activeRoute={activeRoute}
    onClick={onClick}
  >
    <Button
      pad="medium"
      plain
      color="red"
      icon={icon}
      label={label}
      alignSelf="start"
      {...rest}
    />
  </SidebarBox>
);

const logout = () => {
  sessionStorage.removeItem("token");
  window.location = "/";
};

export default ({ routes, vendorList, currentVendor, selectVendor }) => {
  const [isShowingHelp, toggleHelp] = useModal();
  const location = useLocation();
  const user = useAuthState();
  const history = useHistory();
  const activeRoute = (routeName) => {
    return location.pathname.indexOf(routeName) > -1 ? true : false;
  };
  const isAdmin = user?.user?.roles.includes('superadmin');
  return (
    <>
        <Box direction="row" height="100vh">
            <Sidebar
                gap="none"
                responsive={true}
                background="brand"
                header={
                    <SidebarHeader
                        isAdmin={isAdmin}
                        selectVendor={selectVendor}
                        currentVendor={currentVendor}
                        vendorList={vendorList}
                    />
                }
                fill
                pad={{ right: "0" }}
                animation={[{ type: "fadeIn", duration: 300 }]}
                footer={
                    <Nav
                        pad="medium"
                        direction="row"
                        onClick={toggleHelp}
                    >
                        <HelpIcon width="24" height="24"/>
                        <SideBarText>Помощь</SideBarText>
                    </Nav>
                }
            >
                <Nav gap="none" responsive={false}>
                    {routes.map((prop, key) => {
                        if (!prop.redirect) {
                            return (
                                <SidebarButton
                                    activeRoute={activeRoute(prop.layout + prop.path)}
                                    key={key}
                                    onClick={() => history.push(prop.layout + prop.path)}
                                    icon={icons(activeRoute(prop.layout + prop.path))[prop.icon]}
                                    label={prop.name}
                                />
                            );
                        }

                        return null;
                    })}
                    <SidebarButton
                        label="Выход"
                        onClick={() => logout()}
                        icon={<Logout color="rgba(255, 255, 255, 0.5)" />}
                    />
                </Nav>
            </Sidebar>
        </Box>
        <Modal
            isShowing={isShowingHelp}
            hide={toggleHelp}
            background="#000"
            opacity=".5"
            width={'480px'}
            margin="14.75rem auto"
        >
            <Box
                pad="none"
            >
                <ModalTitle>Наши контакты</ModalTitle>
                <Box pad={{left: '32px'}}>
                    <Box pad={{top:'40px'}} direction="row" align="center">
                        <PhoneIcon width="24" height="24"/>
                        <ModalHelpLabel>+7 (778) 931 03 87</ModalHelpLabel>
                    </Box>
                    <Box pad={{top:'40px'}} direction="row" align="center">
                        <EmailIcon width="24" height="24"/>
                        <ModalHelpLabel>pingo@travel.com</ModalHelpLabel>
                    </Box>
                    <Box pad={{top:'40px'}} direction="row" align="center">
                        <LocationIcon width="24" height="24"/>
                        <ModalHelpLabel>Наурызбай батыра, 99/1, 10 этаж, 7 офис</ModalHelpLabel>
                    </Box>
                    <Box pad={{top:'40px'}}>
                        <ModalHelpFooter>
                            Pingo Admin Dashboard
                            <br/>
                            <span>© 2021 All Rights Reserved</span>
                        </ModalHelpFooter>
                    </Box>
                </Box>
            </Box>
        </Modal>
    </>
  );
};

const SideBarText = styled(Text)`
  opacity: 0.5;
`;

const ModalTitle = styled(Text)`
  font-weight: bold;
  font-size: 32px;
  line-height: 40px;
  color: #333333;
  padding: 0 32px 32px 32px;
  border-bottom: 1px solid #E0E0E0;
  margin: 0;
`;

const ModalHelpLabel = styled(Text)`
  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  line-height: 24px;
  color: #333333;
  padding-left: 24px;
`;

const ModalHelpFooter = styled(Text)`
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 16px;
  color: #969BA0;
  padding-bottom: 32px;
`;
