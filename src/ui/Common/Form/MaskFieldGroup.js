import PropTypes from "prop-types";
import InputMask from "react-input-mask";

const MaskFieldGroup = (props) => {
    const {
        onChange,
        onBlur,
        id,
        name,
        label,
        type,
        disabled,
        value,
        placeholder,
    } = props;
    const mask = "+9 999 999 99 99";

    return (
        <InputMask
            disabled={disabled}
            id={id}
            mask={mask}
            label={label}
            name={name}
            onBlur={onBlur}
            onChange={onChange}
            placeholder={placeholder}
            type={`${type}`}
            value={`${value}`}
        />
    );
};

MaskFieldGroup.propTypes = {
    id: PropTypes.string,
    name: PropTypes.string,
    label: PropTypes.string,
    placeholder: PropTypes.string,
    disabled: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    type: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    help: PropTypes.string,
    error: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    children: PropTypes.node,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    onChange: PropTypes.func.isRequired,
    onBlur: PropTypes.func,
    className: PropTypes.string,
};

export default MaskFieldGroup;
