import { Select, ThemeContext } from "grommet";


const FormSelect = (props) => {
  return (
    <ThemeContext.Extend
      value={{
        select: {
          control: {
            extend:
              "border: 0.5px solid #BDBDBD; border-radius:8px; color:#333",
          },
          icons: {
            color: "#828282",
          },
        },
      }}
    >
      <Select {...props} />
    </ThemeContext.Extend>
  );
};

export default FormSelect;
