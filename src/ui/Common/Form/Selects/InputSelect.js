import { Select, ThemeContext } from "grommet";

const InputSelect = (props) => {
  return (
    <ThemeContext.Extend
      value={{
        select: {
          extend: "padding:0; margin:0",
          control: {
            extend:
              "border:none; color:#333; padding:0; margin:0",
          },
          icons: {
            color: "#828282",
          },
        },
      }}
    >
      <Select {...props} />
    </ThemeContext.Extend>
  );
};

export default InputSelect;
