import { useState } from 'react';
import PropTypes from "prop-types";
import "./index.scss";

const Refund = ({name, modal, getRefund, id}) => {
    const [state, setState] = useState(false);
    return (
        <div
            className="refund__block"
            onClick={(e) => {
                e.stopPropagation();
                setState(!state);
                modal(state);
                getRefund(id);
            }}
        >
            <p className="refund__text">
                {name}
            </p>
        </div>
    );
};

Refund.propTypes = {
    name: PropTypes.string,
    modal: PropTypes.func,
};

export default Refund;