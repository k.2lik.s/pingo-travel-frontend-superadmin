import styled from "styled-components";
import { Button as GrommetButton, ThemeContext } from "grommet";

const SumButton = styled(GrommetButton)`
  font-weight: bold;
  background-color: rgba(47, 128, 237, 0.1);
  border-radius: 8px;
  color: #2f80ed;
  border: none;
  padding: 12px 16px;
  text-align: right;
  width: 182px;
`;

export { SumButton };

const SecondaryButton = (props) => {
  return (
    <ThemeContext.Extend
      value={{
        button: {
          extend: `background: #fff;
          font-weight: bold;
          font-size: 20px;
          border: 1px solid #2F80ED;
          line-height: 24px;
          min-height: 56px;
          color: #2f80ed;`,
        },
      }}
    >
      <GrommetButton {...props} />
    </ThemeContext.Extend>
  );
};

const GrayButton = (props) => {
  return (
    <ThemeContext.Extend
      value={{
        button: {
          extend: `
          line-height: 24px;
          min-height: 56px;
          border: 1px solid #828282;
          box-sizing: border-box;
          border-radius: 8px;
          `,
        },
      }}
    >
      <GrommetButton {...props} />
    </ThemeContext.Extend>
  );
};

export { SecondaryButton, GrayButton };
