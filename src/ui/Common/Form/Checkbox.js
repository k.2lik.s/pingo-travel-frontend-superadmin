import { useState } from 'react';
import PropTypes from "prop-types";
import styled from "styled-components";
import {
    Box,
    CheckBox,
    Text
} from "grommet";

const Checkbox = ({ icon, label, id}) => {
    const [checked, setChecked] = useState();
    return (
        <Box
            pad={{ top: '16px' }}
            direction="row"
            key={id}
            onClick={() => setChecked(!checked)}
        >
            <FeatureCheckbox
                checked={checked}
                onChange={(e) =>  setChecked(e.target.checked)}
            />
            { icon
                ? <img style={{ paddingLeft:'16px' }} src={icon} alt="logo"/>
                : null
            }
            <FeatureText>{label}</FeatureText>
        </Box>
    );
};

const FeatureCheckbox = styled(CheckBox)`
  border: 1px solid #BDBDBD;
  border-radius: 2px;
  background: #FFFFFF;
`;

const FeatureText = styled(Text)`
  font-weight: normal;
  font-size: 16px;
  line-height: 24px;
  color: #333333;
  margin: 0;
  padding-left: 14px;
  -moz-user-select: none;
  -khtml-user-select: none;
  -webkit-user-select: none;
  user-select: none;
`;

Checkbox.propTypes = {
    checked: PropTypes.bool,
    label: PropTypes.string,
};

export default Checkbox;