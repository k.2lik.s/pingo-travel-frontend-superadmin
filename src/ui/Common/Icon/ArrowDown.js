import React from "react";
import PropTypes from "prop-types";
import Icon from "./Icon";

const ArrowDownIcon = ({ originalWidth, originalHeight, color, ...props }) => (
    <Icon
        originalHeight={originalHeight}
        originalWidth={originalWidth}
        {...props}
    >
        <path d="M1.03266 4.87351L5.99943 9.84147L10.9662 4.87291C11.203 4.63611 11.586 4.63611 11.8228 4.87291C12.0591 5.10971 12.0591 5.49337 11.8228 5.73017L6.42803 11.1268C6.19183 11.3636 5.80817 11.3636 5.57197 11.1268L0.177153 5.73022C-0.0590496 5.49342 -0.0590496 5.10916 0.177153 4.87236C0.412756 4.63671 0.79646 4.63671 1.03266 4.87351Z" fill="#2F80ED"/>
    </Icon>
);

ArrowDownIcon.defaultProps = {
    originalWidth: 12,
    originalHeight: 16,
};

ArrowDownIcon.propTypes = {
    color: PropTypes.string,
    originalHeight: PropTypes.number,
    originalWidth: PropTypes.number,
};

export default ArrowDownIcon;
