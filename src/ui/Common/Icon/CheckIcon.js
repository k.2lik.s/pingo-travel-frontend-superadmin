import React from "react";
import PropTypes from "prop-types";
import Icon from "./Icon";

const CheckIcon = ({ originalWidth, originalHeight, color, ...props }) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    {...props}
  >
    <path
      d="M23.6107 8C22.9443 8 22.3179 8.25967 21.8465 8.73117L12.6149 17.9778L10.1544 15.5176C9.68333 15.0464 9.05698 14.7868 8.39075 14.7868C7.72442 14.7868 7.09807 15.0464 6.6269 15.5175C6.15583 15.9885 5.89648 16.6149 5.89648 17.2811C5.89648 17.9473 6.15583 18.5737 6.6269 19.0446L10.8516 23.2693C11.3226 23.7405 11.949 24.0001 12.6153 24.0001C13.2813 24.0001 13.9079 23.7404 14.3795 23.2689L25.3745 12.2581C26.3468 11.2855 26.3467 9.70306 25.3745 8.73041C24.9034 8.25945 24.2769 8 23.6107 8Z"
      fill="#219653"
    />
    ;
  </Icon>
);

CheckIcon.defaultProps = {
  originalWidth: 32,
  originalHeight: 32,
};

CheckIcon.propTypes = {
  color: PropTypes.string,
  originalHeight: PropTypes.number,
  originalWidth: PropTypes.number,
};

export default CheckIcon;
