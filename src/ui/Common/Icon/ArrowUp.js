import React from "react";
import PropTypes from "prop-types";
import Icon from "./Icon";

const ArrowUpIcon = ({ originalWidth, originalHeight, color, ...props }) => (
    <Icon
        originalHeight={originalHeight}
        originalWidth={originalWidth}
        {...props}
    >
        <g clip-path="url(#clip0)">
            <path d="M10.9673 11.1265L6.00057 6.15853L1.03381 11.1271C0.797007 11.3639 0.413951 11.3639 0.177151 11.1271C-0.0590504 10.8903 -0.0590503 10.5066 0.177152 10.2698L5.57197 4.87322C5.80817 4.63642 6.19183 4.63642 6.42803 4.87322L11.8228 10.2698C12.059 10.5066 12.059 10.8908 11.8228 11.1276C11.5872 11.3633 11.2035 11.3633 10.9673 11.1265Z" fill="#2F80ED"/>
        </g>
        <defs>
            <clipPath id="clip0">
                <rect width="12" height="16" fill="white" transform="matrix(1 1.74846e-07 1.74846e-07 -1 0 16)"/>
            </clipPath>
        </defs>
    </Icon>
);

ArrowUpIcon.defaultProps = {
    originalWidth: 12,
    originalHeight: 16,
};

ArrowUpIcon.propTypes = {
    color: PropTypes.string,
    originalHeight: PropTypes.number,
    originalWidth: PropTypes.number,
};

export default ArrowUpIcon;
