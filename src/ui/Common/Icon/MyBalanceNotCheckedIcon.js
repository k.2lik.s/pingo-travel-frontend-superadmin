import React from "react";
import PropTypes from "prop-types";
import Icon from "./Icon";

const MyBalanceNotCheckedIcon = ({ originalWidth, originalHeight, color, ...props }) => (
    <Icon
        originalHeight={originalHeight}
        originalWidth={originalWidth}
        {...props}
    >
        <rect x="1" y="1" width="16" height="16" rx="8" fill="white" stroke="#BDBDBD"/>
    </Icon>
);

MyBalanceNotCheckedIcon.defaultProps = {
    originalWidth: 18,
    originalHeight: 18,
};

MyBalanceNotCheckedIcon.propTypes = {
    originalHeight: PropTypes.number,
    originalWidth: PropTypes.number,
};

export default MyBalanceNotCheckedIcon;
