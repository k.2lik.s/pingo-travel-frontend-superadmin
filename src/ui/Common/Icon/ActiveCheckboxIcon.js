import React from "react";
import PropTypes from "prop-types";
import Icon from "./Icon";

const ActiveCheckboxIcon = ({ originalWidth, originalHeight, color, ...props }) => (
    <Icon
        originalHeight={originalHeight}
        originalWidth={originalWidth}
        {...props}
    >
        <circle cx="12" cy="12" r="11.5" fill="white" fill-opacity="0.2" stroke="white"/>
        <circle cx="12" cy="12" r="7.5" fill="white" stroke="white"/>
    </Icon>
);

ActiveCheckboxIcon.defaultProps = {
    originalWidth: 24,
    originalHeight: 24,
};

ActiveCheckboxIcon.propTypes = {
    originalHeight: PropTypes.number,
    originalWidth: PropTypes.number,
};

export default ActiveCheckboxIcon;
