import React from "react";
import PropTypes from "prop-types";
import Icon from "./Icon";
import svg from "./svgs/edit.svg"

const EditIcon = ({ originalWidth, originalHeight, color, ...props }) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    {...props}
  >
    <use fill={color} href={`${svg}#root`}/>
  </Icon>
);

EditIcon.defaultProps = {
  originalWidth: 20,
  originalHeight: 20,
  width: 20,
  height: 20,
  color: '#2F80ED'
};

EditIcon.propTypes = {
  color: PropTypes.string,
  originalHeight: PropTypes.number,
  originalWidth: PropTypes.number,
};

export default EditIcon;
