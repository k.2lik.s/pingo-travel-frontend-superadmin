import React from "react";
import PropTypes from "prop-types";
import Icon from "./Icon";

const NotificationIcon = ({ originalWidth, originalHeight, color, counter, ...props }) => (
    <Icon
        originalHeight={originalHeight}
        originalWidth={originalWidth}
        {...props}
    >
        <path
            d="M23.3333 42.8333H23.1187C23.2568 42.4597 23.3295 42.065 23.3333 41.6666V35.8333C23.3294 33.7663 22.6402 31.759 21.3735 30.1257C20.1068 28.4923 18.3343 27.3251 16.3333 26.8068V26.5C16.3333 25.8811 16.0875 25.2876 15.6499 24.85C15.2123 24.4125 14.6188 24.1666 14 24.1666C13.3812 24.1666 12.7877 24.4125 12.3501 24.85C11.9125 25.2876 11.6667 25.8811 11.6667 26.5V26.8068C9.66574 27.3251 7.89317 28.4923 6.6265 30.1257C5.35983 31.759 4.67058 33.7663 4.66667 35.8333V41.6666C4.67053 42.065 4.74316 42.4597 4.88133 42.8333H4.66667C4.35725 42.8333 4.0605 42.9562 3.84171 43.175C3.62292 43.3938 3.5 43.6905 3.5 44C3.5 44.3094 3.62292 44.6061 3.84171 44.8249C4.0605 45.0437 4.35725 45.1666 4.66667 45.1666H23.3333C23.6428 45.1666 23.9395 45.0437 24.1583 44.8249C24.3771 44.6061 24.5 44.3094 24.5 44C24.5 43.6905 24.3771 43.3938 24.1583 43.175C23.9395 42.9562 23.6428 42.8333 23.3333 42.8333Z"
            fill="#828282"/>
        <path
            d="M9.98193 47.5C10.3863 48.2088 10.971 48.7981 11.6767 49.2079C12.3823 49.6178 13.1839 49.8337 13.9999 49.8337C14.816 49.8337 15.6175 49.6178 16.3232 49.2079C17.0289 48.7981 17.6135 48.2088 18.0179 47.5H9.98193Z"
            fill="#828282"/>
        <rect x="12" y="2" width="34" height="34" rx="17" fill="#2F80ED" stroke="white" stroke-width="4"/>
        <text x="29" y="24" fill="#FFF" text-anchor="middle" style={{font: 'bold 15px sans-serif'}}>{counter}</text>
    </Icon>
);

NotificationIcon.defaultProps = {
    originalWidth: 50,
    originalHeight: 50,
};

NotificationIcon.propTypes = {
    originalHeight: PropTypes.number,
    originalWidth: PropTypes.number,
};

export default NotificationIcon;
