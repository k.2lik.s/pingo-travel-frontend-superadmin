import React from "react";
import PropTypes from "prop-types";
import Icon from "./Icon";

const PreviewCloseIcon = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    {...props}
  >
    <circle
      cx="13"
      cy="13"
      r="12"
      fill="black"
      fill-opacity="0.2"
      stroke="white"
    />
    <path
      fill-rule="evenodd"
      clip-rule="evenodd"
      d="M13.75 13L19 7.75L18.25 7L13 12.25L7.75 7L7 7.75L12.25 13L7 18.25L7.75 19L13 13.75L18.25 19L19 18.25L13.75 13Z"
      fill="white"
    />
  </Icon>
);

PreviewCloseIcon.defaultProps = {
  originalWidth: 26,
  originalHeight: 26,
};

PreviewCloseIcon.propTypes = {
  color: PropTypes.string,
  originalHeight: PropTypes.number,
  originalWidth: PropTypes.number,
};

export default PreviewCloseIcon;
