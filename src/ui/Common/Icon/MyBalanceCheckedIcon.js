import React from "react";
import PropTypes from "prop-types";
import Icon from "./Icon";

const MyBalanceCheckedIcon = ({ originalWidth, originalHeight, color, ...props }) => (
    <Icon
        originalHeight={originalHeight}
        originalWidth={originalWidth}
        {...props}
    >
        <circle cx="8" cy="8" r="8" fill="#2F80ED"/>
        <circle cx="8" cy="8" r="3.5" fill="white"/>
    </Icon>
);

MyBalanceCheckedIcon.defaultProps = {
    originalWidth: 16,
    originalHeight: 16,
};

MyBalanceCheckedIcon.propTypes = {
    originalHeight: PropTypes.number,
    originalWidth: PropTypes.number,
};

export default MyBalanceCheckedIcon;
