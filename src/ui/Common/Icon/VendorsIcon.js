import React from "react";
import PropTypes from "prop-types";
import Icon from "./Icon";
import svg from './svgs/vendor.svg';

const VendorsIcon = ({ originalWidth, originalHeight, color, ...props }) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    {...props}
  >
    <use fill={color} href={`${svg}#root`}/>
  </Icon>
);

VendorsIcon.defaultProps = {
  originalWidth: 24,
  originalHeight: 24,
};

VendorsIcon.propTypes = {
  color: PropTypes.string,
  originalHeight: PropTypes.number,
  originalWidth: PropTypes.number,
};

export default VendorsIcon;
