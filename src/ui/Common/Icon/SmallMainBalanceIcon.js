import React from "react";
import PropTypes from "prop-types";
import Icon from "./Icon";

const SmallMainBalanceIcon = ({ originalWidth, originalHeight, color, ...props }) => (
    <Icon
        originalHeight={originalHeight}
        originalWidth={originalWidth}
        {...props}
    >
        <circle cx="24" cy="24" r="24" fill="#2F80ED"/>
        <g clip-path="url(#clip0)">
            <path d="M31.9457 16.2856C32.5832 16.2856 33.1007 16.8031 33.0984 17.4407V20.5939H26.242C26.1635 20.5939 26.0849 20.5986 26.0087 20.6078C25.5513 20.6609 25.1424 20.8735 24.8375 21.1877C24.4933 21.5411 24.2807 22.0239 24.2807 22.5552V25.124C24.2807 26.2075 25.1609 27.0876 26.2443 27.0876H33.0984V30.3102C33.0984 30.9478 32.581 31.4653 31.9433 31.4653H14.8694C14.2318 31.4653 13.7144 30.9478 13.7144 30.3102V17.4407C13.7144 17.3367 13.7305 17.2351 13.7559 17.138C13.7975 16.9833 13.8714 16.8423 13.9685 16.7199C14.181 16.4543 14.5067 16.2856 14.8717 16.2856H31.9457Z" fill="white"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M33.5048 21.4582C33.6573 21.5113 33.7936 21.5922 33.9091 21.6984C34.1401 21.9086 34.2856 22.2136 34.2856 22.5509V25.122C34.2856 25.4593 34.1401 25.7642 33.9091 25.9744C33.7936 26.0807 33.6573 26.1638 33.5048 26.2147C33.387 26.2539 33.2622 26.277 33.1306 26.277H26.2441C25.6065 26.277 25.0891 25.7596 25.0891 25.122V22.5509C25.0891 21.9133 25.6065 21.3958 26.2441 21.3958H33.1306C33.2622 21.3958 33.3869 21.4189 33.5048 21.4582ZM28.0784 24.6438C28.3971 24.6438 28.6559 24.3851 28.6559 24.0663V23.6828C28.6559 23.364 28.3971 23.1053 28.0784 23.1053H27.6949C27.5609 23.1053 27.4385 23.1538 27.3391 23.23C27.2028 23.334 27.1173 23.498 27.1173 23.6828V24.0663C27.1173 24.3851 27.3761 24.6438 27.6949 24.6438H28.0784Z" fill="white"/>
        </g>
        <defs>
            <clipPath id="clip0">
                <rect width="20.5714" height="20.5714" fill="white" transform="translate(13.7144 13.7143)"/>
            </clipPath>
        </defs>
    </Icon>
);

SmallMainBalanceIcon.defaultProps = {
    originalWidth: 48,
    originalHeight: 48,
};

SmallMainBalanceIcon.propTypes = {
    originalHeight: PropTypes.number,
    originalWidth: PropTypes.number,
};

export default SmallMainBalanceIcon;
