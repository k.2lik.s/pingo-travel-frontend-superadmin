import React from "react";
import PropTypes from "prop-types";
import Icon from "./Icon";

const EmailIcon = ({ originalWidth, originalHeight, color, ...props }) => (
    <Icon
        originalHeight={originalHeight}
        originalWidth={originalWidth}
        {...props}
    >
        <path d="M15.9082 12.123L23.9992 17.238V6.79199L15.9082 12.123Z" fill="#2F80ED"/>
        <path d="M0 6.79199V17.238L8.091 12.123L0 6.79199Z" fill="#2F80ED"/>
        <path d="M22.4989 3.75H1.49895C0.750445 3.75 0.156445 4.308 0.0439453 5.0265L11.9989 12.903L23.9539 5.0265C23.8414 4.308 23.2474 3.75 22.4989 3.75Z" fill="#2F80ED"/>
        <path d="M14.5353 13.0288L12.4128 14.4268C12.2868 14.5093 12.1443 14.5498 12.0003 14.5498C11.8563 14.5498 11.7138 14.5093 11.5878 14.4268L9.46534 13.0273L0.0483398 18.9838C0.16384 19.6963 0.75484 20.2498 1.50034 20.2498H22.5003C23.2458 20.2498 23.8368 19.6963 23.9523 18.9838L14.5353 13.0288Z" fill="#2F80ED"/>
    </Icon>
);

EmailIcon.defaultProps = {
    originalWidth: 24,
    originalHeight: 24,
};

EmailIcon.propTypes = {
    originalHeight: PropTypes.number,
    originalWidth: PropTypes.number,
};

export default EmailIcon;
