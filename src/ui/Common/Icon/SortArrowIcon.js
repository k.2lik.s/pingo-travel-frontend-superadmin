import React from "react";
import PropTypes from "prop-types";
import Icon from "./Icon";
import svg from "./svgs/sort-arrow.svg"

const SortArrowIcon = ({ originalWidth, originalHeight, color, inverted=false, ...props }) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    style={inverted ? {transform: "scaleY(-1)"}: {}}
    {...props}
  >
    <use fill={color} href={`${svg}#root`}/>
  </Icon>
);

SortArrowIcon.defaultProps = {
  originalWidth: 9,
  originalHeight: 7,
  width: 9,
  height: 7,
  color: '#E0E0E0'
};

SortArrowIcon.propTypes = {
  color: PropTypes.string,
  originalHeight: PropTypes.number,
  originalWidth: PropTypes.number,
};

export default SortArrowIcon;
