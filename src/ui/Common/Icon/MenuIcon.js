import React from "react";
import PropTypes from "prop-types";
import Icon from "./Icon";
import svg from "./svgs/menu.svg"

const MenuIcon = ({ originalWidth, originalHeight, color, ...props }) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    {...props}
  >
    <use fill={color} href={`${svg}#root`}/>
  </Icon>
);

MenuIcon.defaultProps = {
  originalWidth: 24,
  originalHeight: 24,
  width: 24,
  height: 24,
  color: '#FFFFFF'
};

MenuIcon.propTypes = {
  color: PropTypes.string,
  originalHeight: PropTypes.number,
  originalWidth: PropTypes.number,
};

export default MenuIcon;
