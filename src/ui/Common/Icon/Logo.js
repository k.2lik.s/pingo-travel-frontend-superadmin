import React from "react";
import PropTypes from "prop-types";
import Icon from "./Icon";
import svg from "./svgs/logo.svg";

const Logo = ({ originalWidth, originalHeight, color, ...props }) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    {...props}
  >
    <use fill={color} href={`${svg}#root`} />
  </Icon>
);

Logo.defaultProps = {
  originalWidth: 127,
  originalHeight: 48,
  width: 127,
  height: 48,
  color: '#FFFFFF'
};

Logo.propTypes = {
  color: PropTypes.string,
  originalHeight: PropTypes.number,
  originalWidth: PropTypes.number,
};

export default Logo;
