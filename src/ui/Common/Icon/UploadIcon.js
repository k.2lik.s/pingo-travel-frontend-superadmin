import React from "react";
import PropTypes from "prop-types";
import Icon from "./Icon";

const UploadIcon = ({ originalWidth, originalHeight, color, ...props }) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    {...props}
  >
    <path
      d="M8.1666 12.8334C8.1666 14.122 7.12198 15.1666 5.8334 15.1666C4.54462 15.1666 3.5 14.122 3.5 12.8334C3.5 11.5446 4.54462 10.5 5.8334 10.5C7.12198 10.5 8.1666 11.5446 8.1666 12.8334Z"
      fill="#BDBDBD"
    />
    <path
      d="M24.5 7H3.5C1.57034 7 0 8.57034 0 10.5V24.5C0 24.5654 0.0162354 24.6271 0.0198669 24.6925C0.00341797 24.8324 0.0104675 24.9736 0.061737 25.1067C0.349915 26.7471 1.77798 28 3.5 28H24.5C26.4297 28 28 26.4297 28 24.5V10.5C28 8.57034 26.4297 7 24.5 7ZM3.5 9.3334H24.5C25.1428 9.3334 25.6666 9.85721 25.6666 10.5V19.7623L19.5301 13.6255C18.732 12.8274 17.4336 12.8274 16.6367 13.6255L11.0834 19.1789L9.61325 17.7089C8.81537 16.9108 7.51675 16.9108 6.71994 17.7089L2.3334 22.0955V10.5C2.3334 9.85721 2.85721 9.3334 3.5 9.3334Z"
      fill="#BDBDBD"
    />
    <path
      d="M24.2828 14V15H25.2828H27.7172H28.7172V14V10.6045H32H33V9.60451V7.26025V6.26025H32H28.7172V3V2H27.7172H25.2828H24.2828V3V6.26025H21H20V7.26025V9.60451V10.6045H21H24.2828V14Z"
      fill="#BDBDBD"
      stroke="white"
      stroke-width="2"
    />
  </Icon>
);

UploadIcon.defaultProps = {
  originalWidth: 32,
  originalHeight: 32,
};

UploadIcon.propTypes = {
  color: PropTypes.string,
  originalHeight: PropTypes.number,
  originalWidth: PropTypes.number,
};

export default UploadIcon;
