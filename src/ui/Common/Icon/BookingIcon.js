import React from "react";
import PropTypes from "prop-types";
import Icon from "./Icon";

const BookingIcon = ({ originalWidth, originalHeight, color, ...props }) => (
    <Icon
        originalHeight={originalHeight}
        originalWidth={originalWidth}
        {...props}
    >
        <path d="M22.5938 0.171875H1.40625C0.630937 0.171875 0 0.802812 0 1.57812V15.5469C0 16.3222 0.630937 16.9531 1.40625 16.9531H22.5938C23.3691 16.9531 24 16.3222 24 15.5469V1.57812C24 0.802812 23.3691 0.171875 22.5938 0.171875Z"
              fill={color}
        />
        <path d="M19.5 20.4219H15.7031V18.3594H8.29688V20.4219H4.5C4.11187 20.4219 3.79688 20.7369 3.79688 21.125C3.79688 21.5131 4.11187 21.8281 4.5 21.8281H19.5C19.8881 21.8281 20.2031 21.5131 20.2031 21.125C20.2031 20.7369 19.8881 20.4219 19.5 20.4219Z"
              fill={color}
        />
    </Icon>
);

BookingIcon.defaultProps = {
    originalWidth: 24,
    originalHeight: 24,
};

BookingIcon.propTypes = {
    color: PropTypes.string,
    originalHeight: PropTypes.number,
    originalWidth: PropTypes.number,
};

export default BookingIcon;
