import { Menu as GrommetMenu, ThemeContext } from "grommet";


const Menu = (props) => {
  return (
    <ThemeContext.Extend
      value={{
        menu: {
            extend:
              "padding:0; background:#040849;box-shadow: none",
          icons: {
            color: "#D7D7D7",
          },
        },
      }}
    >
      <GrommetMenu {...props} />
    </ThemeContext.Extend>
  );
};

export default Menu;
