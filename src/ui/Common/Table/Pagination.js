import { Pagination, ThemeContext } from "grommet";


const ThemedPagination = (props) => {
  return (
    <ThemeContext.Extend
      value={{
        pagination: {
          button: {
            active: {
              background: {
                color: "#2F80ED",
              }
            },
            hover: {
              background: {
                color: "#2F80ED44",
              }
            }
          },
          icons: {
            color: "#2F80ED",
          },
        },
      }}
    >
      <Pagination {...props} />
    </ThemeContext.Extend>
  );
};

export default ThemedPagination;
