import ReactDOM from 'react-dom';

const Modal = ({ isShowing, hide, children, background, opacity, width, margin }) => isShowing ? ReactDOM.createPortal(
    <>
        <div className="modal-overlay" style={{background:background, opacity:opacity}}/>
        <div className="modal-wrapper" aria-modal aria-hidden tabIndex={-1} role="dialog">
            <div className="modal" style={{width:`${width}`, margin: `${margin}`}}>
                <div className="modal-header">
                    <button type="button" className="modal-close-button" data-dismiss="modal" aria-label="Close" onClick={hide}>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div>
                    {children}
                </div>
            </div>
        </div>
    </>, document.body
) : null;

export default Modal;