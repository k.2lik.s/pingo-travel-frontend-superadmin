import { useState, useEffect } from 'react';

const useModal = () => {
    const [isShowing, setIsShowing] = useState(false);

    const toggle = (id) => {
        setIsShowing(!isShowing);
    };

    return [isShowing, toggle];
};

export default useModal;