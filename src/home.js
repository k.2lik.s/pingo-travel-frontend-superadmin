import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import ManagerLayout from "./layouts/Manager.js";
import AuthLayout from "./layouts/Auth.js";
import { useAuthState } from "modules/auth/AuthProvider";
import Nprogress from "nprogress";
import AdminLayout from "./layouts/Admin";

Nprogress.configure({ showSpinner: false });



const Home = () => {
  const user = useAuthState();

  const UnauthenticatedApp = () => {
    return (
      <Switch>
        <Route path="/auth" render={(props) => <AuthLayout {...props} />} />
        <Redirect from="/" to="/auth" />
      </Switch>
    );
  };

  const AuthenticatedApp = () => {
    return (
      <Switch>
        <Route path="/manager" render={(props) => <ManagerLayout {...props} />} />
        <Redirect from="/" to="/manager/dashboard" />
      </Switch>
    );
  };

  const AdminApp = () => {
    return (
      <Switch>
        <Route path="/admin" render={(props) => <AdminLayout {...props} />} />
        <Redirect from="/" to="/admin/vendors" />
      </Switch>
    );
  };

  return !user.isAuthenticated
    ? <UnauthenticatedApp />
    : user.user.roles.includes('superadmin')
      ? <AdminApp />
      : <AuthenticatedApp />;
};

export default Home;
