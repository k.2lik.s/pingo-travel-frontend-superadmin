import SalesPage from "./modules/sales/pages/sales.js";
import HotelEdit from "./modules/hotel/pages";
import Bookings from "./modules/booking/pages/booking.js";
import Detail from "./modules/sales/pages/detail.js";
import Dashboard from "./modules/dashboard/pages/dashboard.js";
import Balance from "./modules/balance/pages/balance.js";
import Rooms from "./modules/rooms/pages/rooms.js";
import RoomCreate from "./modules/rooms/pages/room-create.js";
import Vendors from "./modules/vendors/pages/vendors.js";
import VendorCreate from "./modules/vendors/pages/vendor-create";
import Managers from "./modules/managers/pages/managers";
import ManagerCreate from "./modules/managers/pages/manager-create";

const adminLayout = '/admin';
const managerLayout = '/manager';

export const adminRoutes = [
  {
    path: "/vendors",
    name: "Агентства",
    icon: "vendor",
    component: Vendors,
    layout: adminLayout,
  },
  {
    exact: true,
    path: "/vendors/create",
    name: "Добавить aгентство",
    layout: adminLayout,
    redirect: true,
    component: VendorCreate,
  },
  {
    path: "/managers",
    name: "Менеджеры",
    icon: "manager",
    exact: true,
    component: Managers,
    layout: adminLayout,
  },
  {
    exact: true,
    path: "/managers/create/:stepId",
    name: "Зарегистрировать менеджера",
    layout: adminLayout,
    redirect: true,
    component: ManagerCreate,
  },
];

const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Главная",
    icon: "home",
    component: Dashboard,
    layout: managerLayout,
  },
  {
    path: "/balance",
    name: "Мой баланс",
    icon: "wallet",
    component: Balance,
    layout: managerLayout,
  },
  {
    path: "/rooms",
    name: "Комнаты",
    icon: "room",
    exact: true,
    component: Rooms,
    layout: managerLayout,
  },
  {
    exact: true,
    path: "/rooms/create/:stepId",
    name: "Добавить комнату",
    layout: managerLayout,
    redirect: true,
    component: RoomCreate,
  },
  {
    path: "/sales",
    name: "Продажи",
    icon: "sales",
    exact: true,
    component: SalesPage,
    layout: managerLayout,
  },
  {
    path: "/booking",
    name: "Бронирование",
    icon: "booking",
    exact: true,
    component: Bookings,
    layout: managerLayout,
  },
  {
    exact: true,
    path: "/sales/detail/:id",
    name: "Детали продажи",
    layout: managerLayout,
    redirect: true,
    component: Detail,
  },
    ...HotelEdit,
];

export default dashboardRoutes;
