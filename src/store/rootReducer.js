import { combineReducers } from "redux";
import common from "modules/common/reducers"
import room from "modules/rooms/reducers"
import order from "modules/sales/reducers"
import balance from "modules/balance/reducers"
import hotel from "modules/hotel/reducers"
import booking from "modules/booking/reducers"
import vendor from "modules/vendors/reducers"
import manager from "modules/managers/reducers"

export const rootReducer = combineReducers({
  common,
  room,
  order,
  balance,
  hotel,
  booking,
  vendor,
  manager,
});
