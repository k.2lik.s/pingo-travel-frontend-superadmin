import * as types from "./constants";
import sendRequest from "settings/sendRequest";
import Nprogress from "nprogress";

// export const getVendor = (vendorId) => (dispatch) => {
//     dispatch({ type: types.GET_VENDOR });
//     Nprogress.start();
//     return sendRequest(`/vendors/${vendorId}`, {
//         method: "GET"
//     })
//         .then(({data}) => {
//             dispatch({ type: types.GET_VENDOR_SUCCESS, payload: data });
//             Nprogress.done();
//         })
//         .catch((error) => {
//             dispatch({ type: types.GET_VENDOR_FAILURE, payload: error });
//         });
// };

export const getOwnVendors = () => (dispatch) => {
  dispatch({ type: types.GET_VENDORS });
  return sendRequest("/vendors", {
    method: "GET",
    params: { own: true },
  })
    .then(({ data }) => {
      dispatch({ type: types.GET_VENDORS_SUCCESS, payload: data });
    })
    .catch((error) => {
      dispatch({ type: types.GET_VENDORS_FAILURE, payload: error });
    });
};

export const selectVendor = (vendor) => (dispatch) => {
  dispatch({ type: types.SELECT_VENDOR, payload: vendor });
};

export const getNotifications = (vendor) => (dispatch) => {
  dispatch({ type: types.GET_NOTIFICATIONS });
  return sendRequest(`/vendors/${vendor}/notifications?status=unread`, {
    method: "GET"
  })
      .then(({data}) => {
        dispatch({ type: types.GET_NOTIFICATIONS_SUCCESS, payload: data });
      })
      .catch((error) => {
        dispatch({ type: types.GET_NOTIFICATIONS_FAILURE, payload: error });
      });
};

export const ReadNotification = (notification) => {
    return sendRequest(`/notifications/${notification}/read`, {
        method: "POST",
    });
};

export const getGlobalSearch = (vendor, text) => (dispatch) => {
    dispatch({ type: types.GET_SEARCH });
    return sendRequest(`/vendors/${vendor}/orders?q=${text}`, {
        method: "GET"
    })
        .then(({data}) => {
            dispatch({ type: types.GET_SEARCH_SUCCESS, payload: data });
        })
        .catch((error) => {
            dispatch({ type: types.GET_SEARCH_FAILURE, payload: error });
        });
};