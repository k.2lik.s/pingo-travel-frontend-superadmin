import produce from "immer";
import * as types from "../constants";

const INITAL_STATE = {
  list: [],
  currentVendor: {},
  loading: true,
  error: {},
};

export const vendor = produce((draft, action) => {
  switch (action.type) {
    case types.GET_VENDORS:
      draft.loading = true;
      return;
    case types.GET_VENDORS_SUCCESS:
      draft.list = action.payload.data;
      draft.currentVendor = action.payload.data[0]
      draft.loading = false;
      return;
    case types.GET_VENDORS.FAILURE:
      draft.error = action.payload.error;
      draft.loading = false;
      return;
      case types.SELECT_VENDOR:
        draft.currentVendor = action.payload;
        draft.loading = false;
        return;
  }
}, INITAL_STATE);
