import produce from "immer";
import * as types from "../constants";

const INITAL_STATE = {
    allNotifications: [],
    loading: true,
    error: {},
};

export const notifications = produce((draft, action) => {
    switch (action.type) {
        case types.GET_NOTIFICATIONS:
            draft.loading = true;
            return;
        case types.GET_NOTIFICATIONS_SUCCESS:
            draft.allNotifications = action.payload.data;
            draft.loading = false;
            return;
        case types.GET_NOTIFICATIONS_FAILURE:
            draft.error = action.payload.error;
            draft.loading = false;
            return;
    }
}, INITAL_STATE);
