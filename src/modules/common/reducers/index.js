import { combineReducers } from "redux";
import { vendor } from "./vendor";
import { notifications } from "./notifications";
import { search } from "./search";

export default combineReducers({
    vendor,
    notifications,
    search,
});
