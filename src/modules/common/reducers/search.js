import produce from "immer";
import * as types from "../constants";

const INITAL_STATE = {
    search: [],
    loading: true,
    error: {},
};

export const search = produce((draft, action) => {
    switch (action.type) {
        case types.GET_SEARCH:
            draft.loading = true;
            return;
        case types.GET_SEARCH_SUCCESS:
            draft.allNotifications = action.payload.data;
            draft.loading = false;
            return;
        case types.GET_SEARCH_FAILURE:
            draft.error = action.payload.error;
            draft.loading = false;
            return;
    }
}, INITAL_STATE);
