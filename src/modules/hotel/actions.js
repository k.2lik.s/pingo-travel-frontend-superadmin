import * as types from "./constants";
import sendRequest from "settings/sendRequest";
import Nprogress from "nprogress";

export const getVendor = (vendorId) => (dispatch) => {
    dispatch({ type: types.GET_VENDOR });
    Nprogress.start();
    return sendRequest(`/vendors/${vendorId}`, {
        method: "GET"
    })
        .then(({data}) => {
            dispatch({ type: types.GET_VENDOR_SUCCESS, payload: data });
            Nprogress.done();
        })
        .catch((error) => {
            dispatch({ type: types.GET_VENDOR_FAILURE, payload: error });
        });
};

export const getVendorAccounts = (vendorId) => (dispatch) => {
    dispatch({ type: types.GET_VENDOR_ACCOUNT });
    Nprogress.start();
    return sendRequest(`/vendor-accounts/${vendorId}`, {
        method: "GET"
    })
        .then(({data}) => {
            dispatch({ type: types.GET_VENDOR_ACCOUNT_SUCCESS, payload: data });
            Nprogress.done();
        })
        .catch((error) => {
            dispatch({ type: types.GET_VENDOR_ACCOUNT_FAILURE, payload: error });
        });
};

export const getVendorTypes = () => (dispatch) => {
    dispatch({ type: types.GET_VENDOR_TYPES });
    Nprogress.start();
    return sendRequest(`/vendor-types`, {
        method: "GET"
    })
        .then(({data}) => {
            dispatch({ type: types.GET_VENDOR_TYPES_SUCCESS, payload: data });
            Nprogress.done();
        })
        .catch((error) => {
            dispatch({ type: types.GET_VENDOR_TYPES_FAILURE, payload: error });
        });
};

export const getCover = (vendorId) => (dispatch) => {
    dispatch({ type: types.GET_COVER });
    Nprogress.start();
    sendRequest(`/vendors/${vendorId}/medias?collection=cover`, {
        method: "GET",
    })
        .then(({ data }) => {
            dispatch({ type: types.GET_COVER_SUCCESS, payload: data });
            Nprogress.done();
        })
        .catch((error) => {
            dispatch({ type: types.GET_COVER_FAILURE, payload: error });
        });
};

export const getVendorMedias = (vendorId) => (dispatch) => {
    dispatch({ type: types.GET_VENDOR_MEDIAS });
    Nprogress.start();
    return sendRequest(`/vendors/${vendorId}/medias?collection=gallery`, {
        method: "GET"
    })
        .then(({data}) => {
            dispatch({ type: types.GET_VENDOR_MEDIAS_SUCCESS, payload: data });
            Nprogress.done();
        })
        .catch((error) => {
            dispatch({ type: types.GET_VENDOR_MEDIAS_FAILURE, payload: error });
        });
};

export const postVendorMedia = ({ vendorId, collection, file }) => (dispatch) => {

    console.log('file')
    dispatch({ type: types.ADD_MEDIA_LOADING });

    const formData = new FormData();
    formData.append("collection", collection);
    formData.append("file", file);

    sendRequest(`/vendors/${vendorId}/medias?collection=${collection}`, {
        headers: {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
        },
        method: "POST",
        data: formData,
    })
        .then(({ data }) => {
            dispatch({
                type: types.ADD_MEDIA_SUCCESS,
                payload: { key: collection, data: data.data },
            });
        })
        .catch((error) => {
            dispatch({ type: types.ADD_MEDIA_FAILURE, payload: error });
        });
};

export const removeMedia = ({ mediaId, vendorId, collection }) => (dispatch) => {
    dispatch({ type: types.REMOVE_MEDIA_LOADING });

    sendRequest(`/vendors/${vendorId}/medias/${mediaId}`, {
        method: "DELETE",
    })
        .then(({ data }) => {
            dispatch({
                type: types.REMOVE_MEDIA_SUCCESS,
                payload: { key: collection, id: mediaId },
            });
        })
        .catch((error) => {
            dispatch({ type: types.REMOVE_MEDIA_FAILURE, payload: error });
        });
};

export const getFeatures = () => (dispatch) => {
    dispatch({ type: types.GET_FEATURES });
    Nprogress.start();
    return sendRequest(`/vendor-features`, {
        method: "GET"
    })
        .then(({data}) => {
            dispatch({ type: types.GET_FEATURES_SUCCESS, payload: data });
            Nprogress.done();
        })
        .catch((error) => {
            dispatch({ type: types.GET_FEATURES_FAILURE, payload: error });
        });
};

export const editVendor = (vendorId, form) => (dispatch) => {
    dispatch({ type: types.EDIT_LOADING });

    sendRequest(`/vendors/${vendorId}`, {
        method: "put",
        data: JSON.stringify(form),
    })
        .then(({ data }) => {
            dispatch({ type: types.EDIT_SUCCESS, payload: data });
        })
        .catch(({ response }) => {
            dispatch({ type: types.EDIT_FAILURE, payload: response.data });
        });
};

export const reset = () => dispatch => {
    dispatch({ type: types.RESET_CREATE_ROOM });
}

// TODO: get /vendors/vendorId
// TODO: get /vendors/vendorId/medias?collection=gallery
// TODO: put /vendors/vendorId
// TODO: post /vendors/vendorId/medias
// TODO: get /regions
// TODO: get /vendor-features
// TODO: get /vendors/vendorId/room-facilities
// TODO: post /vendors/vendorId/room-facilities
// TODO: put /room-facilities/id

// TODO: get /vendors/vendorId/bookings/
// TODO: get /vendors/vendorId/room-numbers  bronirovanie
// TODO: get /vendors/vendorId/room-numbers?status=active&condition=free
// TODO: filters: 1.price 2.guests 3.type 4.children 5.adults 6. checkIn checkOut
// TODO: get /room-numbers/:id
// TODO: post /vendors/vendorId/order   body: {}