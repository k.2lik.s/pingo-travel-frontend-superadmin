import produce from "immer";
import * as types from "../constants";

const INITAL_STATE = {
    gallery: [],
    cover: {},
    loading: true,
    error: {},
};

export const medias = produce((draft, action) => {
    switch (action.type) {
        case types.GET_COVER:
            draft.loading = true;
            return;
        case types.GET_COVER_SUCCESS:
            draft.cover = action.payload.data;
            draft.loading = false;
            return;
        case types.GET_COVER_FAILURE:
            draft.errors = action.payload.errors;
            draft.loading = false;
            return;
        case types.GET_VENDOR_MEDIAS:
            draft.loading = true;
            return;
        case types.GET_VENDOR_MEDIAS_SUCCESS:
            draft.cover = action.payload.data;
            draft.loading = false;
            return;
        case types.GET_VENDOR_MEDIAS_FAILURE:
            draft.error = action.payload.error;
            draft.loading = false;
            return;
        case types.ADD_MEDIA_LOADING:
            draft.loading = true;
            return;
        case types.ADD_MEDIA_SUCCESS:
            if (action.payload.key === "cover") {
                draft.cover = action.payload.data;
            }
            if (action.payload.key === "gallery") {
                draft.gallery = [...draft.gallery, action.payload.data];
            }
            draft.success = true;
            draft.loading = false;
            return;
        case types.ADD_MEDIA_FAILURE:
            draft.errors = action.payload.errors;
            draft.loading = false;
            return;
        case types.REMOVE_MEDIA_LOADING:
            draft.loading = true;
            return;
        case types.REMOVE_MEDIA_SUCCESS:
            if (action.payload.key === "cover") {
                draft.cover = {};
            }
            if (action.payload.key === "gallery") {
                draft.gallery = draft.gallery.filter(
                    (image) => image.id !== action.payload.id
                );
            }
            draft.loading = false;
            return;
        case types.REMOVE_MEDIA_FAILURE:
            draft.errors = action.payload.errors;
            draft.loading = false;
            return;
        case types.RESET_CREATE_ROOM:
            return INITAL_STATE;
    }
}, INITAL_STATE);
