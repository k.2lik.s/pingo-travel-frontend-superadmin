import { combineReducers } from "redux";
import { vendorAccount } from "./vendorAccounts";
import { type } from "./type";
import { currentVendor } from "./vendor";
import { features } from "./features";
import { medias } from "./medias";
import { edit } from "./edit";

export default combineReducers({
    vendorAccount,
    type,
    currentVendor,
    medias,
    edit,
    features,
});
