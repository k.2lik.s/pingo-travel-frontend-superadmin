import produce from "immer";
import * as types from "../constants";

const INITAL_STATE = {
    success: false,
    nextStep: false,
    roomId: null,
    loading: false,
    errors: {},
};

export const edit = produce((draft, action) => {
    switch (action.type) {
        case types.EDIT_LOADING:
            draft.loading = true;
            return;
        case types.EDIT_SUCCESS:
            draft.success = true;
            draft.nextStep = true;
            draft.roomId = action.payload.data.id;
            draft.loading = false;
            return;
        case types.EDIT_FAILURE:
            draft.errors = action.payload.errors;
            draft.loading = false;
            return;

        case types.RESET_CREATE_ROOM:
            return INITAL_STATE;
    }
}, INITAL_STATE);
