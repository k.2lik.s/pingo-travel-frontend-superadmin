import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";

import {
    Box,
    Heading,
    FormField,
    TextInput,
    Text,
    TextArea,
    Button,
} from "grommet";

import {
    getVendor,
    getVendorAccounts,
    getVendorTypes
} from "../actions";

import { FormSelect } from "ui/Common/Form/Selects";
import { GrayButton } from "ui/Common/Form/Button";

import Navigation from "../components/Navigation";

import "draft-js/dist/Draft.css";

const Information = () => {
    const { vendor } = useSelector((state) => state.common);
    const dispatch = useDispatch();
    const history = useHistory();

    useEffect(() => {
        if (!vendor.loading) {
            dispatch(getVendorAccounts(vendor.currentVendor.id));
            dispatch(getVendor(vendor.currentVendor.id));
            dispatch(getVendorTypes());
        }
    }, [vendor]);

    const { vendorAccount, type, currentVendor } = useSelector((state) => state.hotel);

    const [informationForm, setInformationForm] = useState({
        types: [],
        bin: '',
        name: '',
        short: '',
        description: '',
        rating: '',
    });

    useEffect(() => {
        setInformationForm({
            types: [],
            bin: vendorAccount?.list?.bin,
            name: currentVendor?.list?.name,
            short: currentVendor?.list?.short,
            description: currentVendor?.list?.description,
            rating: currentVendor?.list?.star_rating,
        })
    }, [vendorAccount, currentVendor]);

    const handleChange = (event) => {
        setInformationForm({
            ...informationForm,
            [event.target.name]: event.target.value,
        });
    };


    return (
        <Box animation={[{ type: "fadeIn", duration: 300 }]}>
            <div className="grid-wrapper">
                <Heading size="large" level={2} margin={{ top: "xsmall" }}>
                    Отель
                </Heading>
                <div className="grid-content">

                    <Box
                        animation={[{ type: "fadeIn", duration: 300 }]}
                        height={{ min: "100%" }}
                        background="#fff"
                    >
                        {!type.loading && !vendorAccount.loading && !currentVendor.loading && !vendor.loading
                            ? <>
                                <Box pad="medium" border={{ side: "bottom", color: "#E0E0E0" }}>
                                    <Heading size="medium" margin="0" level={2}>
                                        Информация
                                    </Heading>
                                </Box>
                                <div className="grid-wrapper--content">
                                    <div className="grid-form-layout">
                                        <Box pad="medium">
                                            <FormField
                                                label={
                                                    <Text>
                                                        БИН
                                                    </Text>
                                                }
                                                htmlFor="bin"
                                            >
                                                <TextInput
                                                    value={informationForm.bin}
                                                    name="bin"
                                                    id="bin"
                                                    disabled
                                                />
                                            </FormField>
                                            <FormField
                                                margin={{ vertical: "32px" }}
                                                label={
                                                    <Text>
                                                        Тип отеля
                                                        <Text color="red"> *</Text>
                                                    </Text>
                                                }
                                                htmlFor="name"
                                            >
                                                <FormSelect
                                                    id="hotel-type"
                                                    labelKey="name"
                                                    valueKey="id"
                                                    value={informationForm.types}
                                                    closeOnChange={false}
                                                    options={type.list}
                                                    onChange={({ value: nextValue }) => {
                                                        setInformationForm({
                                                            ...informationForm,
                                                            types: nextValue,
                                                        });
                                                    }}
                                                />
                                            </FormField>
                                            <FormField
                                                margin={{ bottom: "32px" }}
                                                label={
                                                    <Text>
                                                        Наименование
                                                        <Text color="red"> *</Text>
                                                    </Text>
                                                }
                                                htmlFor="name"
                                            >
                                                <TextInput
                                                    onChange={(e) => handleChange(e)}
                                                    value={informationForm.name}
                                                    name="name"
                                                    id="name"
                                                />
                                            </FormField>
                                            <FormField
                                                label={
                                                    <Text>
                                                        Краткое описание
                                                        <Text color="red"> *</Text>
                                                    </Text>
                                                }
                                                htmlFor="short"
                                            >
                                                <ShortText
                                                    id="short"
                                                    onChange={(e) => handleChange(e)}
                                                    value={informationForm.short}
                                                    name="short"
                                                />
                                            </FormField>
                                            <FormField
                                                margin={{ vertical: "32px" }}
                                                label={
                                                    <Text>
                                                        Описание
                                                        <Text color="red"> *</Text>
                                                    </Text>
                                                }
                                                htmlFor="description"
                                            >
                                                <ShortText
                                                    onChange={(e) => handleChange(e)}
                                                    id="description"
                                                    name="description"
                                                    value={informationForm.description}
                                                />
                                            </FormField>
                                            <FormField
                                                margin={{ bottom: "32px" }}
                                                label={
                                                    <Text>
                                                        Рейтинг
                                                        <Text color="red"> *</Text>
                                                    </Text>
                                                }
                                                htmlFor="rating"
                                            >
                                                <TextInput
                                                    onChange={(e) => handleChange(e)}
                                                    value={informationForm.rating}
                                                    name="rating"
                                                    id="rating"
                                                />
                                            </FormField>
                                        </Box>
                                    </div>
                                </div>
                            </>
                            : null
                        }
                    </Box>
                </div>
                <div className="grid-navigation">
                    <Navigation
                        createSuccess={false}
                        mediaSuccess={false}
                        stepId={"information"}
                    />
                </div>
                <div className="grid-wrapper">
                    <div className="grid-content">
                        <Box justify="between" direction="row" margin={{ top: "medium" }}>
                            <Box width="48%">
                                <GrayButton label="Вернуться" />
                            </Box>
                            <Box width="48%">
                                <Button
                                    onClick={() => history.push('/manager/hotel/edit/gallery')}
                                    primary
                                    // disabled={create.loading}
                                    label="Продолжить"
                                />
                            </Box>
                        </Box>
                    </div>
                </div>
            </div>
        </Box>
  );
};

const ShortText = styled(TextArea)`
  min-height: 130px;
`;

export default Information;
