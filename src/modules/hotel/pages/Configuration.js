import { useEffect, useState,useRef } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
    YMaps,
    Map,
    FullscreenControl,
    GeolocationControl,
    SearchControl,
    ZoomControl,
} from 'react-yandex-maps';
import {
    Heading,
    Box,
    TextInput,
    FormField,
    Text,
    Button
} from "grommet";
import { getVendor } from "../actions";
import { GrayButton } from "ui/Common/Form/Button";
import Navigation from "../components/Navigation";

const Configuration = () => {
    const { vendor } = useSelector((state) => state.common);
    const dispatch = useDispatch();
    const history = useHistory();
    const refYmap = useRef(null);

    useEffect(() => {
        if (!vendor.loading) {
            dispatch(getVendor(vendor.currentVendor.id));
        }
    }, [vendor]);
    const { currentVendor } = useSelector((state) => state.hotel);

    //43.257492, 76.899116
    const [informationForm, setInformationForm] = useState({
        coordinates: {
          lat: 43.257492 ,
          lng:  76.899116,
          zoom: 10,
        },
        website: '',
        phone: '',
        email: '',
        region: '',
        address: '',
    });

    useEffect(() => {
        setInformationForm({
            coordinates: {
                lat: 43.257492 ,
                lng:  76.899116,
                zoom: 10,
            },
            website: currentVendor?.list?.website,
            phone: currentVendor?.list?.phone,
            email: currentVendor?.list?.email,
            region: currentVendor?.list?.region?.name,
            address: currentVendor?.list?.location,
        })
    }, [currentVendor]);

    const clickOnMap = (e) => {
        console.log('map',e.get('coords'))
        console.log('zooom=',refYmap.current._zoom)
        let coords = e.get('coords')
        setInformationForm({
            ...informationForm,
            coordinates: {
                lat: coords[0],
                lng: coords[1],
                zoom: e.sourceEvent?.originalEvent.target.zoom,
            }
        })
    };
    console.log(informationForm.coordinates.zoom)
    console.log(informationForm.coordinates.lat)
    console.log(informationForm.coordinates.lng)


    const handleChange = (event) => {
        setInformationForm({
            ...informationForm,
            [event.target.name]: event.target.value,
        });
    };

    return (
      <Box animation={[{ type: "fadeIn", duration: 300 }]}>
          <div className="grid-wrapper">
              <Heading size="large" level={2} margin={{ top: "xsmall" }}>
                  Отель
              </Heading>
              <div className="grid-content">
                  {{}
                      ? <Box
                          animation={[{ type: "fadeIn", duration: 300 }]}
                          background="#fff"
                          height={{ min: "100%" }}
                      >
                          <Box pad="medium" border={{ side: "bottom", color: "#E0E0E0" }}>
                              <Heading size="medium" margin="0" level={2}>
                                  Контакты
                              </Heading>
                          </Box>
                          <div className="grid-wrapper--content">
                              <div className="grid-form-layout">
                                  <Box pad="medium">
                                      <FormField
                                          label={
                                              <Text>
                                                  Web-сайт
                                              </Text>
                                          }
                                          htmlFor="bin"
                                      >
                                          <TextInput
                                              onChange={(e) => handleChange(e)}
                                              value={informationForm.website}
                                              name="website"
                                              id="website"
                                          />
                                      </FormField>
                                      <FormField
                                          margin={{ vertical: "32px" }}
                                          label={
                                              <Text>
                                                  Телефон
                                                  <Text color="red"> *</Text>
                                              </Text>
                                          }
                                          htmlFor="phone"
                                      >
                                          <TextInput
                                              onChange={(e) => handleChange(e)}
                                              value={informationForm.phone}
                                              name="phone"
                                              id="phone"
                                          />
                                      </FormField>
                                      <FormField
                                          margin={{ bottom: "32px" }}
                                          label={
                                              <Text>
                                                  E-mail
                                                  <Text color="red"> *</Text>
                                              </Text>
                                          }
                                          htmlFor="email"
                                      >
                                          <TextInput
                                              onChange={(e) => handleChange(e)}
                                              value={informationForm.email}
                                              name="email"
                                              id="email"
                                          />
                                      </FormField>
                                      <FormField
                                          label={
                                              <Text>
                                                  Регион
                                                  <Text color="red"> *</Text>
                                              </Text>
                                          }
                                          htmlFor="region"
                                      >
                                          <TextInput
                                              id="region"
                                              onChange={(e) => handleChange(e)}
                                              value={informationForm.region}
                                              name="region"
                                          />
                                      </FormField>
                                      <FormField
                                          margin={{ vertical: "32px" }}
                                          label={
                                              <Text>
                                                  Адрес
                                                  <Text color="red"> *</Text>
                                              </Text>
                                          }
                                          htmlFor="address"
                                      >
                                          <TextInput
                                              onChange={(e) => handleChange(e)}
                                              value={informationForm.address}
                                              id="address"
                                              name="address"
                                          />
                                      </FormField>
                                      <YMaps   >
                                          <Map
                                              width="inherit"
                                              instanceRef={(ref)=> refYmap.current=ref}
                                              onClick={clickOnMap}
                                              defaultState={{ center: [informationForm.coordinates.lat, informationForm.coordinates.lng], zoom: informationForm.coordinates.zoom }}
                                          >
                                              <FullscreenControl />
                                              <GeolocationControl options={{ float: 'left' }} />
                                              <SearchControl options={{ float: 'right' }} />
                                              <ZoomControl options={{ float: 'right' }} />
                                          </Map>
                                      </YMaps>
                                  </Box>
                              </div>
                          </div>
                      </Box>
                      : null
                  }
              </div>
              <div className="grid-navigation">
                  <Navigation
                      createSuccess={true}
                      mediaSuccess={true}
                      stepId={"configuration"}
                  />
              </div>
              <div className="grid-wrapper">
                  <div className="grid-content">
                      <Box justify="between" direction="row" margin={{ top: "medium" }}>
                          <Box width="48%">
                              <GrayButton label="Вернуться" />
                          </Box>
                          <Box width="48%">
                              <Button
                                  onClick={() => history.push('/manager/hotel/edit/gallery')}
                                  primary
                                  // disabled={create.loading}
                                  label="Продолжить"
                              />
                          </Box>
                      </Box>
                  </div>
              </div>
          </div>
      </Box>
  );
};

export default Configuration;
