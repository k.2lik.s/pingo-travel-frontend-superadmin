import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import {
  Heading,
  Grid,
  Box,
  Text,
  Button
} from "grommet";
import styled from "styled-components";
import {
  getVendorMedias,
  postVendorMedia,
  removeMedia,
  getCover,
} from "../actions";
import { GrayButton } from "ui/Common/Form/Button";
import { UploadIcon, PreviewCloseIcon } from "ui/Common/Icon";
import Navigation from "../components/Navigation";

const UploadBox = styled.label`
  border: 0.5px dashed #bdbdbd;
  cursor: pointer;
  border-radius: 8px;
  text-align: center;
`;
const PreviewIconWrapper = styled.div`
  position: absolute;
  right: 12px;
  top: 12px;
  cursor: pointer;
`;

const PreviewCoverBox = styled(Box)`
  min-height: 424px;
  position: relative;
  background-image: ${(props) => `url(${props.src})`};
  background-size: cover;
  background-position: center;
  position: relative;
  ${(props) =>
    props.loading &&
    `  &::after {
    content: "";
    background-color: rgba(255, 255, 255, 0.7);
    position: absolute;
    height: 100%;
    width: 100%;
  }`}
`;

const PreviewGalleryBox = styled(Box)`
  min-height: 220px;
  min-height: 220px;
  position: relative;
  background-image: ${(props) => `url(${props.src})`};
  background-size: cover;
  background-position: center;
  ${(props) =>
    props.loading &&
    `  &::after {
    content: "";
    background-color: rgba(255, 255, 255, 0.7);
    position: absolute;
    height: 100%;
    width: 100%;
  }`}
`;

const Gallery = () => {
  const dispatch = useDispatch();
  const { vendor } = useSelector((state) => state.common);
  const history = useHistory();

  useEffect(() => {
    if (!vendor.loading) {
      dispatch(getVendorMedias(vendor?.currentVendor?.id));
      dispatch(getCover(vendor?.currentVendor?.id));
    }
  }, [vendor]);

  const { medias } = useSelector((state) => state.hotel);

  const handleDeleteImage = ({ mediaId, collection }) => {
    dispatch(removeMedia({
      mediaId,
      collection,
      vendorId: vendor?.currentVendor?.id,
    }));
  }

  const resetFileValue = (event, file) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      event.target.value = null;
    };
  };

  const handleImagesChange = (event) => {
    const file = event.target.files[0];


    dispatch(postVendorMedia({
      collection: "gallery",
      file,
      vendorId: vendor?.currentVendor?.id,
    }));

    resetFileValue(event, file);
  };

  const handleCoverChange = (event, stateKey) => {
    const file = event.target.files[0];
    dispatch(postVendorMedia({
      collection: "cover",
      file,
      vendorId: vendor?.currentVendor?.id,
    }));
    resetFileValue(event, file);
  };
  console.log('id',medias.cover[0]?.id)

  return (
      <Box animation={[{ type: "fadeIn", duration: 300 }]}>
        <div className="grid-wrapper">
          <Heading size="large" level={2} margin={{ top: "xsmall" }}>
            Отель
          </Heading>
          <div className="grid-content">

            <Box
                animation={[{ type: "fadeIn", duration: 300 }]}
                height={{ min: "100%" }}
                background="#fff"
            >
              {{}
                  ? <Box animation={[{ type: "fadeIn", duration: 300 }]} background="#fff">
                    <Box pad="medium" border={{ side: "bottom", color: "#E0E0E0" }}>
                      <Heading size="medium" margin="0" level={2}>
                        Галерея
                      </Heading>
                    </Box>
                    <div className="grid-wrapper--content">
                      <div className="grid-form-layout">
                        <Box pad="medium">
                          <Text>
                            Обложка<Text color="red"> *</Text>
                          </Text>
                          <Text size="small" margin={{ bottom: "xsmall", top: "small" }}>
                            Загрузите обложку из предложенных. Он должен привлекать внимание
                            зрителей и отражать содержание видео.
                          </Text>
                          {medias.cover.length > 0 && (
                              <PreviewCoverBox
                                  src={medias.cover[0].src}
                                  loading={medias.loading}
                                  margin={{ bottom: "small" }}
                              >
                                <PreviewIconWrapper
                                    onClick={() =>
                                        handleDeleteImage({
                                          mediaId: medias.cover[0].id,
                                          collection: "cover",
                                        })
                                    }
                                >
                                  <PreviewCloseIcon width={24} height={24} />
                                </PreviewIconWrapper>
                              </PreviewCoverBox>
                          )}

                          <UploadBox for="upload">
                            <Box direction="column" align="center" pad={{ vertical: "38px" }}>
                              <Box>
                                <UploadIcon width="32" height="32" />
                              </Box>
                              <Box>
                                <Text size="small"> Заменить обложку</Text>
                              </Box>
                              <input
                                  type="file"
                                  id="upload"
                                  onChange={(e) => handleCoverChange(e)}
                                  style={{ display: "none" }}
                              />
                            </Box>
                          </UploadBox>
                          <Box pad={{ vertical: "medium" }}>
                            <Text>
                              Галерея <Text color="red"> *</Text>
                            </Text>
                            <Text size="small" margin={{ bottom: "xsmall", top: "small" }}>
                              Отличные фотографии помогут гостям получить полное представление
                              о вашем объекте. Загрузите изображения хорошего качества,
                              позволяющие увидеть ваш объект со всех сторон. Эти фотографии
                              будут показаны на странице вашего объекта размещения на сайте.
                            </Text>
                            <UploadBox for="upload-multiple">
                              <Box
                                  direction="column"
                                  align="center"
                                  pad={{ vertical: "38px" }}
                              >
                                <Box>
                                  <UploadIcon width="32" height="32" />
                                </Box>
                                <Box>
                                  <Text size="small"> Загрузить фотографии</Text>
                                </Box>
                                <input
                                    type="file"
                                    id="upload-multiple"
                                    multiple
                                    onChange={handleImagesChange}
                                    style={{ display: "none" }}
                                />
                              </Box>
                            </UploadBox>
                            <Grid
                                columns={{
                                  count: 3,
                                  size: "auto",
                                }}
                                gap="small"
                                margin={{ top: "medium" }}
                            >
                              {medias.gallery.map((image) => (
                                  <PreviewGalleryBox loading={medias.loading} src={image.src}>
                                    <PreviewIconWrapper
                                        onClick={() =>
                                            handleDeleteImage({
                                              mediaId: image.id,
                                              collection: "gallery",
                                            })
                                        }
                                    >
                                      <PreviewCloseIcon width={24} height={24} />
                                    </PreviewIconWrapper>
                                  </PreviewGalleryBox>
                              ))}
                            </Grid>
                          </Box>
                        </Box>
                      </div>
                    </div>
                  </Box>
                  : null
              }
            </Box>
          </div>
          <div className="grid-navigation">
            <Navigation
                createSuccess={true}
                mediaSuccess={false}
                stepId={"gallery"}
            />
          </div>
          <div className="grid-wrapper">
            <div className="grid-content">
              <Box justify="between" direction="row" margin={{ top: "medium" }}>
                <Box width="48%">
                  <GrayButton label="Вернуться" />
                </Box>
                <Box width="48%">
                  <Button
                      onClick={() => history.push('/manager/hotel/edit/gallery')}
                      primary
                      label="Продолжить"
                  />
                </Box>
              </Box>
            </div>
          </div>
        </div>
      </Box>
  );
};



export default Gallery;
