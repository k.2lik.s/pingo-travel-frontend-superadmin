import { useHistory } from "react-router-dom";
import {
    Heading,
    Box,
    Text,
    Button,
    Grid,
    CheckBox,
} from "grommet";
import { GrayButton } from "ui/Common/Form/Button";
import Navigation from "../components/Navigation";

const Comfort = () => {
    const history = useHistory();

    return (
        <Box animation={[{ type: "fadeIn", duration: 300 }]}>
            <div className="grid-wrapper">
                <Heading size="large" level={2} margin={{ top: "xsmall" }}>
                    Отель
                </Heading>
                <div className="grid-content">
                    {{}
                        ? <Box
                            animation={[{ type: "fadeIn", duration: 300 }]}
                            background="#fff"
                            height={{ min: "100%" }}
                        >
                            <Box pad="medium" border={{ side: "bottom", color: "#E0E0E0" }}>
                                <Heading size="medium" margin="0" level={2}>
                                    Дополнительные услуги
                                </Heading>
                            </Box>
                            <Box pad="medium">
                                <Box pad={{ bottom: "medium" }}>
                                    <Text size="medium">
                                        <b>Расскажите нам о своих удобствах</b>
                                    </Text>
                                </Box>
                                <Grid
                                    columns={{
                                        count: 2,
                                        size: "auto",
                                    }}
                                    gap={{ column: "medium" }}
                                    alignContent="center"
                                >
                                    <Box
                                        pad={{ vertical: "medium" }}
                                        border={{ side: "bottom", color: "#E0E0E0" }}
                                    >
                                        <CheckBox
                                            size="xsmall"
                                            label={<Text size="small">Бесплатный Wi-Fi</Text>}
                                        />
                                    </Box>
                                    <Box
                                        pad={{ vertical: "medium" }}
                                        border={{ side: "bottom", color: "#E0E0E0" }}
                                    >
                                        <CheckBox
                                            size="xsmall"
                                            label={<Text size="small">Бесплатный Wi-Fi</Text>}
                                        />
                                    </Box>
                                    <Box
                                        pad={{ vertical: "medium" }}
                                        border={{ side: "bottom", color: "#E0E0E0" }}
                                    >
                                        <CheckBox
                                            size="xsmall"
                                            label={<Text size="small">Бесплатный Wi-Fi</Text>}
                                        />
                                    </Box>
                                    <Box
                                        pad={{ vertical: "medium" }}
                                        border={{ side: "bottom", color: "#E0E0E0" }}
                                    >
                                        <CheckBox
                                            size="xsmall"
                                            label={<Text size="small">Бесплатный Wi-Fi</Text>}
                                        />
                                    </Box>
                                    <Box
                                        pad={{ vertical: "medium" }}
                                        border={{ side: "bottom", color: "#E0E0E0" }}
                                    >
                                        <CheckBox
                                            size="xsmall"
                                            label={<Text size="small">Бесплатный Wi-Fi</Text>}
                                        />
                                    </Box>
                                    <Box
                                        pad={{ vertical: "medium" }}
                                        border={{ side: "bottom", color: "#E0E0E0" }}
                                    >
                                        <CheckBox
                                            size="xsmall"
                                            label={<Text size="small">Бесплатный Wi-Fi</Text>}
                                        />
                                    </Box>
                                    <Box
                                        pad={{ vertical: "medium" }}
                                        border={{ side: "bottom", color: "#E0E0E0" }}
                                    >
                                        <CheckBox
                                            size="xsmall"
                                            label={<Text size="small">Бесплатный Wi-Fi</Text>}
                                        />
                                    </Box>
                                    <Box
                                        pad={{ vertical: "medium" }}
                                        border={{ side: "bottom", color: "#E0E0E0" }}
                                    >
                                        <CheckBox
                                            size="xsmall"
                                            label={<Text size="small">Бесплатный Wi-Fi</Text>}
                                        />
                                    </Box>
                                    <Box
                                        pad={{ vertical: "medium" }}
                                        border={{ side: "bottom", color: "#E0E0E0" }}
                                    >
                                        <CheckBox
                                            size="xsmall"
                                            label={<Text size="small">Бесплатный Wi-Fi</Text>}
                                        />
                                    </Box>
                                    <Box
                                        pad={{ vertical: "medium" }}
                                        border={{ side: "bottom", color: "#E0E0E0" }}
                                    >
                                        <CheckBox
                                            size="xsmall"
                                            label={<Text size="small">Бесплатный Wi-Fi</Text>}
                                        />
                                    </Box>
                                </Grid>
                            </Box>
                        </Box>
                        : null
                    }
                </div>
                <div className="grid-navigation">
                    <Navigation
                        createSuccess
                        mediaSuccess
                        configurationSuccess
                        priceSuccess
                        featureSuccess
                        stepId={"comfort"}
                    />
                </div>
                <div className="grid-wrapper">
                    <div className="grid-content">
                        <Box justify="between" direction="row" margin={{ top: "medium" }}>
                            <Box width="48%">
                                <GrayButton label="Вернуться" />
                            </Box>
                            <Box width="48%">
                                <Button
                                    onClick={() => history.push('/manager/hotel/edit/comfort')}
                                    primary
                                    // disabled={create.loading}
                                    label="Продолжить"
                                />
                            </Box>
                        </Box>
                    </div>
                </div>
            </div>
        </Box>
    );
};

export default Comfort;
