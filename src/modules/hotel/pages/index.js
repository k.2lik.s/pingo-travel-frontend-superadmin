import HotelEditInformation from "./Information";
import HotelEditGallery from "./Gallery";
import HotelEditContacts from "./Configuration";
import HotelEditConfiguration from "./Price";
import HotelEditFeatures from "./Features";
import HotelEditComfort from "./Comfort";

const HotelNavigation = [
    {
        exact: true,
        path: "/hotel/edit/information",
        name: "Отель",
        icon: "hotel",
        layout: "/manager",
        component: HotelEditInformation,
    },
    {
      exact: true,
      redirect: true,
      path: "/hotel/edit/gallery",
      name: "Отель",
      icon: "hotel",
      layout: "/manager",
      component: HotelEditGallery,
    },
    {
      exact: true,
      redirect: true,
      path: "/hotel/edit/configuration",
      name: "Отель",
      icon: "hotel",
      layout: "/manager",
      component: HotelEditContacts,
    },
    {
      exact: true,
      redirect: true,
      path: "/hotel/edit/price",
      name: "Отель",
      icon: "hotel",
      layout: "/manager",
      component: HotelEditConfiguration,
    },
    {
      exact: true,
      redirect: true,
      path: "/hotel/edit/features",
      name: "Отель",
      icon: "hotel",
      layout: "/manager",
      component: HotelEditFeatures,
    },
    {
      exact: true,
      redirect: true,
      path: "/hotel/edit/comfort",
      name: "Отель",
      icon: "hotel",
      layout: "/manager",
      component: HotelEditComfort,
    }
];

export default HotelNavigation;
