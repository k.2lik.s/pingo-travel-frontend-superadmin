import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import {
    Heading,
    Box,
    FormField,
    Text,
    Form as GrommetForm,
    Button,
} from "grommet";
import { getFeatures } from "../actions";
import { GrayButton } from "ui/Common/Form/Button";
import Checkbox from "../../../ui/Common/Form/Checkbox";
import Navigation from "../components/Navigation";

const Features = () => {
    // const [clickFeatures, setClickFeatures] = useState();
    const [firstArrayOfFeatures, setFirstArrayOfFeatures] = useState([]);
    const [secondArrayOfFeatures, setSecondArrayOfFeatures] = useState([]);
    const { vendor } = useSelector((state) => state.common);
    const dispatch = useDispatch();
    const history = useHistory();

    useEffect(() => {
        if (!vendor.loading) {
            dispatch(getFeatures());
        }
    }, [vendor]);

    const { features } = useSelector((state) => state.hotel);

    useEffect(() => {
        if (!features.loading) {
            setFirstArrayOfFeatures(features.list.slice(0, features.list.length / 2));
            setSecondArrayOfFeatures(features.list.slice(features.list.length / 2));
        }
    },[features]);

    return (
        <Box animation={[{ type: "fadeIn", duration: 300 }]}>
            <div className="grid-wrapper">
                <Heading size="large" level={2} margin={{ top: "xsmall" }}>
                    Отель
                </Heading>
                <div className="grid-content">
                    {{}
                        ? <Box
                            animation={[{ type: "fadeIn", duration: 300 }]}
                            background="#fff"
                            height={{ min: "100%" }}
                        >
                            <Box pad="medium" border={{ side: "bottom", color: "#E0E0E0" }}>
                                <Heading size="medium" margin="0" level={2}>
                                    Удобства
                                </Heading>
                            </Box>
                            <Box pad="medium">
                                <Box pad={{ bottom: "medium" }}>
                                    <Text size="medium">
                                        <b>Расскажите нам о своих особенностях </b>
                                        <Text color="red">*</Text>
                                    </Text>
                                </Box>
                                <GrommetForm
                                    onSubmit={({ value: values, touched }) =>
                                        // 'touched' is a single boolean value indication of
                                        // whether any of the checkboxes had changed.
                                        console.log("Submit", values, touched)
                                    }
                                >
                                    <FormField name="controlled">
                                        <Box direction="row" justify="between">
                                            <Box width="50%">
                                                {firstArrayOfFeatures.map((feature) => (
                                                    <Checkbox
                                                        icon={feature.icon}
                                                        label={feature.name}
                                                        id={feature.id}
                                                    />
                                                ))}
                                            </Box>
                                            <Box width="50%">
                                                {secondArrayOfFeatures.map((feature) => (
                                                    <Checkbox
                                                        icon={feature.icon}
                                                        label={feature.name}
                                                        id={feature.id}
                                                    />
                                                ))}
                                            </Box>
                                        </Box>
                                    </FormField>
                                </GrommetForm>
                            </Box>
                        </Box>
                        : null
                    }
                </div>
                <div className="grid-navigation">
                    <Navigation
                        createSuccess
                        mediaSuccess
                        configurationSuccess
                        priceSuccess
                        stepId={"features"}
                    />
                </div>
                <div className="grid-wrapper">
                    <div className="grid-content">
                        <Box justify="between" direction="row" margin={{ top: "medium" }}>
                            <Box width="48%">
                                <GrayButton label="Вернуться" />
                            </Box>
                            <Box width="48%">
                                <Button
                                    onClick={() => history.push('/manager/hotel/edit/comfort')}
                                    primary
                                    // disabled={create.loading}
                                    label="Продолжить"
                                />
                            </Box>
                        </Box>
                    </div>
                </div>
            </div>
        </Box>
    );
};

export default Features;
