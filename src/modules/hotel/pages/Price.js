import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import styled from "styled-components";
import {
  Heading,
  Box,
  FormField,
  Text,
  RangeInput,
  TextArea,
  Button,
} from "grommet";
import { getVendor } from "../actions";
import { GrayButton } from "ui/Common/Form/Button";
import Navigation from "../components/Navigation";

const Price = () => {
  const [value, setValue] = useState(720);
  const [value2, setValue2] = useState(720);
  const [policy, setPolicy] = useState('');

  const { vendor } = useSelector((state) => state.common);
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    if (!vendor.loading) {
      dispatch(getVendor(vendor.currentVendor.id));
    }
  }, [vendor]);
  const { currentVendor } = useSelector((state) => state.hotel);

  useEffect(() => {
    if (!currentVendor.loading) {
      setPolicy(currentVendor?.list?.policy)
      setValue((currentVendor?.list?.check_in.substring(0,2) * 60) + +currentVendor?.list?.check_in.substring(3,5));
      setValue2((currentVendor?.list?.check_out.substring(0,2) * 60) + +currentVendor?.list?.check_out.substring(3,5));
    }
  }, [currentVendor]);

  const handleChangeTime = (value) => {
    let hour = Math.floor(value/60);
    let minutes = value - (hour*60);
    if (hour < 10) {
      hour = `0${hour}`
    }
    if (minutes < 10) {
      minutes = `0${minutes}`
    }
    return `${hour}:${minutes}`
  };

  return (
      <Box animation={[{ type: "fadeIn", duration: 300 }]}>
        <div className="grid-wrapper">
          <Heading size="large" level={2} margin={{ top: "xsmall" }}>
            Отель
          </Heading>
          <div className="grid-content">
            {{}
                ? <Box
                    animation={[{ type: "fadeIn", duration: 300 }]}
                    background="#fff"
                    height={{ min: "100%" }}
                >
                  <Box pad="medium" border={{ side: "bottom", color: "#E0E0E0" }}>
                    <Heading size="medium" margin="0" level={2}>
                      Условия размещения
                    </Heading>
                  </Box>
                  <div className="grid-form-layout">
                    <Box pad="medium">
                      <Box direction="row" align="center" justify="between">
                        <TimeBox
                            border={{ color: '#BDBDBD', size: '0.5px', style: 'solid', radius: '8px' }}
                            pad={{ horizontal:'40px', vertical:'25px'}}
                            width="400px"
                            align="center"
                        >
                          <TimeBox
                              border={{ radius: '8px' }}
                              background="#F4F4F4"
                              align="center"
                              pad={{ vertical:'12px', horizontal:'16px' }}
                              margin={{ bottom:'16px' }}
                              width="96px"
                          >
                            <Text>{handleChangeTime(value)}</Text>
                          </TimeBox>
                          <TimeRangeInput
                              value={value}
                              step={5}
                              onChange={event => setValue(event.target.value)}
                              max={1439}
                              min={0}
                          />
                          <Box width="100%" direction="row" justify="between">
                            <TimeText>00:00</TimeText>
                            <TimeText>23:59</TimeText>
                          </Box>
                        </TimeBox>
                        <TimeBox
                            border={{ color: '#BDBDBD', size: '0.5px', style: 'solid', radius: '8px' }}
                            pad={{ horizontal:'40px', vertical:'25px' }}
                            width="400px"
                            align="center"
                        >
                          <TimeBox
                              border={{ radius: '8px' }}
                              background="#F4F4F4"
                              align="center"
                              pad={{ vertical:'12px', horizontal:'16px' }}
                              margin={{ bottom:'16px' }}
                              width="96px"
                          >
                            <Text>{handleChangeTime(value2)}</Text>
                          </TimeBox>
                          <TimeRangeInput
                              value={value2}
                              onChange={event => setValue2(event.target.value)}
                              step={5}
                              max={1439}
                              min={0}
                          />
                          <Box width="100%" direction="row" justify="between">
                              <TimeText>00:00</TimeText>
                              <TimeText>23:59</TimeText>
                          </Box>
                        </TimeBox>
                      </Box>
                      <FormField
                          // error={errors.short && errors.short[0]}
                          margin={{ vertical: "32px" }}
                          label={
                            <Text>
                              Порядок проживания в отеле
                              <Text color="red"> *</Text>
                            </Text>
                          }
                          htmlFor="short"
                      >
                        <CustomTextArea
                            id="short"
                            onChange={(e) => setPolicy(e.target.value)}
                            // value={informationForm.short}
                            name="short"
                        />
                      </FormField>
                    </Box>
                  </div>
                </Box>
                : null
            }
          </div>
          <div className="grid-navigation">
            <Navigation
                createSuccess
                mediaSuccess
                configurationSuccess
                stepId={"price"}
            />
          </div>
          <div className="grid-wrapper">
            <div className="grid-content">
              <Box justify="between" direction="row" margin={{ top: "medium" }}>
                <Box width="48%">
                  <GrayButton label="Вернуться" />
                </Box>
                <Box width="48%">
                  <Button
                      onClick={() => history.push('/manager/hotel/edit/features')}
                      primary
                      // disabled={create.loading}
                      label="Продолжить"
                  />
                </Box>
              </Box>
            </div>
          </div>
        </div>
      </Box>
  );
};

const TimeRangeInput = styled(RangeInput)`
  &::-webkit-slider-runnable-track {
    width: 100%;
    height: 7px;
    background: linear-gradient(to bottom, #519CF8, #519CF8) 100% 50% / 100% 4px no-repeat transparent;
    border-radius: 2px;
  }
  &::-webkit-slider-thumb {
    &:hover {
      box-shadow: 0 0 8px rgba(0, 0, 0, 0.1);
      background: #FFFFFF;
    }
    background: #519CF8;
    border: 2px solid #FFFFFF;
    box-sizing: border-box;
    box-shadow: 0 0 8px rgba(0, 0, 0, 0.1);
  }
`;

const TimeText = styled(Text)`
  font-style: normal;
  font-weight: 600;
  font-size: 10px;
  line-height: 12px;
  color: #333333;
  margin: 0;
`;

const TimeBox = styled(Box)`
  border-radius: 8px;
`;

const CustomTextArea = styled(TextArea)`
  min-height: 290px;
`;

export default Price;
