import { useEffect, useState } from "react";
import {
  Heading,
  Box,
  Text,
  TableHeader,
  TableCell,
  Chart,
  TableBody,
  Stack,
} from "grommet";
import { TableRow as GrommetTableRow } from "grommet/components/TableRow";
import { Table as GrommetTable } from "grommet/components/Table";
import { useDispatch, useSelector } from "react-redux";
import { useRouteMatch, withRouter } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { format } from "date-fns";
import styled from "styled-components";
import {
  MainBalanceIcon,
  ReserveBalanceIcon,
} from "ui/Common/Icon";
import {
  getOrders,
  getAmountAllRooms,
  getAmountFreeRooms,
  getAmountOccupiedRooms,
  getAmountInactiveRooms,
} from "modules/sales/actions";
import { calcs } from "../calcs";

const Dashboard = ({ history }) => {

  const { t } = useTranslation('common');
  let { url } = useRouteMatch();
  const [values, setValues] = useState([]);
  const [xAxis, setXAxis] = useState();
  const [bounds, setBounds] = useState();
  const [yAxis, setYAxis] = useState();
  const currencyFormatter = Intl.NumberFormat('ru-RU');
  const { balance } = useSelector((state) => state.balance);
  const { vendor } = useSelector((state) => state.common);
  const { amountOfRooms, order } = useSelector((state) => state.order);

  const dispatch = useDispatch();

  useEffect(() => {
    if (!vendor.loading) {
      dispatch(getOrders(
          vendor.currentVendor.id,
          1,
          5,
          "",
          "",
          format(new Date("2019-07-31T15:27:42.920Z"), 'dd.MM.yyyy'),
          format(new Date("2023-08-07T15:27:42.920Z"), 'dd.MM.yyyy'),
      ));
      dispatch(getAmountAllRooms(vendor.currentVendor.id));
      dispatch(getAmountFreeRooms(vendor.currentVendor.id));
      dispatch(getAmountOccupiedRooms(vendor.currentVendor.id));
      dispatch(getAmountInactiveRooms(vendor.currentVendor.id));
    }
  }, [vendor]);

  const orderStatus = (status) => {
    if (status === 'expired') {
      return 'Время ожидания истекло'
    } else if (status === 'awaiting') {
      return "Ожидание оплаты"
    } else if (status === 'failed') {
      return "Отклонено"
    } else if (status === 'completed') {
      return "Завершено"
    } else if (status === 'refunded') {
      return "Возврат"
    } else {
      return " "
    }
  };

  const paymentState = (status) => {
    if (status === 'declined') {
      return 'Отклонено'
    } else if (status === 'completed') {
      return 'Завершено'
    } else {
      return "-"
    }
  };

  useEffect(() => {
    if(order) {
      console.log(order);
      const charValue = order.list.map((item) =>  ({value: [new Date(item?.created_at).getTime(), item.total_paid] }));
      console.log(charValue);
      setValues(charValue);
    }
  }, [order])

  useEffect(() => {
    if(values.length > 0) {
      const { bounds } = calcs(values, { coarseness: 5, steps: [3, 3] });
      const xAxis = values.map(x =>
          format(new Date(x.value[0]), 'dd MMM')
      );
      const yAxis = values.map(y => `${y.value[1]} k`);
      setXAxis(xAxis);
      setYAxis(yAxis);
      setBounds(bounds);
    }
  }, [values])
  return (
    <Box animation={[{ type: "fadeIn", duration: 300 }]}>
      <Heading size="large" level={2} margin={{ top: "xsmall" }}>
        Главная
      </Heading>
      <Box
        direction="row"
        align="top"
      >
        <HeaderBox
          background="#fff"
          justify="between"
          direction="row"
          align="center"
          pad="32px"
          width="24%"
          margin={{right: '32px'}}
          onClick={() => history.push(`${url}/manager/balance`)}
          type="main"
        >
          <Box
            direction="row"
            align="center"
          >
            <MainBalanceIcon height="48" width="48" />
            <Box margin={{left:'16px'}}>
              <HeaderBoxTitle>
                Основной баланс
              </HeaderBoxTitle>
              <HeaderBoxValue>
                {`${currencyFormatter.format(balance.list[0]?.balance)} KZT`}
              </HeaderBoxValue>
            </Box>
          </Box>
        </HeaderBox>
        <HeaderBox
          background="#fff"
          justify="between"
          direction="row"
          align="center"
          pad="32px"
          width="24%"
          margin={{right: '32px'}}
          onClick={() => history.push(`${url}/manager/balance`)}
          type="reserve"
        >
          <Box
            direction="row"
            align="center"
          >
            <ReserveBalanceIcon height="51" width="60" />
            <Box margin={{left:'16px'}}>
              <HeaderBoxTitle>
                Резервный баланс
              </HeaderBoxTitle>
              <HeaderBoxValue>
                {`${currencyFormatter.format(balance?.list[1]?.balance)} KZT`}
              </HeaderBoxValue>
            </Box>
          </Box>
        </HeaderBox>
        <HeaderBox
          background="#fff"
          pad="32px"
          width="24%"
          justify="center"
          align="center"
          margin={{right: '32px'}}
          type="payment"
          onClick={() => history.push(`${url}/manager/balance`)}
        >
          <PaymentRequest>
              {t('common:payment_request')}
          </PaymentRequest>
        </HeaderBox>
        <HeaderBox
          background="#fff"
          pad="32px"
          width="24%"
          justify="center"
          align="center"
          type="room"
          onClick={() => history.push(`${url}/manager/rooms`)}
        >
          <PaymentRequest>
            Добавить комнату
          </PaymentRequest>
        </HeaderBox>
      </Box>
      <Box
          justify="between"
          direction="row"
          align="top"
          margin={{"bottom": "28px", "top" : "32px"}}
      >
        <Box background="#fff" justify="between" pad="32px" width="23%">
          <Text
              size="xlarge"
              history
              weight="bold"
          >
            {amountOfRooms?.amountOfAllRooms?.total}
          </Text>
          <RoomsAvailabilityTitle>
            Всего комнат
          </RoomsAvailabilityTitle>
        </Box>
        <Box background="#fff" justify="between" pad="32px" width="23%">
          <Text
              size="xlarge"
              weight="bold"
          >
            {amountOfRooms?.amountOfFreeRooms?.total}
          </Text>
          <RoomsAvailabilityTitle>
            Доступные комнаты
          </RoomsAvailabilityTitle>
        </Box>
        <Box background="#fff" justify="between" pad="32px" width="23%">
          <Text
              size="xlarge"
              weight="bold"
          >
            {amountOfRooms?.amountOfOccupiedRooms?.total}
          </Text>
          <RoomsAvailabilityTitle>
            Забронированные комнаты
          </RoomsAvailabilityTitle>
        </Box>
        <Box background="#fff" justify="between" pad="32px" width="23%">
          <Text
              size="xlarge"
              weight="bold"
          >
            {amountOfRooms?.amountOfInactiveRooms?.total}
          </Text>
          <RoomsAvailabilityTitle>
            Недоступные комнаты
          </RoomsAvailabilityTitle>
        </Box>
      </Box>
      <Box direction="row" justify="between" >
        {xAxis && yAxis && values.length > 0 && <Box background="#fff" width="48%">
          <Box align="center" pad="large">
            <Box direction="row">
              <Box justify="between" pad={{"top": "48px"}} >
                {yAxis.map((y, index) => {
                  const first = index === 0;
                  const last = index === yAxis.length - 1 && !first;
                  let align;
                  if (first) {
                    align = "start";
                  } else if (last) {
                    align = "end";
                  } else {
                    align = "center";
                  }
                  return (
                      <Box key={y} direction="row" align={align}>
                        <Box pad={{ horizontal: "small" }}>
                          <Text>{y}</Text>
                        </Box>
                      </Box>
                  );
                })}
              </Box>
              <Box>
                <Box
                    direction="row"
                    justify="between"
                    width="medium"
                    margin={{ vertical: "small" }}
                >
                  {xAxis.map(x => (
                      <Text key={x}>{x}</Text>
                  ))}
                </Box>
                <Stack guidingChild="first">
                  <Chart
                      values={values}
                      bounds={bounds}
                      aria-label="chart"
                      type="bar"
                  />
                  <Box fill justify="between">
                    {yAxis.map((y, index) => {
                      const first = index === 0;
                      const last = index === yAxis.length - 1 && !first;
                      let align;
                      if (first) {
                        align = "start";
                      } else if (last) {
                        align = "end";
                      } else {
                        align = "center";
                      }
                      return (
                          <Box key={y} direction="row" align={align}>
                            <Box border="top" bounds="dashed" flex />
                          </Box>
                      );
                    })}
                  </Box>
                </Stack>
              </Box>
            </Box>
          </Box>
        </Box>}

        <Box background="#fff" width="48%" direcion="column">
          <Table>
            <TableHeader>
              <TableCell align="start">
                <Text>№ заказа</Text>
              </TableCell>
              <TableCell align="start">
                <Text>Дата бронирования</Text>
              </TableCell>
              <TableCell align="start">
                <Text>Стоимость</Text>
              </TableCell>
              <TableCell align="start">
                <Text>Статус заказа</Text>
              </TableCell>
              <TableCell align="start">
                <Text>Статус оплаты</Text>
              </TableCell>
            </TableHeader>
            <TableBody>
              {order.list.map((item,i) => (
                  <TableRow
                      key={item.id}
                      onClick={() => history.push(`${url}/detail/${item.id}`, {
                        id: item.id,
                      })}
                  >
                    <TableCell>
                      <Text>{`#${item?.order_number}`}</Text>
                    </TableCell>
                    <TableCell align="center">
                      <Text>
                        {item.created_at
                            ? format(new Date(item.created_at), 'dd.MM.yyyy')
                            : null
                        }
                      </Text>
                    </TableCell>
                    <TableCell align="center">
                      <Text>{`${currencyFormatter.format(item?.total_paid)} KZT`}</Text>
                    </TableCell>
                    <TableCell align="center">
                      <Text>{orderStatus(item?.status)}</Text>
                    </TableCell>
                    <TableCell align="center">
                      <StatusText status={paymentState(item?.active_purchase?.status)}>
                        {paymentState(item?.active_purchase?.status)}
                      </StatusText>
                    </TableCell>

                  </TableRow>
              ))}
            </TableBody>
          </Table>
        </Box>
      </Box>
    </Box>
  );
};

const HeaderBox = styled(Box)`
  border-radius: 16px;
  height: 136px;
  background: ${({type}) => {
    switch (type) {
      case 'main':
        return "linear-gradient(96.34deg, #65A7FF 0%, #2F80ED 100%), #2F80ED";
      case 'reserve':
        return "linear-gradient(96.34deg, #FFAF67 0%, #F2994A 100%), #F2994A";
      case 'payment':
        return "#219653";
      case 'room':
        return "#2F80ED";
      default:
        return null;
    }
  }};
  box-shadow: 0 4px 6px rgba(18, 132, 247, 0.04);
`;

const HeaderBoxTitle = styled(Text)`
  font-weight: normal;
  font-size: 14px;
  line-height: 16px;
  margin: 0;
  color: #FFFFFF;
  padding-bottom: 8px;
`;

const HeaderBoxValue = styled(Text)`
  font-weight: bold;
  font-size: 24px;
  line-height: 32px;
  margin: 0;
  color: #FFFFFF;
`;

const PaymentRequest = styled(Text)`
  font-weight: bold;
  font-size: 24px;
  line-height: 32px;
  margin: 0;
  color: #FFFFFF;
`;

// sales style

const RoomsAvailabilityTitle = styled(Text)`
  font-style: normal;
  font-weight: 600;
  font-size: 18px;
  line-height: 24px;
  color: #BDBDBD;
  padding-top: 8px;
  margin: 0;
`;

const TableRow = styled(GrommetTableRow)`
  &:hover {
    background: rgba(47, 128, 237, 0.1);
    cursor: pointer;
  }
`;

const Table = styled(GrommetTable)`
  table-layout: fixed;
  width: 100%;
`;

const StatusText = styled(Text)`
  color: ${(props) => {
  if(props.status === 'Завершено') {
    return "#219653"
  } else if(props.status === 'Отклонено') {
    return "#F2994A"
  } else if(props.status === 'Отменено') {
    return "#EB5757"
  }
}};
`;

export default withRouter(Dashboard);
