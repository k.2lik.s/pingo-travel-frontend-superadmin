import { useState } from 'react';
import {
    Box,
    TableCell,
    Text,
    TableRow as GrommetTableRow,
    Button,
    Accordion,
    AccordionPanel,
} from "grommet";
import styled from "styled-components";
import Refund from "ui/Common/Form/Refund";
import { ArrowUpIcon, ArrowDownIcon } from "ui/Common/Icon";

const SalesDetail = ({item, i, hide}) => {
    const [status, setStatus] = useState(false);
    const currencyFormatter = Intl.NumberFormat('ru-RU');

    return (
        <TableRow
            key={i}
        >
            {/*<Accordion animate={true} multiple={true}>*/}

                <TableCell
                    background="#FBFBFB"
                    pad="none"
                    align="center"
                >
                    <Box pad={{top:'10px', bottom:'10px'}}>
                        <Text>
                            {item?.bookable
                                ? item?.bookable?.room_number
                                : "-"
                            }
                        </Text>
                    </Box>
                    {/*<Accordion animate={true} >*/}
                    {/*    <AccordionPanel>*/}
                    {/*        {" "}*/}
                    {/*    </AccordionPanel>*/}
                    {/*</Accordion>*/}
                    {/*{status*/}
                    {/*    ? <CustomBox>{' '}</CustomBox>*/}
                    {/*    : null*/}
                    {/*}*/}
                </TableCell>
                <TableCell background="#FBFBFB" pad="none" align="center">
                    <Box pad={{top:'10px', bottom:'10px'}}>
                        <Text>
                            {item?.bookable
                                ? item?.bookable?.room.name
                                : "-"
                            }
                        </Text>
                    </Box>
                    {/*{status*/}
                    {/*    ? <CustomBox>{' '}</CustomBox>*/}
                    {/*    : null*/}
                    {/*}*/}
                </TableCell>
                <TableCell background="#FBFBFB" pad="none" align="center">
                    <Box pad={{top:'10px', bottom:'10px'}}>
                        <Text>
                            {item?.bookable
                                ? `${currencyFormatter.format(item?.bookable?.room.price)} тг`
                                : "-"
                            }
                        </Text>
                    </Box>
                    {/*{status*/}
                    {/*    ? <CustomBox>{' '}</CustomBox>*/}
                    {/*    : null*/}
                    {/*}*/}
                </TableCell>
                <TableCell
                    background="#FBFBFB"
                    pad="none"
                    align="center"
                    size="medium"
                >
                    <Box pad={{top:'10px', bottom:'10px'}}>
                        {item.children.length > 0
                            ? <RevealButton
                                onClick={() => {
                                    setStatus(!status);
                                }}
                            >
                                {status ? 'Закрыть' : 'Открыть'}
                                {status
                                    ? <ArrowUpIcon width={12} height={16} />
                                    : <ArrowDownIcon width={12} height={16} />
                                }
                            </RevealButton>
                            : "-"
                        }
                    </Box>
                    {/*<Accordion animate={true} openMulti={true}>*/}
                    {/*    <AccordionPanel>*/}
                    {/*        {item.additional_service.map((service) => (*/}
                    {/*            <Box*/}
                    {/*                justify="between"*/}
                    {/*                direction="row"*/}
                    {/*                width="inherit"*/}
                    {/*                pad="10px"*/}
                    {/*            >*/}
                    {/*                <TitleText>{`${service.key}: `}</TitleText>*/}
                    {/*                <CostText>{`${service.value} KZT`}</CostText>*/}
                    {/*            </Box>*/}
                    {/*        ))}*/}
                    {/*    </AccordionPanel>*/}
                    {/*</Accordion>*/}
                    {item.children.length > 0 && status
                        ? <CustomBox>
                            {item.children.map((service) => (
                                <Box
                                    justify="between"
                                    direction="row"
                                    width="inherit"
                                    pad="10px"
                                >
                                    <TitleText>{`${service.bookable.name}:`}</TitleText>
                                    <CostText>{`${service.bookable.price} KZT`}</CostText>
                                </Box>
                            ))}
                        </CustomBox>
                        : null
                    }
                </TableCell>
                <TableCell background="#FBFBFB" pad="none" align="center">
                    <Box pad={{top:'10px', bottom:'10px'}}>
                        <TransactionCost>
                            {`${currencyFormatter.format(item?.price_paid)} KZT`}
                        </TransactionCost>
                    </Box>
                    {/*{status*/}
                    {/*    ? <CustomBox>{' '}</CustomBox>*/}
                    {/*    : null*/}
                    {/*}*/}
                </TableCell>
                {/*{item?.refund*/}
                {/*    ? <TableCell background="#FBFBFB" pad="none" align="center">*/}
                {/*        <Box pad={{top:'10px', bottom:'10px'}}>*/}
                {/*            <Refund*/}
                {/*                name={item?.refund}*/}
                {/*                modal={hide}*/}
                {/*            />*/}
                {/*        </Box>*/}
                {/*        /!*{status*!/*/}
                {/*        /!*    ? <CustomBox>{' '}</CustomBox>*!/*/}
                {/*        /!*    : null*!/*/}
                {/*        /!*}*!/*/}
                {/*    </TableCell>*/}
                {/*    : null*/}
                {/*}*/}

        </TableRow>
    );
};

const TransactionCost = styled(Text)`
  font-weight: bold;
  font-size: 18px;
  line-height: 24px;
  margin: 0;
  color: #219653;
`;

const RevealButton = styled(Button)`
  background: rgba(47, 128, 237, 0.1);
  border-radius: 8px;
  min-width: 101px;
  min-height: 32px;
  color: #2F80ED;
  font-weight: normal;
  font-size: 14px;
  line-height: 16px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 10px;
`;

const CustomBox = styled(Box)`
  max-width: 100%;
  min-height: inherit;
  height: auto;
  background: #FBFBFB;
  text-align: left;
`;

const TitleText = styled(Text)`
  font-weight: normal;
  font-size: 16px;
  line-height: 24px;
  color: #333333;
  margin: 0;
  padding-right: 8px;
`;

const TableRow = styled(GrommetTableRow)`
  background: #ffffff;
`;

const CostText = styled(Text)`
  font-size: 16px;
  line-height: 24px;
  font-weight: bold;
  color: #2F80ED;
  margin: 0;
`;

export default SalesDetail;