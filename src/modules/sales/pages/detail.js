import { useEffect, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { withRouter, useHistory } from "react-router-dom";
import { format, isBefore } from "date-fns";
import styled from "styled-components";
import {
    Box,
    Heading,
    Text,
    Table,
    TableHeader,
    TableCell,
    TableBody,
    TableRow,
    Button,
    TableFooter,
    TextInput,
} from "grommet";
import {
    getOrderDetail,
    getOrderDetailPurchase,
    getOrderDetailBookings,
    getOrderRefund,
} from "modules/sales/actions";
import SalesDetail from "../components/salesDetail";
import useModal from "ui/Common/Modal/useModal";
import Modal from "ui/Common/Modal";


const Detail = () => {
    const history = useHistory();
    const { vendor } = useSelector((state) => state.common);
    const { orderDetail } = useSelector((state) => state.order);
    const { orderPurchases } = useSelector((state) => state.order);
    const { orderBookings } = useSelector((state) => state.order);
    const { orderRefund } = useSelector((state) => state.order);
    const dispatch = useDispatch();

    useEffect(() => {
        if (!vendor.loading) {
            dispatch(getOrderDetail(history.location.state.id));
            dispatch(getOrderDetailPurchase(history.location.state.id));
            dispatch(getOrderDetailBookings(history.location.state.id));
            // dispatch(getOrderRefund(history.location.state.id));
        }
    }, [vendor, history.location.state.id]);

    const sendRefundGet = useCallback(() => {
        dispatch(getOrderRefund(history.location.state.id));
        toggle1();
    }, [history.location.state.id]);

    const currencyFormatter = Intl.NumberFormat('ru-RU');

    // const [isShowing, toggle] = useModal();
    const [isShowing1, toggle1] = useModal();
    const [isShowing2, toggle2] = useModal();
    const hide = () => {
        // toggle();
        toggle1();
    };

    const paymentState = (status) => {
        if (status === 'declined') {
            return 'Отклонено'
        } else if (status === 'completed') {
            return 'Завершено'
        } else {
            return "-"
        }
    };

    const orderStatus = (status) => {
        if (status === 'expired') {
            return 'Время ожидания истекло'
        } else if (status === 'awaiting') {
            return "Ожидание оплаты"
        } else if (status === 'failed') {
            return "Отклонено"
        } else if (status === 'completed') {
            return "Завершено"
        } else if (status === 'refunded') {
            return "Возврат"
        } else {
            return " "
        }
    };

    const transactionSummary = orderPurchases?.list.reduce((sum, item) => {
        if (item.operation_type === 'completed') {
            return sum + item.amount
        } else if (item.operation_type === 'refund') {
            return sum - item.amount
        }
            return sum + item.amount
        }, 0);

    const operationType = (type) => {
        if (type === 'payment') {
            return 'Оплата'
        } else if (type === 'refund') {
            return 'Возврат'
        } else {
            return ''
        }
    }

    const refundButtonIsShow = (operationType, status, date) => {
        if (isBefore(new Date(), new Date(date)) && operationType === 'payment' && status === 'completed') {
            return true
        } else return isBefore(new Date(), new Date(date)) && operationType === 'refund' && status === 'declined';
    };

    return (
        <>
            <Box
                justify="between"
                direction="row"
                align="center"
            >
                <Heading size="large" level={2} margin={{top: "xsmall"}}>
                    {`№${orderDetail?.list?.order_number}`}
                </Heading>
                {refundButtonIsShow(orderPurchases?.list.operation_type, orderPurchases?.list.status, orderBookings?.list[0]?.ended_at)
                    ? <ReturnHeaderButton onClick={sendRefundGet}>
                        Возврат заказа
                    </ReturnHeaderButton>
                    : null
                }
            </Box>


            <Box
                justify="between"
                direction="row"
                align="top"
                margin={{"bottom": "28px"}}
            >
                <Box
                    background="#fff"
                    pad="32px"
                    width="32%"
                >
                    <TitleText>Основная информация</TitleText>
                    <Box
                        border={{
                            color:'#E0E0E0',
                            side:'horizontal',
                        }}
                        pad={{
                            left:'none',
                            bottom:'12px',
                            top:'12px',
                        }}
                    >
                        <Box
                            justify="between"
                            direction="row"
                            pad={{
                                bottom:'10px',
                                top:'10px',
                            }}
                        >
                            <ItemTitle>Номер заказа:</ItemTitle>
                            <Text
                                size="16px"
                                weight="bold"
                            >
                                {`№${orderDetail?.list?.order_number}`}
                            </Text>
                        </Box>
                    </Box>
                    <Box
                        border={{
                            color:'#E0E0E0',
                            side:'horizontal',
                        }}
                        pad={{
                            left:'none',
                            bottom:'12px',
                            top:'12px',
                        }}
                    >
                        <Box
                            justify="between"
                            direction="row"
                            pad={{
                                bottom:'10px',
                                top:'10px',
                            }}
                        >
                            <ItemTitle>Дата бронирования:</ItemTitle>
                            <Text
                                size="16px"
                                weight="bold"
                            >
                                {orderDetail?.list?.created_at
                                    ? format(new Date(orderDetail?.list?.created_at), 'dd.MM.yyyy')
                                    : ""
                                }
                            </Text>
                        </Box>
                    </Box>
                    <Box
                        border={{
                            color:'#E0E0E0',
                            side:'horizontal',
                        }}
                        pad={{
                            left:'none',
                            bottom:'12px',
                            top:'12px',
                        }}
                    >
                        <Box
                            justify="between"
                            direction="row"
                            pad={{
                                bottom:'10px',
                                top:'10px',
                            }}
                        >
                            <ItemTitle>Даты заезда - выезда:</ItemTitle>
                            <Text
                                size="16px"
                                weight="bold"
                            >
                                {orderBookings?.list[0]?.started_at && orderBookings?.list[0]?.ended_at
                                    ? `${format(new Date(orderBookings?.list[0]?.started_at), 'dd.MM.yyyy')} - ${format(new Date(orderBookings?.list[0]?.ended_at), 'dd.MM.yyyy')}`
                                    : null
                                }
                            </Text>
                        </Box>
                    </Box>
                    <Box
                        border={{
                            color:'#E0E0E0',
                            side:'horizontal',
                        }}
                        pad={{
                            left:'none',
                            bottom:'12px',
                            top:'12px',
                        }}
                    >
                        <Box
                            justify="between"
                            direction="row"
                            pad={{
                                bottom:'10px',
                                top:'10px',
                            }}
                        >
                            <ItemTitle>Статус:</ItemTitle>
                            <Text
                                size="16px"
                                weight="bold"
                            >
                                {orderStatus(orderDetail?.list?.status)}
                            </Text>
                        </Box>
                    </Box>
                    <Box
                        border={{
                            color:'#E0E0E0',
                            side:'horizontal',
                        }}
                        pad={{
                            left:'none',
                            bottom:'12px',
                            top:'12px',
                        }}
                    >
                        <Box
                            justify="between"
                            direction="row"
                            pad={{
                                bottom:'10px',
                                top:'10px',
                            }}
                        >
                            <ItemTitle>Стоимость:</ItemTitle>
                            <Text
                                size="16px"
                                weight="bold"
                            >
                                {`${currencyFormatter.format(orderDetail?.list?.total_paid)} KZT`}
                            </Text>
                        </Box>
                    </Box>
                </Box>
                <Box
                    background="#fff"
                    pad="32px"
                    width="32%"
                >
                    <Box
                        justify="between"
                        direction="row"
                        align="top"
                    >
                        <TitleText>Транзакция</TitleText>
                        {orderDetail?.list?.active_purchase
                            ? <ButtonBlue onClick={toggle2}>
                                Посмотреть еще
                            </ButtonBlue>
                            : null
                        }
                    </Box>
                    <Box
                        border={{
                            color:'#E0E0E0',
                            side:'horizontal',
                        }}
                        pad={{
                            left:'none',
                            bottom:'12px',
                            top:'12px',
                        }}
                    >
                        <Box
                            justify="between"
                            direction="row"
                            pad={{
                                bottom:'10px',
                                top:'10px',
                            }}
                        >
                            <ItemTitle>Транзакция:</ItemTitle>
                            <Text
                                size="16px"
                                weight="bold"
                            >
                                {orderDetail?.list?.active_purchase
                                    ? orderDetail.list?.active_purchase.transaction
                                    : '-'
                                }
                            </Text>
                        </Box>
                    </Box>
                    <Box
                        border={{
                            color:'#E0E0E0',
                            side:'horizontal',
                        }}
                        pad={{
                            left:'none',
                            bottom:'12px',
                            top:'12px',
                        }}
                    >
                        <Box
                            justify="between"
                            direction="row"
                            pad={{
                                bottom:'10px',
                                top:'10px',
                            }}
                        >
                            <ItemTitle>Сервис:</ItemTitle>
                            <Text
                                size="16px"
                                weight="bold"
                            >
                                {orderDetail?.list?.active_purchase
                                    ? orderDetail.list?.active_purchase.payment_service
                                    : '-'
                                }
                            </Text>
                        </Box>
                    </Box>
                    <Box
                        border={{
                            color:'#E0E0E0',
                            side:'horizontal',
                        }}
                        pad={{
                            left:'none',
                            bottom:'12px',
                            top:'12px',
                        }}
                    >
                        <Box
                            justify="between"
                            direction="row"
                            pad={{
                                bottom:'10px',
                                top:'10px',
                            }}
                        >
                            <ItemTitle>Метод:</ItemTitle>
                            <Text
                                size="16px"
                                weight="bold"
                            >
                                {orderDetail?.list?.active_purchase
                                    ? orderDetail.list?.active_purchase.payment_method
                                    : '-'
                                }
                            </Text>
                        </Box>
                    </Box>
                    <Box
                        border={{
                            color:'#E0E0E0',
                            side:'horizontal',
                        }}
                        pad={{
                            left:'none',
                            bottom:'12px',
                            top:'12px',
                        }}
                    >
                        <Box
                            justify="between"
                            direction="row"
                            pad={{
                                bottom:'10px',
                                top:'10px',
                            }}
                        >
                            <ItemTitle>Статус:</ItemTitle>
                            <StatusText
                                size="16px"
                                weight="bold"
                                status={paymentState(orderDetail.list?.active_purchase?.status)}
                            >
                                {orderDetail?.list?.active_purchase
                                    ? paymentState(orderDetail.list?.active_purchase?.status)
                                    : '-'
                                }
                            </StatusText>
                        </Box>
                    </Box>
                    <Box
                        border={{
                            color:'#E0E0E0',
                            side:'horizontal',
                        }}
                        pad={{
                            left:'none',
                            bottom:'12px',
                            top:'12px',
                        }}
                    >
                        <Box
                            justify="between"
                            direction="row"
                            pad={{
                                bottom:'10px',
                                top:'10px',
                            }}
                        >
                            <ItemTitle>Тип операции:</ItemTitle>
                            <Text
                                size="16px"
                                weight="bold"
                            >
                                {orderDetail?.list?.active_purchase
                                    ? operationType(orderDetail.list?.active_purchase.operation_type)
                                    : '-'
                                }
                            </Text>
                        </Box>
                    </Box>
                </Box>
                <Box
                    background="#fff"
                    pad="32px"
                    width="32%"
                >
                    <TitleText>Заказчик</TitleText>
                    <Box
                        border={{
                            color:'#E0E0E0',
                            side:'horizontal',
                        }}
                        pad={{
                            left:'none',
                            bottom:'12px',
                            top:'12px',
                        }}
                    >
                        <Box
                            justify="between"
                            direction="row"
                            pad={{
                                bottom:'10px',
                                top:'10px',
                            }}
                        >
                            <ItemTitle>ИИН/№ документа:</ItemTitle>
                            <Text
                                size="16px"
                                weight="bold"
                            >
                                991203400498
                            </Text>
                        </Box>
                    </Box>
                    <Box
                        border={{
                            color:'#E0E0E0',
                            side:'horizontal',
                        }}
                        pad={{
                            left:'none',
                            bottom:'12px',
                            top:'12px',
                        }}
                    >
                        <Box
                            justify="between"
                            direction="row"
                            pad={{
                                bottom:'10px',
                                top:'10px',
                            }}
                        >
                            <ItemTitle>Фамилия:</ItemTitle>
                            <Text
                                size="16px"
                                weight="bold"
                            >
                                {orderDetail.list?.customer?.surname}
                            </Text>
                        </Box>
                    </Box>
                    <Box
                        border={{
                            color:'#E0E0E0',
                            side:'horizontal',
                        }}
                        pad={{
                            left:'none',
                            bottom:'12px',
                            top:'12px',
                        }}
                    >
                        <Box
                            justify="between"
                            direction="row"
                            pad={{
                                bottom:'10px',
                                top:'10px',
                            }}
                        >
                            <ItemTitle>Имя:</ItemTitle>
                            <Text
                                size="16px"
                                weight="bold"
                            >
                                {orderDetail.list?.customer?.name}
                            </Text>
                        </Box>
                    </Box>
                    <Box
                        border={{
                            color:'#E0E0E0',
                            side:'horizontal',
                        }}
                        pad={{
                            left:'none',
                            bottom:'12px',
                            top:'12px',
                        }}
                    >
                        <Box
                            justify="between"
                            direction="row"
                            pad={{
                                bottom:'10px',
                                top:'10px',
                            }}
                        >
                            <ItemTitle>Телефон:</ItemTitle>
                            <Text
                                size="16px"
                                weight="bold"
                            >
                                991203400498
                            </Text>
                        </Box>
                    </Box>
                    <Box
                        border={{
                            color:'#E0E0E0',
                            side:'horizontal',
                        }}
                        pad={{
                            left:'none',
                            bottom:'12px',
                            top:'12px',
                        }}
                    >
                        <Box
                            justify="between"
                            direction="row"
                            pad={{
                                bottom:'10px',
                                top:'10px',
                            }}
                        >
                            <ItemTitle>Email:</ItemTitle>
                            <Text
                                size="16px"
                                weight="bold"
                            >
                                {orderDetail.list?.customer?.email}
                            </Text>
                        </Box>
                    </Box>
                </Box>
            </Box>

            <Box
                background="#fff"
            >
                <TableTitle>Номер</TableTitle>
                <Table>
                    <TableHeader>
                        <TableCell align="start">
                            <Text>№ комнаты:</Text>
                        </TableCell>
                        <TableCell align="start">
                            <Text>Тип комнаты:</Text>
                        </TableCell>
                        <TableCell align="start">
                            <Text>Цена:</Text>
                        </TableCell>
                        <TableCell align="start">
                            <Text>Дополнительные услуги:</Text>
                        </TableCell>
                        <TableCell align="start">
                            <Text>Сумма:</Text>
                        </TableCell>
                        {/*<TableCell align="start">*/}
                        {/*    <Text>{"   "}</Text>*/}
                        {/*</TableCell>*/}
                    </TableHeader>
                    <TableBody>
                        {orderBookings?.list.map((item,i) => (
                            <SalesDetail item={item} i={i} hide={toggle1} />
                            ))}
                    </TableBody>
                </Table>
            </Box>

            {/*<Modal*/}
            {/*    isShowing={isShowing}*/}
            {/*    hide={toggle}*/}
            {/*    background="#000"*/}
            {/*    opacity=".5"*/}
            {/*    width={'500px'}*/}
            {/*>*/}
            {/*    <Box*/}
            {/*        pad="none"*/}
            {/*    >*/}
            {/*        <ModalTitle>Заказ №1423</ModalTitle>*/}
            {/*        <InputTitle>Сумма возврата</InputTitle>*/}
            {/*        <Box pad={{left:'32px', right:'32px', bottom: '6px'}}>*/}
            {/*            <Box direction="row" align="center" round="xsmall" border>*/}
            {/*                <TextInput plain />*/}
            {/*                <Box*/}
            {/*                    background="#F2F2F2"*/}
            {/*                    round={{ corner: "right", size: "xsmall" }}*/}
            {/*                    border={{ side: "left" }}*/}
            {/*                    pad={{ vertical: "small", horizontal: "medium" }}*/}
            {/*                >*/}
            {/*                    <Text size="small">KZT</Text>*/}
            {/*                </Box>*/}
            {/*            </Box>*/}
            {/*        </Box>*/}
            {/*        <ModalInfoText>Процент от суммы</ModalInfoText>*/}
            {/*        <BackModalButton onClick={() => toggle()}>Отменить</BackModalButton>*/}
            {/*        <ModalButton onClick={toggle1}>Далее</ModalButton>*/}
            {/*    </Box>*/}
            {/*</Modal>*/}
            <Modal
                isShowing={isShowing1}
                hide={hide}
                width={'500px'}
                background="#000"
                opacity=".5"
                margin="11.75rem auto"
            >
                <Box
                    pad="none"
                >
                    <ModalTitle>{`№${orderDetail?.list?.order_number}`}</ModalTitle>
                    {orderDetail?.list?.primary_purchase?.payment_service === 'cloudpayments'
                        ? <div
                            style={{
                                display:'flex',
                                justifyContent:'space-between',
                                padding:'32px',
                                alignItems:'center',
                            }}
                        >
                            <RefundAmountTitle>Сумма возврата</RefundAmountTitle>
                            <RefundAmount>
                                9 000 KZT (90%)
                            </RefundAmount>
                        </div>
                        : null
                    }

                    <ModalInfoText>Вы уверены, что хотите осуществить возврат денег за заказ?</ModalInfoText>
                    {/*<BackModalButton onClick={() => toggle1()}>Назад</BackModalButton>*/}
                    <ModalButton onClick={hide}>Да, подтвердить</ModalButton>
                </Box>
            </Modal>

            <Modal
                isShowing={isShowing2}
                hide={toggle2}
                background="#000"
                opacity=".5"
                width="fit-content"
                margin="11.75rem auto"
            >
                <Table>
                    <TableHeader>
                        <TableCell align="start">
                            <TransactionInfoTitle>Транзакция</TransactionInfoTitle>
                        </TableCell>
                        <TableCell align="start">
                            <TransactionInfoTitle>Сервис</TransactionInfoTitle>
                        </TableCell>
                        <TableCell align="start">
                            <TransactionInfoTitle>Метод</TransactionInfoTitle>
                        </TableCell>
                        <TableCell align="start">
                            <TransactionInfoTitle>Сумма</TransactionInfoTitle>
                        </TableCell>
                        <TableCell align="start">
                            <TransactionInfoTitle>Тип операции </TransactionInfoTitle>
                        </TableCell>
                        <TableCell align="start">
                            <TransactionInfoTitle>Дата создания</TransactionInfoTitle>
                        </TableCell>
                        <TableCell align="start">
                            <TransactionInfoTitle>Статус</TransactionInfoTitle>
                        </TableCell>
                    </TableHeader>
                    <TableBody>
                        {orderPurchases?.list.map((item) => (
                            <TableRow key={item.id}>
                                <TableCell>
                                    <TransactionInfoValue>{item.transaction}</TransactionInfoValue>
                                </TableCell>
                                <TableCell align="center">
                                    <TransactionInfoValue>{item.payment_service}</TransactionInfoValue>
                                </TableCell>
                                <TableCell align="center">
                                    <TransactionInfoValue>{item.payment_method}</TransactionInfoValue>
                                </TableCell>
                                <TableCell align="center">
                                    <TransactionInfoValue>
                                        {item.operation_type === 'payment'
                                            ? `+ ${currencyFormatter.format(item.amount)} ${item.currency}`
                                            : `- ${currencyFormatter.format(item.amount)} ${item.currency}`
                                        }
                                    </TransactionInfoValue>
                                </TableCell>
                                <TableCell align="center">
                                    <TransactionInfoValue
                                        status={operationType(item.operation_type)}
                                    >
                                        {operationType(item.operation_type)}
                                    </TransactionInfoValue>
                                </TableCell>
                                <TableCell align="center">
                                    <TransactionInfoValue>
                                        {item.created_at
                                            ? format(new Date(item.created_at), 'dd.MM.yyyy')
                                            : ""
                                        }
                                    </TransactionInfoValue>
                                </TableCell>
                                <TableCell align="center">
                                    <TransactionInfoValue
                                        status={paymentState(item.status)}
                                    >
                                        {paymentState(item.status)}
                                    </TransactionInfoValue>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                    {/*<TableFooter border="none">*/}
                    {/*    <TableRow border="none">*/}
                    {/*        {data6.map((item) => (*/}
                    {/*            <TableCell align="end">*/}
                    {/*                <Text border="none">{item}</Text>*/}
                    {/*            </TableCell>*/}
                    {/*        ))}*/}
                    {/*    </TableRow>*/}
                    {/*</TableFooter>*/}
                </Table>
                <ModalTableFooter>
                    <ModalFooterText>Итого: </ModalFooterText>
                    <ModalFooterTextValue>
                        {`${currencyFormatter.format(transactionSummary)} KZT`}
                    </ModalFooterTextValue>
                </ModalTableFooter>
            </Modal>
            <Box
                pad="32px"
                justify="end"
                direction="row"
                align="center"
            >
                <Text>Итого:</Text>
                <FooterText>{`${currencyFormatter.format(orderDetail?.list?.total_paid)} KZT`}</FooterText>
            </Box>
        </>
    );
};

const ModalTableFooter = styled.div`
  background: #F2F2F2;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  padding-right: 24px;
  border-radius: 0 0 8px 8px;
  padding-top: 15px;
  padding-bottom: 15px;
`

const ModalFooterText = styled(Text)`
  font-weight: normal;
  font-size: 16px;
  line-height: 24px;
  margin: 0;
  padding-right: 8px;
`

const ModalFooterTextValue = styled(Text)`
  font-weight: normal;
  font-size: 16px;
  line-height: 24px;
  margin: 0;
  color: #219653;
`

const TransactionInfoTitle = styled(Text)`
  font-weight: bold;
  font-size: 18px;
  line-height: 24px;
  text-transform: uppercase;
  color: #333333;
  margin: 0;
`;

const TransactionInfoValue = styled(Text)`
  font-weight: normal;
  font-size: 16px;
  line-height: 24px;
  color: ${(props) => {
    if(props.status === 'Завершено' || props.status === 'Оплата') {
      return "#219653"
    } else if(props.status === 'Отклонено') {
      return "#F2994A"
    } else if(props.status === 'Отменено' || props.status === 'Возврат') {
      return "#EB5757"
    } else {
      return "#333333"
    }
  }};
  margin: 0;
`;

const RefundAmountTitle = styled(Text)`
  font-weight: normal;
  font-size: 18px;
  line-height: 24px;
  color: #333333;
  margin: 0;
`;

const RefundAmount = styled(Text)`
  font-weight: bold;
  font-size: 24px;
  line-height: 32px;
  color: #333333;
  margin: 0;
`;

const ModalButton = styled(Button)`
  background: #2F80ED;
  border-radius: 8px;
  font-weight: 600;
  font-size: 18px;
  line-height: 24px;
  text-align: center;
  color: #FFFFFF;
  min-width: 416px;
  min-height: 64px;
  margin-left: 32px;
  margin-right: 32px;
  margin-bottom: 32px;
`;

const BackModalButton = styled(Button)`
  border: none;
  font-weight: 600;
  font-size: 18px;
  line-height: 24px;
  text-decoration-line: underline;
  text-align: center;
  margin: 0;
  padding-bottom: 24px;
  color: #BDBDBD;
`;

const ModalTitle = styled(Text)`
  font-weight: bold;
  font-size: 32px;
  line-height: 40px;
  color: #333333;
  padding: 32px;
  border-bottom: 1px solid #E0E0E0;
  margin: 0;
`;

const InputTitle = styled(Text)`
  font-weight: 600;
  font-size: 18px;
  line-height: 24px;
  color: #333333;
  padding: 32px 0 8px 32px;
  margin: 0;
`;

const ModalInfoText = styled(Text)`
  font-weight: normal;
  font-size: 16px;
  line-height: 24px;
  color: #828282;
  margin: 0;
  padding: 10px 32px 32px 32px;
  text-align: center;
`;

const FooterText = styled(Text)`
  color: #179633;
  font-weight: bold;
  font-size: 32px;
  line-height: 40px;
  margin: 0;
  padding-left: 16px;
`;

const ButtonBlue = styled(Button)`
  background: rgba(47, 128, 237, 0.1);
  border-radius: 8px;
  padding: 8px 12px;
  font-weight: normal;
  font-size: 14px;
  line-height: 16px;
  color: #2F80ED;
  max-height: 32px;
`;

const ReturnHeaderButton = styled(Button)`
  border-radius: 8px;
  background: #EB5757;
  color: #FFFFFF;
  font-weight: 600;
  font-size: 18px;
  line-height: 24px;
  text-align: center;
  min-width: 344px;
  min-height: 48px;
`;

const TitleText = styled(Text)`
  font-weight: bold;
  font-size: 32px;
  line-height: 40px;
  color: #333333;
  margin: 0;
  padding-bottom: 32px;
`;

const TableTitle = styled(Text)`
  font-weight: bold;
  font-size: 32px;
  line-height: 40px;
  color: #333333;
  margin: 0;
  padding-bottom: 20px;
  padding-top: 20px;
  padding-left: 32px;
`;

const ItemTitle = styled(Text)`
  font-weight: normal;
  font-size: 16px;
  line-height: 22px;
  color: #828282;
  margin: 0;
`;

const StatusText = styled(Text)`
  color: ${(props) => {
    if(props.status === 'Завершено') {
        return "#219653"
    } else if(props.status === 'Отклонено') {
        return "#F2994A"
    } else if(props.status === 'Отменено') {
        return "#EB5757"
    }
}};
`;

export default withRouter(Detail);
