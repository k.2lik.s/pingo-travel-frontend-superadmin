import { useEffect, useState, useCallback } from "react";
import { useRouteMatch, withRouter } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { format, isBefore } from "date-fns";
import styled from "styled-components";
import {
    Table,
    TableHeader,
    TableCell,
    TableBody,
    TableRow as GrommetTableRow,
    Box,
    Text,
    Heading,
    TextInput,
    Button,
    DateInput,
    Select,
} from 'grommet';
import {
    getOrders,
    getAmountAllRooms,
    getAmountFreeRooms,
    getAmountOccupiedRooms,
    getAmountInactiveRooms,
    getOrderRefund,
} from "modules/sales/actions";
import useModal from "ui/Common/Modal/useModal";
import { FormSelect } from "ui/Common/Form/Selects";
import Refund from "ui/Common/Form/Refund";
import Modal from "ui/Common/Modal";
import '../styles.scss';


const StatusOptions = [
    {title: 'Ожидания оплаты', value: 'awaiting payment'},
    {title: 'Время ожидания истекло', value: 'expired'},
    {title: 'Отклонено', value: 'failed'},
    {title: 'Завершено', value: 'completed'},
    {title: 'Возврат', value: 'refunded'},
];

const StatusPayment = [
    {title: '-', value: "-"},
    {title: 'Завершено', value: 'completed'},
    {title: 'Отклонено', value: 'declined'},
];

const SalesPage = ({ history }) => {
    const [page, setPage] = useState(1);
    const [perPage, setPerPage] = useState(100);
    const [orderNumber, setOrderNumber] = useState('');
    const [totalPaid, setTotalPaid] = useState('');
    const [dateValue, setDateValue] = useState([
        "2019-07-31T15:27:42.920Z",
        "2023-08-07T15:27:42.920Z",
    ]);
    const [createdAt, setCreatedAt] = useState('');
    const [statusValue, setStatusValue] = useState([]);
    const [statusTitle, setStatusTitle] = useState('');
    const [statusPaymentValue, setStatusPaymentValue] = useState([]);
    const [statusPaymentTitle, setStatusPaymentTitle] = useState('');
    const [status, setStatus] = useState('');
    const { vendor } = useSelector((state) => state.common);
    const { order } = useSelector((state) => state.order);
    const { amountOfRooms } = useSelector((state) => state.order);
    const dispatch = useDispatch();
    useEffect(() => {
        if (!vendor.loading) {
            dispatch(getOrders(
                vendor.currentVendor.id,
                page,
                perPage,
                orderNumber,
                totalPaid,
                dateValue[0] ? format(new Date(dateValue[0]), 'dd.MM.yyyy') : null,
                dateValue[1] ? format(new Date(dateValue[1]), 'dd.MM.yyyy') : null,
                createdAt ? format(new Date(createdAt), 'dd.MM.yyyy') : null,
                statusValue[0]?.value,
                statusPaymentValue[0]?.value,
            ));
            dispatch(getAmountAllRooms(vendor.currentVendor.id));
            dispatch(getAmountFreeRooms(vendor.currentVendor.id));
            dispatch(getAmountOccupiedRooms(vendor.currentVendor.id));
            dispatch(getAmountInactiveRooms(vendor.currentVendor.id));
        }
    }, [vendor, orderNumber, totalPaid, dateValue, createdAt, statusValue, statusPaymentValue]);

    const sendRefundGet = useCallback((id) => dispatch(getOrderRefund(id)), [])

    let { path, url } = useRouteMatch();
    const [isShowing, toggle] = useModal();
    const [isShowing1, toggle1] = useModal();
    const hide = () => {
        toggle();
        toggle1();
    };

    const dateFormat = new Intl.DateTimeFormat("ru-RU", {
        month: "long",
        day: "numeric",
    });

    const onChangeDate = event => {
        const nextValue = event.value;
        console.log('onChange', nextValue);
        setDateValue(nextValue);
    };

    const onChangeCreatedAt = event => {
        const nextValue = event.value;
        console.log('onChange', nextValue);
        setCreatedAt(nextValue);
    };

    const currencyFormatter = Intl.NumberFormat('ru-RU');

    const orderStatus = (status) => {
      if (status === 'expired') {
          return 'Время ожидания истекло'
      } else if (status === 'awaiting') {
          return "Ожидание оплаты"
      } else if (status === 'failed') {
          return "Отклонено"
      } else if (status === 'completed') {
          return "Завершено"
      } else if (status === 'refunded') {
          return "Возврат"
      } else {
          return " "
      }
    };

    const paymentState = (status) => {
        if (status === 'declined') {
            return 'Отклонено'
        } else if (status === 'completed') {
            return 'Завершено'
        } else {
            return "-"
        }
    };

    const refundButton = (status, payment, date) => {
        if (isBefore(new Date(), new Date(date)) && status === 'completed' && payment === 'payment') {
            return true
        } else if (isBefore(new Date(), new Date(date)) && status === 'declined' && payment === 'refund') {
            return true
        } else {
            return false
        }
    };

    return (
        <div>
            <div>
                <Heading size="large" level={2} margin={{ top: "xsmall" }}>
                    Заказы
                </Heading>

                <Box
                    justify="between"
                    direction="row"
                    align="top"
                    margin={{"bottom": "28px"}}
                >
                    <Box background="#fff" justify="between" pad="32px" width="23%">
                        <Text
                            size="xlarge"
                            history
                            weight="bold"
                        >
                            {amountOfRooms?.amountOfAllRooms?.total}
                        </Text>
                        <RoomsAvailabilityTitle>
                            Всего комнат
                        </RoomsAvailabilityTitle>
                    </Box>
                    <Box background="#fff" justify="between" pad="32px" width="23%">
                        <Text
                            size="xlarge"
                            weight="bold"
                        >
                            {amountOfRooms?.amountOfFreeRooms?.total}
                        </Text>
                        <RoomsAvailabilityTitle>
                            Доступные комнаты
                        </RoomsAvailabilityTitle>
                    </Box>
                    <Box background="#fff" justify="between" pad="32px" width="23%">
                        <Text
                            size="xlarge"
                            weight="bold"
                        >
                            {amountOfRooms?.amountOfOccupiedRooms?.total}
                        </Text>
                        <RoomsAvailabilityTitle>
                            Забронированные комнаты
                        </RoomsAvailabilityTitle>
                    </Box>
                    <Box background="#fff" justify="between" pad="32px" width="23%">
                        <Text
                            size="xlarge"
                            weight="bold"
                        >
                            {amountOfRooms?.amountOfInactiveRooms?.total}
                        </Text>
                        <RoomsAvailabilityTitle>
                            Недоступные комнаты
                        </RoomsAvailabilityTitle>
                    </Box>
                </Box>

                <Table>
                    <TableHeader>
                        <TableCell align="start">
                            <Text>№ заказа</Text>
                            <Box width="110px" pad={{ top: "medium", left: "none" }} align="center">
                                <TextInput
                                    placeholder="№ заказа"
                                    onChange={(e) => setOrderNumber(e.target.value)}
                                />
                            </Box>
                        </TableCell>
                        <TableCell align="start">
                            <Text>Дата бронирования</Text>
                            <Box width="200px" pad={{ top: "medium", left: "none" }} align="center">
                                <DateInput
                                    format="dd.mm.yyyy"
                                    placeholder="Бронирование"
                                    value={createdAt}
                                    calendarProps={{
                                        locale: 'ru-RU'
                                    }}
                                    onChange={onChangeCreatedAt}
                                />
                            </Box>
                        </TableCell>
                        <TableCell align="start">
                            <Text>Даты заезда – выезда</Text>
                            <Box width="240px" pad={{ top: "medium", left: "none" }} align="center">
                                <DateInput
                                    value={dateValue}
                                    format="dd.mm.yyyy-dd.mm.yyyy"
                                    placeholder="Заезд – выезд"
                                    calendarProps={{
                                        locale: 'ru-RU'
                                    }}
                                    buttonProps={{
                                        label: `${dateFormat.format(
                                            new Date(dateValue[0]),
                                        )} - ${dateFormat.format(new Date(dateValue[1]))}`,
                                    }}
                                    onChange={onChangeDate}
                                />
                            </Box>
                        </TableCell>
                        <TableCell align="start">
                            <Text>Стоимость</Text>
                            <Box width="inherit" pad={{ top: "medium", left: "none" }} align="center">
                                <TextInput
                                    type="number"
                                    placeholder="Стоимость"
                                    onChange={(e) => setTotalPaid(e.target.value)}
                                />
                            </Box>
                        </TableCell>
                        <TableCell align="start">
                            <Text>Статус заказа</Text>
                            <Box width="inherit" pad={{ top: "medium", left: "none" }} align="center">
                                <FormSelect
                                    placeholder="Статус заказа"
                                    options={StatusOptions.map((item) => item.title)}
                                    value={statusTitle}
                                    onChange={({value: nextValue}) => {
                                        setStatusTitle(nextValue);
                                        setStatusValue(StatusOptions.filter(({title}) => title === nextValue));
                                    }}
                                    clear={{
                                        label: 'Очистить'
                                    }}
                                />
                            </Box>
                        </TableCell>
                        <TableCell align="start">
                            <Text>Статус оплаты</Text>
                            <Box width="inherit" pad={{ top: "medium", left: "none" }} align="center">
                                <FormSelect
                                    placeholder="Статус оплаты"
                                    options={StatusPayment.map((item) => item.title)}
                                    value={statusPaymentTitle}
                                    onChange={({value: nextValue}) => {
                                        setStatusPaymentTitle(nextValue);
                                        setStatusPaymentValue(StatusPayment.filter(({title}) => title === nextValue));
                                    }}
                                    clear={{
                                        label: 'Очистить'
                                    }}
                                />
                            </Box>
                        </TableCell>
                        <TableCell align="start">
                            <Text>{"   "}</Text>
                        </TableCell>
                    </TableHeader>
                    <TableBody>
                        {order.list.map((item,i) => (
                            <TableRow
                                key={item.id}
                                onClick={() => history.push(`${url}/detail/${item.id}`, {
                                    id: item.id,
                                })}
                            >
                                <TableCell>
                                    <Text>{`#${item?.order_number}`}</Text>
                                </TableCell>
                                <TableCell align="center">
                                    <Text>
                                        {item.created_at
                                            ? format(new Date(item.created_at), 'dd.MM.yyyy')
                                            : null
                                        }
                                    </Text>
                                </TableCell>
                                <TableCell align="center">
                                    <Text>
                                        {`
                                        ${format(new Date(item?.bookings[0]?.started_at), 'dd.MM.yyyy')} 
                                        - 
                                        ${format(new Date(item?.bookings[0]?.ended_at), 'dd.MM.yyyy')}
                                        `}
                                    </Text>
                                </TableCell>
                                <TableCell align="center">
                                    <Text>{`${currencyFormatter.format(item?.total_paid)} KZT`}</Text>
                                </TableCell>
                                <TableCell align="center">
                                    <Text>{orderStatus(item?.status)}</Text>
                                </TableCell>
                                <TableCell align="center">
                                    <StatusText status={paymentState(item?.active_purchase?.status)}>
                                        {paymentState(item?.active_purchase?.status)}
                                    </StatusText>
                                </TableCell>
                                {refundButton(item?.active_purchase?.status, item?.active_purchase?.operation_type, item?.bookings[0]?.ended_at)
                                    ? <TableCell align="center">
                                        <Refund
                                            name="Возврат"
                                            modal={toggle1}
                                            id={item.id}
                                        />
                                    </TableCell>
                                    : null
                                }

                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
                {/*<Pagination numberItems={237} />*/}
            </div>
            {/*<Modal*/}
            {/*    isShowing={isShowing}*/}
            {/*    hide={toggle}*/}
            {/*    background="#000"*/}
            {/*    opacity=".5"*/}
            {/*    width={'500px'}*/}
            {/*>*/}
            {/*    <Box*/}
            {/*        pad="none"*/}
            {/*    >*/}
            {/*        <ModalTitle>Заказ №1423</ModalTitle>*/}
            {/*        <InputTitle>Сумма возврата</InputTitle>*/}
            {/*        <Box pad={{left:'32px', right:'32px', bottom: '6px'}}>*/}
            {/*            <Box direction="row" align="center" round="xsmall" border>*/}
            {/*                <TextInput plain />*/}
            {/*                <Box*/}
            {/*                    background="#F2F2F2"*/}
            {/*                    round={{ corner: "right", size: "xsmall" }}*/}
            {/*                    border={{ side: "left" }}*/}
            {/*                    pad={{ vertical: "small", horizontal: "medium" }}*/}
            {/*                >*/}
            {/*                    <Text size="small">KZT</Text>*/}
            {/*                </Box>*/}
            {/*            </Box>*/}
            {/*        </Box>*/}
            {/*        <ModalInfoText>Процент от суммы</ModalInfoText>*/}
            {/*        <BackModalButton onClick={() => toggle()}>Отменить</BackModalButton>*/}
            {/*        <ModalButton onClick={toggle1}>Далее</ModalButton>*/}
            {/*    </Box>*/}
            {/*</Modal>*/}
            <Modal
                isShowing={isShowing1}
                hide={toggle1}
                background="#000"
                opacity=".5"
                width={'500px'}
            >
                <Box
                    pad="none"
                >
                    <ModalTitle>Заказ №1423</ModalTitle>
                    <div
                        style={{
                            display:'flex',
                            justifyContent:'space-between',
                            padding:'32px',
                            alignItems:'center',
                        }}
                    >
                        <RefundAmountTitle>Сумма возврата</RefundAmountTitle>
                        <RefundAmount>9 000 KZT (90%)</RefundAmount>
                    </div>

                    <ModalInfoText>Вы уверены, что хотите осуществить возврат денег за заказ?</ModalInfoText>
                    {/*<BackModalButton onClick={() => toggle1()}>Назад</BackModalButton>*/}
                    <ModalButton onClick={toggle1}>Да, подтвердить</ModalButton>
                </Box>
            </Modal>
        </div>
    );
};

const RefundAmountTitle = styled(Text)`
  font-weight: normal;
  font-size: 18px;
  line-height: 24px;
  color: #333333;
  margin: 0;
`;

const RefundAmount = styled(Text)`
  font-weight: bold;
  font-size: 24px;
  line-height: 32px;
  color: #333333;
  margin: 0;
`;

const TableRow = styled(GrommetTableRow)`
  &:hover {
    background: rgba(47, 128, 237, 0.1);
    cursor: pointer;
  }
`;

const ModalButton = styled(Button)`
  background: #2F80ED;
  border-radius: 8px;
  font-weight: 600;
  font-size: 18px;
  line-height: 24px;
  text-align: center;
  color: #FFFFFF;
  min-width: 416px;
  min-height: 64px;
  margin-left: 32px;
  margin-right: 32px;
  margin-bottom: 32px;
`;

const BackModalButton = styled(Button)`
  border: none;
  font-weight: 600;
  font-size: 18px;
  line-height: 24px;
  text-decoration-line: underline;
  text-align: center;
  margin: 0;
  padding-bottom: 24px;
  color: #BDBDBD;
`;

const RoomsAvailabilityTitle = styled(Text)`
  font-style: normal;
  font-weight: 600;
  font-size: 18px;
  line-height: 24px;
  color: #BDBDBD;
  padding-top: 8px;
  margin: 0;
`;

const ModalTitle = styled(Text)`
  font-weight: bold;
  font-size: 32px;
  line-height: 40px;
  color: #333333;
  padding: 32px;
  border-bottom: 1px solid #E0E0E0;
  margin: 0;
`;

const InputTitle = styled(Text)`
  font-weight: 600;
  font-size: 18px;
  line-height: 24px;
  color: #333333;
  padding: 32px 0 8px 32px;
  margin: 0;
`;

const ModalInfoText = styled(Text)`
  font-weight: normal;
  font-size: 16px;
  line-height: 24px;
  color: #828282;
  margin: 0;
  padding-bottom: 32px;
  padding-left: 32px;
  padding-right: 32px;
  text-align: center;
`;

const StatusText = styled(Text)`
  color: ${(props) => {
    if(props.status === 'Завершено') {
        return "#219653"
    } else if(props.status === 'Отклонено') {
        return "#F2994A"
    } else if(props.status === 'Отменено') {
        return "#EB5757"
    }
}};
`;

export default withRouter(SalesPage);
