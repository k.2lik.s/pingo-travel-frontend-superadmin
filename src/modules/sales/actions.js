import * as types from "./constants";
import sendRequest from "settings/sendRequest";
import Nprogress from "nprogress";

export const getOrders = (vendorId, page, per_page, orderNumber, totalPaid, dateFrom, dateTo, createdAt, status, purchaseStatus) => (dispatch) => {
    dispatch({ type: types.GET_ORDERS });
    Nprogress.start();
    return sendRequest(`/vendors/${vendorId}/orders`, {
        method: "GET",
        params: {
            orderNumber: orderNumber,
            totalPaid: totalPaid,
            dateFrom: dateFrom,
            dateTo: dateTo,
            createdAt: createdAt,
            status: status,
            purchase_status: purchaseStatus,
            page: page,
            per_page: per_page,
        },
    })
        .then(({ data }) => {
            dispatch({ type: types.GET_ORDERS_SUCCESS, payload: data });
            Nprogress.done();
        })
        .catch((error) => {
            dispatch({ type: types.GET_ORDERS_FAILURE, payload: error });
        });
};

export const getOrderDetail = (orderId) => (dispatch) => {
    dispatch({ type: types.GET_ORDER_DETAIL });
    Nprogress.start();
    return sendRequest(`/orders/${orderId}`, {
        method: "GET",
    })
        .then(({ data }) => {
            dispatch({ type: types.GET_ORDER_DETAIL_SUCCESS, payload: data });
            Nprogress.done();
        })
        .catch((error) => {
            dispatch({ type: types.GET_ORDER_DETAIL_FAILURE, payload: error });
        });
};

export const getOrderDetailPurchase = (orderId) => (dispatch) => {
  dispatch({ type: types.GET_ORDER_DETAIL_PURCHASE });
  Nprogress.start();
  return sendRequest(`/orders/${orderId}/purchases`, {
      method: "GET",
  })
      .then(({data}) => {
          dispatch({ type: types.GET_ORDER_DETAIL_PURCHASE_SUCCESS, payload: data });
          Nprogress.done();
      })
      .catch((error) => {
          dispatch({ type: types.GET_ORDER_DETAIL_PURCHASE_FAILURE, payload: error });
      });
};

export const getOrderDetailBookings = (orderId) => (dispatch) => {
    dispatch({ type: types.GET_ORDER_DETAIL_BOOKINGS });
    Nprogress.start();
    return sendRequest(`/orders/${orderId}/bookings`, {
        method: "GET",
    })
        .then(({data}) => {
            dispatch({type: types.GET_ORDER_DETAIL_BOOKINGS_SUCCESS, payload: data});
            Nprogress.done();
        })
        .catch((error) => {
           dispatch({ type: types.GET_ORDER_DETAIL_BOOKINGS_FAILURE, payload: error });
        });
};

export const getOrderRefund = (orderId) => (dispatch) => {
    dispatch({ type: types.GET_ORDER_REFUND });
    Nprogress.start();
    return sendRequest(`/orders/${orderId}/refund`, {
        method: "GET"
    })
        .then(({data}) => {
            dispatch({ type: types.GET_ORDER_REFUND_SUCCESS, payload: data });
            Nprogress.done();
        })
        .catch((error) => {
            dispatch({ type: types.GET_ORDER_REFUND_FAILURE, payload: error });
        });
};

export const getAmountAllRooms = (vendorId) => (dispatch) => {
    dispatch({ type: types.GET_AMOUNT_ALL_ROOMS });
    Nprogress.start();
    return sendRequest(`/vendors/${vendorId}/room-numbers?per_page=0`, {
        method: "GET"
    })
        .then(({data}) => {
            dispatch({ type: types.GET_AMOUNT_ALL_ROOMS_SUCCESS, payload: data });
            Nprogress.done();
        })
        .catch((error) => {
            dispatch({ type: types.GET_AMOUNT_ALL_ROOMS_FAILURE, payload: error });
        });
};

export const getAmountFreeRooms = (vendorId) => (dispatch) => {
    dispatch({ type: types.GET_AMOUNT_FREE_ROOMS });
    Nprogress.start();
    return sendRequest(`/vendors/${vendorId}/room-numbers?per_page=0&condition=free`, {
        method: "GET"
    })
        .then(({data}) => {
            dispatch({ type: types.GET_AMOUNT_FREE_ROOMS_SUCCESS, payload: data });
            Nprogress.done();
        })
        .catch((error) => {
            dispatch({ type: types.GET_AMOUNT_FREE_ROOMS_FAILURE, payload: error });
        });
};

export const getAmountOccupiedRooms = (vendorId) => (dispatch) => {
    dispatch({ type: types.GET_AMOUNT_OCCUPIED_ROOMS });
    Nprogress.start();
    return sendRequest(`/vendors/${vendorId}/room-numbers?per_page=0&condition=occupied`, {
        method: "GET"
    })
        .then(({data}) => {
            dispatch({ type: types.GET_AMOUNT_OCCUPIED_ROOMS_SUCCESS, payload: data });
            Nprogress.done();
        })
        .catch((error) => {
            dispatch({ type: types.GET_AMOUNT_OCCUPIED_ROOMS_FAILURE, payload: error });
        });
};

export const getAmountInactiveRooms = (vendorId) => (dispatch) => {
    dispatch({ type: types.GET_AMOUNT_INACTIVE_ROOMS });
    Nprogress.start();
    return sendRequest(`/vendors/${vendorId}/room-numbers?per_page=0&status=inactive`, {
        method: "GET"
    })
        .then(({data}) => {
            dispatch({ type: types.GET_AMOUNT_INACTIVE_ROOMS_SUCCESS, payload: data });
            Nprogress.done();
        })
        .catch((error) => {
            dispatch({ type: types.GET_AMOUNT_INACTIVE_ROOMS_FAILURE, payload: error });
        });
};
