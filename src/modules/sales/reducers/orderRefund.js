import produce from "immer";
import * as types from "../constants";

const INITAL_STATE = {
    list: [],
    loading: true,
    error: {},
};

export const orderRefund = produce((draft, action) => {
    switch (action.type) {
        case types.GET_ORDER_REFUND:
            draft.loading = true;
            return;
        case types.GET_ORDER_REFUND_SUCCESS:
            draft.list = action.payload.data;
            draft.loading = false;
            return;
        case types.GET_ORDER_REFUND_FAILURE:
            draft.error = action.payload.error;
            draft.loading = false;
            return;
    }
}, INITAL_STATE);
