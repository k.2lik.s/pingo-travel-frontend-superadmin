import produce from "immer";
import * as types from "../constants";

const INITAL_STATE = {
    amountOfAllRooms: '',
    amountOfFreeRooms: '',
    amountOfOccupiedRooms: '',
    amountOfInactiveRooms: '',
    loading: true,
    error: {},
};

export const amountOfRooms = produce((draft, action) => {
    switch (action.type) {
        case types.GET_AMOUNT_ALL_ROOMS:
            draft.loading = true;
            return;
        case types.GET_AMOUNT_ALL_ROOMS_SUCCESS:
            draft.amountOfAllRooms = action.payload.data;
            draft.loading = false;
            return;
        case types.GET_AMOUNT_ALL_ROOMS_FAILURE:
            draft.error = action.payload.error;
            draft.loading = false;
            return;
        case types.GET_AMOUNT_FREE_ROOMS:
            draft.loading = true;
            return;
        case types.GET_AMOUNT_FREE_ROOMS_SUCCESS:
            draft.amountOfFreeRooms = action.payload.data;
            draft.loading = false;
            return;
        case types.GET_AMOUNT_FREE_ROOMS_FAILURE:
            draft.error = action.payload.error;
            draft.loading = false;
            return;
        case types.GET_AMOUNT_OCCUPIED_ROOMS:
            draft.loading = true;
            return;
        case types.GET_AMOUNT_OCCUPIED_ROOMS_SUCCESS:
            draft.amountOfOccupiedRooms = action.payload.data;
            draft.loading = false;
            return;
        case types.GET_AMOUNT_OCCUPIED_ROOMS_FAILURE:
            draft.error = action.payload.error;
            draft.loading = false;
            return;
        case types.GET_AMOUNT_INACTIVE_ROOMS:
            draft.loading = true;
            return;
        case types.GET_AMOUNT_INACTIVE_ROOMS_SUCCESS:
            draft.amountOfInactiveRooms = action.payload.data;
            draft.loading = false;
            return;
        case types.GET_AMOUNT_INACTIVE_ROOMS_FAILURE:
            draft.error = action.payload.error;
            draft.loading = false;
            return;
    }
}, INITAL_STATE);
