import { combineReducers } from "redux";
import { order } from "./order";
import { orderDetail } from "./orderDetail";
import { orderPurchases } from "./orderPurchases";
import { orderBookings } from "./orderBookings";
import { orderRefund } from "./orderRefund";
import { amountOfRooms } from "./amountOfRooms";

export default combineReducers({
    order,
    orderDetail,
    orderPurchases,
    orderBookings,
    orderRefund,
    amountOfRooms,
});
