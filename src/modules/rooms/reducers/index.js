import { combineReducers } from "redux";
import { room } from "./room";
import { type } from "./type";
import { create } from "./create";
import { media } from "./media";

export default combineReducers({
    room,
    type,
    create,
    media
});
