import produce from "immer";
import * as types from "../constants";

const INITAL_STATE = {
  list: [],
  currentRoom: {},
  loading: true,
  error: {},
};

export const room = produce((draft, action) => {
  switch (action.type) {
    case types.GET_ROOMS:
      draft.loading = true;
      return;
    case types.GET_ROOMS_SUCCESS:
      draft.list = action.payload.data;
      draft.loading = false;
      return;
    case types.GET_ROOMS.FAILURE:
      draft.error = action.payload.error;
      draft.loading = false;
      return;
    case types.GET_ROOM:
      draft.loading = true;
      return;
    case types.GET_ROOM_SUCCESS:
      draft.currentRoom = action.payload.data;
      draft.loading = false;
      return;
    case types.GET_ROOM.FAILURE:
      draft.error = action.payload.error;
      draft.loading = false;
      return;
  }
}, INITAL_STATE);
