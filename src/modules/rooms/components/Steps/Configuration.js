import { Heading, Grid, Box, TextInput, FormField, Text } from "grommet";
import { Formik, Form, FieldArray } from "formik";

import { SecondaryButton } from "ui/Common/Form/Button";

const Configuration = () => {
  return (
    <Box
      animation={[{ type: "fadeIn", duration: 300 }]}
      background="#fff"
      height={{ min: "100%" }}
    >
      <Box pad="medium" border={{ side: "bottom", color: "#E0E0E0" }}>
        <Heading size="medium" margin="0" level={2}>
          Конфигурация
        </Heading>
      </Box>
      <div className="grid-wrapper--content">
        <div className="grid-form-layout">
          <Box pad="medium">
            <Formik
              initialValues={{
                configurations: [
                  {
                    room_number: "",
                    floor: "",
                    status: "",
                  },
                ],
              }}
              onSubmit={async (values) => {
                await new Promise((r) => setTimeout(r, 500));
                alert(JSON.stringify(values, null, 2));
              }}
            >
              {({ values }) => (
                <Form>
                  <FieldArray name="configurations">
                    {({ insert, remove, push }) => (
                      <>
                        <Grid
                          columns={{
                            count: 3,
                            size: "auto",
                          }}
                          gap="small"
                        >
                          {values.configurations.length > 0 &&
                            values.configurations.map(
                              (configuration, index) => (
                                <>
                                  <FormField
                                    id={configuration.room_number}
                                    label={
                                      <Text textAlign="center">
                                        <Box align="center">№ комнаты </Box>
                                      </Text>
                                    }
                                    htmlFor="room-number"
                                  >
                                    <TextInput id="room-number" />
                                  </FormField>
                                  <FormField
                                    id={configuration.floor}
                                    label={<Box align="center">Этаж</Box>}
                                    htmlFor="room-number"
                                  >
                                    <TextInput id="room-number" />
                                  </FormField>
                                  <FormField
                                    id={configuration.status}
                                    label={<Box align="center">Статус</Box>}
                                    htmlFor="room-number"
                                  >
                                    <TextInput id="room-number" />
                                  </FormField>
                                </>
                              )
                            )}
                        </Grid>
                        <Box pad={{ vertical: "medium" }}>
                          <SecondaryButton
                            type="button"
                            onClick={() =>
                              push({
                                room_number: "",
                                floor: "",
                                status: "",
                              })
                            }
                            label="Добавить"
                          />
                        </Box>
                      </>
                    )}
                  </FieldArray>
                </Form>
              )}
            </Formik>
          </Box>
        </div>
      </div>
    </Box>
  );
};

export default Configuration;
