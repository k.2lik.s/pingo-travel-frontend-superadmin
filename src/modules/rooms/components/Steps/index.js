export { default as Information } from "./Information";
export { default as Gallery } from "./Gallery";
export { default as Price } from "./Price";
export { default as Configuration } from "./Configuration";
