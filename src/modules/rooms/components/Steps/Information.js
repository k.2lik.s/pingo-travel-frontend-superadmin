import { useState } from "react";
import { Box, Heading, FormField, TextInput, Text, TextArea } from "grommet";
import { FormSelect } from "ui/Common/Form/Selects";
import { Editor, EditorState } from "draft-js";
import "draft-js/dist/Draft.css";

const Information = ({
  selectOptions,
  setInformationForm,
  informationForm,
  handleChange,
  errors,
}) => {
  const [state, setState] = useState({
    editorState: EditorState.createEmpty(),
  });

  const onEditorStateChange = (editorState) => {
    setState({
      ...state,
      editorState,
    });
  };

  return (
    <Box
      animation={[{ type: "fadeIn", duration: 300 }]}
      height={{ min: "100%" }}
      background="#fff"
    >
      <Box pad="medium" border={{ side: "bottom", color: "#E0E0E0" }}>
        <Heading size="medium" margin="0" level={2}>
          Основная информация
        </Heading>
      </Box>
      <div className="grid-wrapper--content">
        <div className="grid-form-layout">
          <Box pad="medium">
            <FormField
              error={errors.types && errors.types[0]}
              label={
                <Text>
                  Тип комнаты
                  <Text color="red"> *</Text>
                </Text>
              }
              htmlFor="room-type"
            >
              <FormSelect
                id="room-type"
                labelKey="name"
                valueKey="id"
                value={informationForm.types}
                multiple
                closeOnChange={false}
                options={selectOptions.list}
                onChange={({ value: nextValue }) => {
                  setInformationForm({
                    ...informationForm,
                    types: nextValue,
                    adults: nextValue
                      ? Math.min(...nextValue.map((type) => type.guest))
                      : null,
                  });
                }}
              />
            </FormField>
            <FormField
              margin={{ vertical: "32px" }}
              error={errors.name && errors.name[0]}
              label={
                <Text>
                  Наименование
                  <Text color="red"> *</Text>
                </Text>
              }
              htmlFor="name"
            >
              <TextInput
                onChange={(e) => handleChange(e, "information")}
                value={informationForm.name}
                name="name"
                id="name"
              />
            </FormField>
            <FormField
              error={errors.short && errors.short[0]}
              label={
                <Text>
                  Краткое описание
                  <Text color="red"> *</Text>
                </Text>
              }
              htmlFor="short"
            >
              <TextArea
                id="short"
                onChange={(e) => handleChange(e, "information")}
                value={informationForm.short}
                name="short"
              />
            </FormField>
            <FormField
              margin={{ vertical: "32px" }}
              label={
                <Text>
                  Описание
                  <Text color="red"> *</Text>
                </Text>
              }
              htmlFor="description"
            >
              <TextArea
                onChange={(e) => handleChange(e, "information")}
                id="description"
                name="description"
                value={informationForm.description}
              />
            </FormField>

            <Text size="medium">
              <b>
                Размещение<Text color="red"> *</Text>
              </b>
            </Text>
            <Text size="small" margin={{ bottom: "xsmall", top: "small" }}>
              Установите заполняемость комнаты. Укажите, сколько взрослых и
              детей может вместить этот тип комнаты
            </Text>
            <FormField
              margin={{ top: "small" }}
              error={errors.adults && errors.adults[0]}
              label={
                <Text weight="normal" size="small">
                  Взрослые
                </Text>
              }
              htmlFor="adult"
            >
              <TextInput
                id="adults"
                type="number"
                min="1"
                onChange={(e) => handleChange(e, "information")}
                name="adults"
                value={informationForm.adults}
              />
            </FormField>
            <FormField
              error={errors.children && errors.children[0]}
              margin={{ top: "small" }}
              label={
                <Text weight="normal" size="small">
                  Дети
                </Text>
              }
              htmlFor="child"
            >
              <TextInput
                id="children"
                min="1"
                type="number"
                onChange={(e) => handleChange(e, "information")}
                name="children"
                value={informationForm.child}
              />
            </FormField>
          </Box>
        </div>
      </div>
    </Box>
  );
};

export default Information;
