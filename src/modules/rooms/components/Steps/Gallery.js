import { Heading, Grid, Box, Text } from "grommet";
import styled from "styled-components";
import { UploadIcon, PreviewCloseIcon } from "ui/Common/Icon";

const UploadBox = styled.label`
  border: 0.5px dashed #bdbdbd;
  cursor: pointer;
  border-radius: 8px;
  text-align: center;
`;
const PreviewIconWrapper = styled.div`
  position: absolute;
  right: 12px;
  top: 12px;
  cursor: pointer;
`;

const PreviewCoverBox = styled(Box)`
  min-height: 424px;
  position: relative;
  background-image: ${(props) => `url(${props.src})`};
  background-size: cover;
  background-position: center;
  position: relative;
  ${(props) =>
    props.loading &&
    `  &::after {
    content: "";
    background-color: rgba(255, 255, 255, 0.7);
    position: absolute;
    height: 100%;
    width: 100%;
  }`}
`;

const PreviewGalleryBox = styled(Box)`
  min-height: 220px;
  min-height: 220px;
  position: relative;
  background-image: ${(props) => `url(${props.src})`};
  background-size: cover;
  background-position: center;
  ${(props) =>
    props.loading &&
    `  &::after {
    content: "";
    background-color: rgba(255, 255, 255, 0.7);
    position: absolute;
    height: 100%;
    width: 100%;
  }`}
`;

const Gallery = ({
  handleImagesChange,
  handleCoverChange,
  handleDeleteImage,
  media,
}) => {

  return (
    <Box animation={[{ type: "fadeIn", duration: 300 }]} background="#fff">
      <Box pad="medium" border={{ side: "bottom", color: "#E0E0E0" }}>
        <Heading size="medium" margin="0" level={2}>
          Галерея
        </Heading>
      </Box>
      <div className="grid-wrapper--content">
        <div className="grid-form-layout">
          <Box pad="medium">
            <Text>
              Обложка<Text color="red"> *</Text>
            </Text>
            <Text size="small" margin={{ bottom: "xsmall", top: "small" }}>
              Загрузите обложку из предложенных. Он должен привлекать внимание
              зрителей и отражать содержание видео.
            </Text>
            {media.cover.src && (
              <PreviewCoverBox
                src={media.cover.src}
                loading={media.loading}
                margin={{ bottom: "small" }}
              >
                <PreviewIconWrapper
                  onClick={() =>
                    handleDeleteImage({
                      mediaId: media.cover.id,
                      collection: "cover",
                    })
                  }
                >
                  <PreviewCloseIcon width={24} height={24} />
                </PreviewIconWrapper>
              </PreviewCoverBox>
            )}

            <UploadBox for="upload">
              <Box direction="column" align="center" pad={{ vertical: "38px" }}>
                <Box>
                  <UploadIcon width="32" height="32" />
                </Box>
                <Box>
                  <Text size="small"> Заменить обложку</Text>
                </Box>
                <input
                  type="file"
                  id="upload"
                  onChange={(e) => handleCoverChange(e)}
                  style={{ display: "none" }}
                />
              </Box>
            </UploadBox>
            <Box pad={{ vertical: "medium" }}>
              <Text>
                Галерея <Text color="red"> *</Text>
              </Text>
              <Text size="small" margin={{ bottom: "xsmall", top: "small" }}>
                Отличные фотографии помогут гостям получить полное представление
                о вашем объекте. Загрузите изображения хорошего качества,
                позволяющие увидеть ваш объект со всех сторон. Эти фотографии
                будут показаны на странице вашего объекта размещения на сайте.
              </Text>
              <UploadBox for="upload-multiple">
                <Box
                  direction="column"
                  align="center"
                  pad={{ vertical: "38px" }}
                >
                  <Box>
                    <UploadIcon width="32" height="32" />
                  </Box>
                  <Box>
                    <Text size="small"> Загрузить фотографии</Text>
                  </Box>
                  <input
                    type="file"
                    id="upload-multiple"
                    multiple
                    onChange={(e) => handleImagesChange(e)}
                    style={{ display: "none" }}
                  />
                </Box>
              </UploadBox>
              <Grid
                columns={{
                  count: 3,
                  size: "auto",
                }}
                gap="small"
                margin={{ top: "medium" }}
              >
                {media.gallery.map((image) => (
                  <PreviewGalleryBox loading={media.loading} src={image.src}>
                    <PreviewIconWrapper
                      onClick={() =>
                        handleDeleteImage({
                          mediaId: image.id,
                          collection: "gallery",
                        })
                      }
                    >
                      <PreviewCloseIcon width={24} height={24} />
                    </PreviewIconWrapper>
                  </PreviewGalleryBox>
                ))}
              </Grid>
            </Box>
          </Box>
        </div>
      </div>
    </Box>
  );
};



export default Gallery;
