import {
  Heading,
  Box,
  TextInput,
  DateInput,
  Button,
  FormField,
  Text,
} from "grommet";
import { Previous, Next } from "grommet-icons";
import { Formik, Form, FieldArray } from "formik";
import { FormSelect, InputSelect } from "ui/Common/Form/Selects";

const dateFormat = new Intl.DateTimeFormat("ru-RU", {
  month: "long",
  day: "numeric",
});

const Price = ({dateValue, setDateValue, onDateChange}) => {
  return (
    <Box animation={[{ type: "fadeIn", duration: 300 }]}>
      <Box pad="medium" background="#fff">
        <div className="grid-wrapper--content">
          <div className="grid-form-layout">
            <Text margin={{ bottom: "xsmall" }} size="medium">
              <b>Базовая цена за комнату</b> <Text color="red"> *</Text>
            </Text>

            <Box direction="row" align="center" round="xsmall" border>
              <TextInput plain />
              <Box
                background="#F2F2F2"
                round={{ corner: "right", size: "xsmall" }}
                border={{ side: "left" }}
                pad={{ vertical: "small", horizontal: "medium" }}
              >
                <Text size="small">KZT</Text>
              </Box>
            </Box>
          </div>
        </div>
      </Box>

      <Formik
        initialValues={{
          prices: [
            {
              base_price: "",
              type: "",
              dateStart: "",
              dateEnd: "",
              price: "",
            },
          ],
        }}
        onSubmit={async (values) => {
          await new Promise((r) => setTimeout(r, 500));
          alert(JSON.stringify(values, null, 2));
        }}
      >
        {({ values }) => (
          <Form>
            <FieldArray name="prices">
              {({ insert, remove, push }) => (
                <>
                  {values.prices.length > 0 &&
                    values.prices.map((price, index) => (
                      <>
                        <Box background="#fff" margin={{ vertical: "medium" }}>
                          <Box
                            border={{
                              side: "bottom",
                              color: "#E0E0E0",
                            }}
                          >
                            <Box
                              pad={{
                                vertical: "medium",
                                left: "small",
                              }}
                            >
                              <TextInput
                                border={false}
                                placeholder="Название специальной цены"
                                plain
                              />
                            </Box>
                          </Box>
                          <div className="grid-wrapper--content">
                            <div className="grid-form-layout">
                              <Box pad={{ left: "medium", top: "medium" }}>
                                <FormField
                                  label={
                                    <Text>
                                      Тип
                                      <Text color="red"> *</Text>
                                    </Text>
                                  }
                                  htmlFor="price-type"
                                >
                                  <FormSelect
                                    options={["one", "two"]}
                                    onChange={({ option }) =>
                                      console.log(option)
                                    }
                                    id="price-type"
                                  />
                                </FormField>
                                <FormField
                                  margin={{ vertical: "medium" }}
                                  label={
                                    <Text>
                                      Укажите дату
                                      <Text color="red"> *</Text>
                                    </Text>
                                  }
                                  htmlFor="price-type"
                                >
                                  <DateInput
                                    value={dateValue}
                                    buttonProps={{
                                      label: (
                                        <div style={{ padding: "8px 0" }}>
                                          {dateFormat.format(
                                            new Date(dateValue[0])
                                          )}{" "}
                                          -{" "}
                                          {dateFormat.format(
                                            new Date(dateValue[1])
                                          )}
                                        </div>
                                      ),
                                      icon: false,
                                    }}
                                    calendarProps={{
                                      locale: "ru-RU",
                                      fill: true,
                                      daysOfWeek: true,
                                      header: ({
                                        date: currentDate,
                                        locale,
                                        onNextMonth,
                                        onPreviousMonth,
                                        nextInBound,
                                        ...rest
                                      }) => (
                                        <Box
                                          direction="row"
                                          align="center"
                                          justify="between"
                                        >
                                          <Button
                                            disabled={!nextInBound}
                                            icon={<Previous />}
                                            onClick={onPreviousMonth}
                                          />
                                          <Heading
                                            level={3}
                                            margin={{
                                              vertical: "medium",
                                            }}
                                          >
                                            {currentDate.toLocaleDateString(
                                              locale,
                                              {
                                                month: "long",
                                                year: "numeric",
                                              }
                                            )}
                                          </Heading>
                                          <Button
                                            disabled={!nextInBound}
                                            icon={<Next />}
                                            onClick={onNextMonth}
                                          />
                                        </Box>
                                      ),
                                    }}
                                    onChange={onDateChange}
                                  />
                                </FormField>
                                <FormField
                                  margin={{ bottom: "medium" }}
                                  label={
                                    <Text>
                                      Укажите цену
                                      <Text color="red"> *</Text>
                                    </Text>
                                  }
                                  htmlFor="currency-type"
                                >
                                  <Box
                                    direction="row"
                                    align="center"
                                    round="xsmall"
                                    border
                                  >
                                    <TextInput plain />
                                    <Box
                                      background="#F2F2F2"
                                      round={{
                                        corner: "right",
                                        size: "xsmall",
                                      }}
                                      border={{ side: "left" }}
                                      width="small"
                                      pad="0"
                                    >
                                      <InputSelect
                                        options={["KZT", "USD"]}
                                        value={["KZT"]}
                                        onChange={({ option }) =>
                                          console.log(option)
                                        }
                                        id="currency-type"
                                      />
                                    </Box>
                                  </Box>
                                </FormField>
                              </Box>
                            </div>
                          </div>
                        </Box>
                      </>
                    ))}

                  <Box margin={{ top: "medium" }}>
                    <Button
                      onClick={() =>
                        push({
                          base_price: "",
                          type: "",
                          dateStart: "",
                          dateEnd: "",
                          price: "",
                        })
                      }
                      alignSelf="start"
                      size="medium"
                      type="button"
                      primary
                      label="Специальная цена"
                    />
                  </Box>
                </>
              )}
            </FieldArray>
          </Form>
        )}
      </Formik>
    </Box>
  );
};

export default Price;
