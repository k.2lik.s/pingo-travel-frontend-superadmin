import * as types from "./constants";
import sendRequest from "settings/sendRequest";
import Nprogress from "nprogress";

export const getRooms = (vendorId) => (dispatch) => {
  dispatch({ type: types.GET_ROOMS });
  Nprogress.start();
  sendRequest(`/vendors/${vendorId}/rooms`, {
    method: "GET",
  })
    .then(({ data }) => {
      dispatch({ type: types.GET_ROOMS_SUCCESS, payload: data });
      Nprogress.done();
    })
    .catch((error) => {
      dispatch({ type: types.GET_ROOMS_FAILURE, payload: error });
    });
};

export const getCover = (roomId) => (dispatch) => {
  dispatch({ type: types.GET_COVER });
  sendRequest(`/rooms/${roomId}/medias?collection=cover`, {
    method: "GET",
  })
    .then(({ data }) => {
      dispatch({ type: types.GET_COVER_SUCCESS, payload: data });
    })
    .catch((error) => {
      dispatch({ type: types.GET_COVER_FAILURE, payload: error });
    });
};

export const addMedia = ({ roomId, collection, file }) => (dispatch) => {
  dispatch({ type: types.ADD_MEDIA_LOADING });

  const formData = new FormData();
  formData.append("collection", collection);
  formData.append("file", file);

  sendRequest(`/rooms/${roomId}/medias?collection=${collection}`, {
    headers: {
      Accept: "application/json",
      "Content-Type": "multipart/form-data",
    },
    method: "POST",
    data: formData,
  })
    .then(({ data }) => {
      dispatch({
        type: types.ADD_MEDIA_SUCCESS,
        payload: { key: collection, data: data.data },
      });
    })
    .catch((error) => {
      dispatch({ type: types.ADD_MEDIA_FAILURE, payload: error });
    });
};

export const removeMedia = ({ mediaId, roomId, collection }) => (dispatch) => {
  dispatch({ type: types.REMOVE_MEDIA_LOADING });

  sendRequest(`/rooms/${roomId}/medias/${mediaId}`, {
    method: "DELETE",
  })
    .then(({ data }) => {
      dispatch({
        type: types.REMOVE_MEDIA_SUCCESS,
        payload: { key: collection, id: mediaId },
      });
    })
    .catch((error) => {
      dispatch({ type: types.REMOVE_MEDIA_FAILURE, payload: error });
    });
};

export const getGallery = (roomId) => (dispatch) => {
  dispatch({ type: types.GET_GALLERY });
  sendRequest(`/rooms/${roomId}/medias?collection=gallery`, {
    method: "GET",
  })
    .then(({ data }) => {
      dispatch({ type: types.GET_GALLERY_SUCCESS, payload: data });
    })
    .catch((error) => {
      dispatch({ type: types.GET_GALLERY_FAILURE, payload: error });
    });
};

export const getRoomTypes = () => (dispatch) => {
  dispatch({ type: types.GET_ROOM_TYPES });
  sendRequest(`/room-types`, {
    method: "GET",
  })
    .then(({ data }) => {
      dispatch({ type: types.GET_ROOM_TYPES_SUCCESS, payload: data });
    })
    .catch((error) => {
      dispatch({ type: types.GET_ROOM_TYPES_FAILURE, payload: error });
    });
};

export const createRoom = (vendorId, form) => (dispatch) => {
  dispatch({ type: types.CREATE_LOADING });

  sendRequest(`/vendors/${vendorId}/rooms`, {
    method: "POST",
    data: JSON.stringify(form),
  })
    .then(({ data }) => {
      dispatch({ type: types.CREATE_SUCCESS, payload: data });
    })
    .catch(({ response }) => {
      dispatch({ type: types.CREATE_FAILURE, payload: response.data });
    });
};

export const reset = () => dispatch => {
  dispatch({ type: types.RESET_CREATE_ROOM });

}