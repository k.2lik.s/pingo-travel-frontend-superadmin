import { useState, useEffect } from "react";
import { connect } from "react-redux";
import { useParams, useHistory } from "react-router-dom";
import {
  Heading,
  Grid,
  Box,
  Button,
  FormField,
  CheckBoxGroup,
  CheckBox,
  Text,
  Form as GrommetForm,
} from "grommet";

import {
  getRoomTypes,
  createRoom,
  reset,
  addMedia,
  removeMedia,
} from "../actions";

import {
  Information,
  Gallery,
  Configuration,
  Price,
} from "../components/Steps";
import Navigation from "../components/Navigation";

import { GrayButton } from "ui/Common/Form/Button";

const RoomCreate = ({
  getRoomTypes,
  roomTypes,
  create,
  vendorId,
  createRoom,
  reset,
  addMedia,
  removeMedia,
  media,
}) => {
  const { stepId } = useParams();
  const [dateValue, setDateValue] = useState([
    "2020-07-31T15:27:42.920Z",
    "2020-08-07T15:27:42.920Z",
  ]);
  const [features, setFeatures] = useState();
  const [informationForm, setInformationForm] = useState({
    types: [],
    name: "",
    short: "",
    description: "",
    adults: null,
    children: "",
    status: "inactive",
    price: 0,
  });

  useEffect(() => {
    getRoomTypes();

    return () => {
      reset();
    };
  }, []);

  const history = useHistory();

  useEffect(() => {
    if (!create.nextStep) {
      history.push(`/manager/rooms/create/information`);
    }
    if (create.nextStep) {
      history.push(`/manager/rooms/create/gallery`);
    }
  }, [create]);

  const handleChange = (event, stateKey) => {
    if (stateKey === "information") {
      setInformationForm({
        ...informationForm,
        [event.target.name]: event.target.value,
      });
    }
  };

  const handleSubmit = () => {
    if (stepId === "information") {
      const typeIds = informationForm.types.map((type) => type.id);
      createRoom(vendorId, {
        ...informationForm,
        types: typeIds,
      });
    }
    if (stepId === "gallery") {
      history.push(`/manager/rooms/create/configuration`);
    }
  };

  const onDateChange = (event) => {
    const nextValue = event.value;
    console.log("onDateChange", nextValue);
    setDateValue(nextValue);
  };

  const resetFileValue = (event, file) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      event.target.value = null;
    };
  };

  const handleCoverChange = (event, stateKey) => {
    const file = event.target.files[0];
    addMedia({
      collection: "cover",
      file,
      roomId: create.roomId,
    });
    resetFileValue(event, file);
  };

  const handleImagesChange = (event) => {
    const file = event.target.files[0];

    addMedia({
      collection: "gallery",
      file,
      roomId: create.roomId,
    });

    resetFileValue(event, file);
  };

  const handleDeleteImage = ({ mediaId, collection }) =>
    removeMedia({
      mediaId,
      collection,
      roomId: create.roomId,
    });

  return (
    <Box animation={[{ type: "fadeIn", duration: 300 }]}>
      <div className="grid-wrapper">
        <Heading size="large" level={2} margin={{ top: "xsmall" }}>
          Добавить комнату
        </Heading>

        <div className="grid-content">
          {stepId === "information" && (
            <Information
              selectOptions={roomTypes}
              errors={create.errors}
              handleChange={handleChange}
              informationForm={informationForm}
              setInformationForm={setInformationForm}
            />
          )}
          {stepId === "gallery" && (
            <Gallery
              media={media}
              handleDeleteImage={handleDeleteImage}
              handleImagesChange={handleImagesChange}
              handleCoverChange={handleCoverChange}
            />
          )}
          {stepId === "configuration" && <Configuration />}
          {stepId === "price" && (
            <Price
              dateValue={dateValue}
              setDateValue={setDateValue}
              onDateChange={onDateChange}
            />
          )}
          {stepId === "features" && (
            <Box
              animation={[{ type: "fadeIn", duration: 300 }]}
              background="#fff"
              height={{ min: "100%" }}
            >
              <Box pad="medium" border={{ side: "bottom", color: "#E0E0E0" }}>
                <Heading size="medium" margin="0" level={2}>
                  Удобства
                </Heading>
              </Box>
              <Box pad="medium">
                <Box pad={{ bottom: "medium" }}>
                  <Text size="medium">
                    <b>Расскажите нам о своих особенностях </b>
                    <Text color="red">*</Text>
                  </Text>
                </Box>
                <GrommetForm
                  onSubmit={({ value: values, touched }) =>
                    // 'touched' is a single boolean value indication of
                    // whether any of the checkboxes had changed.
                    console.log("Submit", values, touched)
                  }
                >
                  <FormField name="controlled">
                    <CheckBoxGroup
                      id="check-box-group-id"
                      name="controlled"
                      value={features}
                      onChange={({ value: nextValue }) =>
                        setFeatures(nextValue)
                      }
                      options={[
                        "Бесплатный Wi-Fi",
                        "Ресторан",
                        "Доставка еды и напитков в номер",
                        "Бар",
                      ]}
                    />
                  </FormField>
                </GrommetForm>
              </Box>
            </Box>
          )}
          {stepId === "comfort" && (
            <Box
              animation={[{ type: "fadeIn", duration: 300 }]}
              background="#fff"
              height={{ min: "100%" }}
            >
              <Box pad="medium" border={{ side: "bottom", color: "#E0E0E0" }}>
                <Heading size="medium" margin="0" level={2}>
                  Дополнительные услуги
                </Heading>
              </Box>
              <Box pad="medium">
                <Box pad={{ bottom: "medium" }}>
                  <Text size="medium">
                    <b>Расскажите нам о своих удобствах</b>
                  </Text>
                </Box>
                <Grid
                  columns={{
                    count: 2,
                    size: "auto",
                  }}
                  gap={{ column: "medium" }}
                  alignContent="center"
                >
                  <Box
                    pad={{ vertical: "medium" }}
                    border={{ side: "bottom", color: "#E0E0E0" }}
                  >
                    <CheckBox
                      size="xsmall"
                      label={<Text size="small">Бесплатный Wi-Fi</Text>}
                    />
                  </Box>
                  <Box
                    pad={{ vertical: "medium" }}
                    border={{ side: "bottom", color: "#E0E0E0" }}
                  >
                    <CheckBox
                      size="xsmall"
                      label={<Text size="small">Бесплатный Wi-Fi</Text>}
                    />
                  </Box>
                  <Box
                    pad={{ vertical: "medium" }}
                    border={{ side: "bottom", color: "#E0E0E0" }}
                  >
                    <CheckBox
                      size="xsmall"
                      label={<Text size="small">Бесплатный Wi-Fi</Text>}
                    />
                  </Box>
                  <Box
                    pad={{ vertical: "medium" }}
                    border={{ side: "bottom", color: "#E0E0E0" }}
                  >
                    <CheckBox
                      size="xsmall"
                      label={<Text size="small">Бесплатный Wi-Fi</Text>}
                    />
                  </Box>
                  <Box
                    pad={{ vertical: "medium" }}
                    border={{ side: "bottom", color: "#E0E0E0" }}
                  >
                    <CheckBox
                      size="xsmall"
                      label={<Text size="small">Бесплатный Wi-Fi</Text>}
                    />
                  </Box>
                  <Box
                    pad={{ vertical: "medium" }}
                    border={{ side: "bottom", color: "#E0E0E0" }}
                  >
                    <CheckBox
                      size="xsmall"
                      label={<Text size="small">Бесплатный Wi-Fi</Text>}
                    />
                  </Box>
                  <Box
                    pad={{ vertical: "medium" }}
                    border={{ side: "bottom", color: "#E0E0E0" }}
                  >
                    <CheckBox
                      size="xsmall"
                      label={<Text size="small">Бесплатный Wi-Fi</Text>}
                    />
                  </Box>
                  <Box
                    pad={{ vertical: "medium" }}
                    border={{ side: "bottom", color: "#E0E0E0" }}
                  >
                    <CheckBox
                      size="xsmall"
                      label={<Text size="small">Бесплатный Wi-Fi</Text>}
                    />
                  </Box>
                  <Box
                    pad={{ vertical: "medium" }}
                    border={{ side: "bottom", color: "#E0E0E0" }}
                  >
                    <CheckBox
                      size="xsmall"
                      label={<Text size="small">Бесплатный Wi-Fi</Text>}
                    />
                  </Box>
                  <Box
                    pad={{ vertical: "medium" }}
                    border={{ side: "bottom", color: "#E0E0E0" }}
                  >
                    <CheckBox
                      size="xsmall"
                      label={<Text size="small">Бесплатный Wi-Fi</Text>}
                    />
                  </Box>
                </Grid>
              </Box>
            </Box>
          )}
        </div>
        <div className="grid-navigation">
          <Navigation
            createSuccess={create.success}
            mediaSuccess={media.success}
            stepId={stepId}
          />
        </div>
        <div className="grid-wrapper">
          <div className="grid-content">
            <Box justify="between" direction="row" margin={{ top: "medium" }}>
              <Box width="48%">
                <GrayButton label="Вернуться" />
              </Box>
              <Box width="48%">
                <Button
                  onClick={() => handleSubmit()}
                  primary
                  disabled={create.loading}
                  label="Продолжить"
                />
              </Box>
            </Box>
          </div>
        </div>
      </div>
    </Box>
  );
};

const mapToStateProps = (state) => ({
  roomTypes: state.room.type,
  vendorId: state.common.vendor.currentVendor.id,
  create: state.room.create,
  media: state.room.media,
});

export default connect(mapToStateProps, {
  getRoomTypes,
  createRoom,
  addMedia,
  removeMedia,
  reset,
})(RoomCreate);
