import { useState, useEffect } from "react";
import { withRouter, useRouteMatch } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import { getRooms } from "modules/rooms/actions";
import {
  Table,
  TableBody,
  TableCell,
  TableHeader,
  TableRow as GrommetTableRow,
  Text,
  CheckBox,
  Box,
  TextInput,
  Heading,
  Button,
} from "grommet";

import Nprogress from "nprogress";

import Spinner from "ui/Common/Progress";

const TableRow = styled(GrommetTableRow)`
  background: ${(props) =>
    props.checked ? "rgba(47, 128, 237, 0.1)" : "#fff"};

  &:hover {
    background: rgba(47, 128, 237, 0.1);
    cursor: pointer;
  }
`;

const Rooms = ({ history }) => {

  const { vendor } = useSelector((state) => state.common);
  const { room } = useSelector((state) => state.room);
  const dispatch = useDispatch();
  useEffect(() => {
    if (!vendor.loading) {
      dispatch(getRooms(vendor.currentVendor.id));
    }
  }, [vendor]);

  // const onCheckAll = () => {
  //   if (data.every((item) => item.checked === true)) {
  //     setData(data.map((item) => ({ ...item, checked: false })));
  //   } else {
  //     setData(data.map((item) => ({ ...item, checked: true })));
  //   }
  // };

  const onCheck = (id) => {
    return null;
  };

  let { url } = useRouteMatch();

  return (
    <Box animation={[{ type: "fadeIn", duration: 300 }]}>
      <Box justify="between" direction="row">
        <Box>
          <Heading size="large" level={2} margin={{ top: "xsmall" }}>
            Комнаты
          </Heading>
        </Box>
        <Box>
          <Button
            size="large"
            primary
            type="submit"
            label="Добавить комнату"
            onClick={() => history.push(`${url}/create/information`)}
          ></Button>
        </Box>
      </Box>

      <Table>
        <TableHeader>
          <TableRow>
            <TableCell>
              <Box justify="between" direction="row">
                <Box pad={{ right: "small" }}>
                  <CheckBox
                    label="№"
                    // checked={data.every((item) => item.checked === true)}
                    // indeterminate={data.every((item) => item.checked !== true)}
                    // onChange={onCheckAll}
                  />
                </Box>
                <Text>Наименование</Text>
              </Box>
              <Box pad={{ top: "medium", left: "medium" }} align="end">
                <TextInput placeholder="Наименование" />
              </Box>
            </TableCell>
            <TableCell align="start">
              <Text>Взрослые</Text>
              <Box pad={{ top: "medium", left: 0 }} align="start">
                <TextInput placeholder="Взрослые" />
              </Box>
            </TableCell>
            <TableCell align="start">
              <Text>Дети</Text>
              <Box pad={{ top: "medium", left: 0 }}>
                <TextInput placeholder="Дети" />
              </Box>
            </TableCell>
            <TableCell align="start">
              <Text truncate>Всего комнат</Text>
              <Box pad={{ top: "medium", left: 0 }}>
                <TextInput placeholder="Всего комнат" />
              </Box>
            </TableCell>
            <TableCell align="start">
              <Text>Цена</Text>
              <Box pad={{ top: "medium", left: 0 }}>
                <TextInput placeholder="Цена" />
              </Box>
            </TableCell>
            <TableCell align="start">
              <Text>Статус</Text>
              <Box pad={{ top: "medium", left: 0 }} align="center">
                <TextInput placeholder="Статус" />
              </Box>
            </TableCell>
          </TableRow>
        </TableHeader>
        <TableBody>
          {room.list.map((item, i) => (
            <TableRow
              key={item.id}
              checked={item.checked}
              onClick={(e) => onCheck(item.id)}
            >
              <TableCell>
                <Box direction="row" justify="between">
                  <Box direction="row" justify="between" width="xxsmall">
                    <CheckBox checked={item.checked} />
                    <Box align="center" alignSelf="center">
                      <Text>{i + 1}</Text>
                    </Box>
                  </Box>
                  <Box width="small" pad={{ left: "medium" }}>
                    <Text>{item.name}</Text>
                  </Box>
                </Box>
              </TableCell>
              <TableCell align="center">
                <Text>{item.adults}</Text>
              </TableCell>
              <TableCell align="center">
                <Text>{item.children}</Text>
              </TableCell>
              <TableCell align="center">
                <Text>{item.amount_room}</Text>
              </TableCell>
              <TableCell align="center">
                <Text>{item.price}</Text>
              </TableCell>
              <TableCell align="center">
                <Text>{item.status}</Text>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Box>
  );
};
export default withRouter(Rooms);
