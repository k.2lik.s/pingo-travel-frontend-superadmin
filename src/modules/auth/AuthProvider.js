import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { getProfile } from "./api";
import { getOwnVendors } from "modules/common/actions";
export const AuthContext = React.createContext();

const AuthProvider = ({ children }) => {
  const [state, setState] = React.useState({
    status: "pending",
    error: null,
    user: null,
  });

  const dispatch = useDispatch();

  useEffect(() => {
    getProfile()
      .then((profile) =>
        setState({
          ...state,
          status: "success",
          error: null,
          user: profile.data.data,
        })
      )
      .then(() => {
        dispatch(getOwnVendors());
      })
      .catch((error) => {
        setState({ ...state, status: "error", error, user: null });
      });
  }, []);

  if (state.status === "pending") {
    return null;
  }

  return <AuthContext.Provider value={state}>{children}</AuthContext.Provider>;
};

const useAuthState = () => {
  const state = React.useContext(AuthContext);
  const isSuccess = state.status === "success";
  const isAuthenticated = state.user && isSuccess;
  return {
    ...state,
    isAuthenticated,
  };
};

export { AuthProvider, useAuthState };
