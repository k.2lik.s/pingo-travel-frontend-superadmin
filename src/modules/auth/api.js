import sendRequest from "settings/sendRequest";

export const getProfile = () => {
  return sendRequest("/me", {
    method: "GET",
  });
};

export const login = (form) => {
  return sendRequest("/login", {
    method: "POST",
    data: JSON.stringify(form),
  });
};

export const updateProfile = (form) => {
  return sendRequest("/me", {
    method: "POST",
    data: JSON.stringify(form),
  });
};
