import { useState, useEffect } from "react";
import { connect } from "react-redux";
import { useParams, useHistory } from "react-router-dom";
import {
  Heading,
  Box,
  Button,
  FormField,
  Text,
  TextInput,
  TextArea,
} from "grommet";

import {
  getVendorTypes,
  getVendorFeatures,
  createVendor,
  getRegions,
  reset,
} from "../actions";
import { FormSelect } from "ui/Common/Form/Selects";
import { GrayButton } from "ui/Common/Form/Button";

const RoomCreate = ({
  getVendorTypes,
  getVendorFeatures,
  getRegions,
  vendorTypes,
  vendorFeatures,
  regions,
  create,
  createVendor,
  reset,
}) => {
  const { stepId } = useParams();
  const [form, setForm] = useState({
    company_name: "",
    name: "",
    fee: "",
    legal_address: "",
    email: "",
    phone: "",
    region: 0,
    location: "",
    website: "",
    types: [],
    features: [],
    status: "inactive",
    coordinates: {lat: 43.2566, lng:76.9286, zoom: 3},
  });
  const errors = create.errors;

  useEffect(() => {
    getVendorFeatures();
    getVendorTypes();
    getRegions();

    return () => {
      reset();
    };
  }, []);

  const history = useHistory();

  const handleChange = (event, stateKey) => {
    if (stateKey === "information") {
      setForm({
        ...form,
        [event.target.name]: event.target.value,
      });
    }
  };

  const handleSubmit = () => {
    const typeIds = form.types.map((type) => type.id);
    const featureIds = form.features.map(f => f.id);
    createVendor({
      ...form,
      types: typeIds,
      features: featureIds,
    });
  };

  return (
    <Box animation={[{ type: "fadeIn", duration: 300 }]}>
      <div className="grid-wrapper">
        <Heading size="large" level={2} margin={{ top: "xsmall" }}>
          Добавить агентство
        </Heading>

        <div className="grid-content">
          <Box
            animation={[{ type: "fadeIn", duration: 300 }]}
            height={{ min: "100%" }}
            background="#fff"
          >
            <Box pad="medium" border={{ side: "bottom", color: "#E0E0E0" }}>
              <Heading size="medium" margin="0" level={2}>
                Информация
              </Heading>
            </Box>
            <div className="grid-wrapper--content">
              <div className="grid-form-layout">
                <Box pad="medium">
                  <FormField
                    margin={{ top: "32px" }}
                    error={errors.company_name && errors.company_name[0]}
                    label={
                      <Text>
                        Наименование компании
                        <Text color="red"> *</Text>
                      </Text>
                    }
                    htmlFor="company_name"
                  >
                    <TextInput
                      onChange={(e) => handleChange(e, "information")}
                      value={form.company_name}
                      name="company_name"
                      id="company_name"
                    />
                  </FormField>

                  <FormField
                      margin={{ top: "32px" }}
                      error={errors.fee && errors.fee[0]}
                      label={
                        <Text>
                          Плата
                          <Text color="red"> *</Text>
                        </Text>
                      }
                      htmlFor="fee"
                  >
                    <TextInput
                        onChange={(e) => handleChange(e, "information")}
                        value={form.fee}
                        name="fee"
                        type="number"
                        id="fee"
                    />
                  </FormField>

                  <FormField
                      margin={{ top: "32px" }}
                      error={errors.legal_address && errors.legal_address[0]}
                      label={
                        <Text>
                          Юридический адрес
                          <Text color="red"> *</Text>
                        </Text>
                      }
                      htmlFor="legal_address"
                  >
                    <TextInput
                        onChange={(e) => handleChange(e, "information")}
                        value={form.legal_address}
                        name="legal_address"
                        id="legal_address"
                    />
                  </FormField>

                  <FormField
                    margin={{ top: "32px" }}
                    error={errors.email && errors.email[0]}
                    label={
                      <Text>E-mail</Text>
                    }
                    htmlFor="email"
                  >
                    <TextInput
                      onChange={(e) => handleChange(e, "information")}
                      value={form.email}
                      name="email"
                      id="email"
                    />
                  </FormField>

                  <FormField
                    margin={{ top: "32px" }}
                    error={errors.location && errors.location[0]}
                    label={
                      <Text>
                        Место расположения
                        <Text color="red"> *</Text>
                      </Text>
                    }
                    htmlFor="location"
                  >
                    <TextInput
                      onChange={(e) => handleChange(e, "information")}
                      value={form.location}
                      name="location"
                      id="location"
                    />
                  </FormField>

                  <FormField
                      margin={{ top: "32px" }}
                      error={errors.name && errors.name[0]}
                      label={
                        <Text>
                          Наименование
                          <Text color="red"> *</Text>
                        </Text>
                      }
                      htmlFor="name"
                  >
                    <TextInput
                        onChange={(e) => handleChange(e, "information")}
                        value={form.name}
                        name="name"
                        id="name"
                    />
                  </FormField>

                  <FormField
                      margin={{ top: "32px" }}
                      error={errors.phone && errors.phone[0]}
                      label={
                        <Text>
                          Телефон
                          <Text color="red"> *</Text>
                        </Text>
                      }
                      htmlFor="phone"
                  >
                    <TextInput
                        onChange={(e) => handleChange(e, "information")}
                        value={form.phone}
                        name="phone"
                        id="phone"
                    />
                  </FormField>

                  <FormField
                      margin={{ top: "32px" }}
                      error={errors.region && errors.region[0]}
                      label={
                        <Text>
                          Регион
                          <Text color="red"> *</Text>
                        </Text>
                      }
                      htmlFor="region"
                  >
                    <FormSelect
                        id="region"
                        labelKey="name"
                        valueKey="id"
                        value={regions.list.find(r => r.id === form.region)}
                        options={regions.list}
                        onChange={({ value: nextValue }) => {
                          setForm({
                            ...form,
                            region: nextValue.id,
                          });
                        }}
                    />
                  </FormField>


                  <FormField
                    margin={{ top: "32px" }}
                    error={errors.website && errors.website[0]}
                    label={
                      <Text>
                        Сайт
                      </Text>
                    }
                    htmlFor="website"
                  >
                    <TextInput
                      onChange={(e) => handleChange(e, "information")}
                      value={form.website}
                      name="website"
                      id="website"
                    />
                  </FormField>

                  <FormField
                    margin={{ top: "32px" }}
                    error={errors.types && errors.types[0]}
                    label={
                      <Text>
                        Тип агентства
                        <Text color="red"> *</Text>
                      </Text>
                    }
                    htmlFor="vendor-type"
                  >
                    <FormSelect
                      id="vendor-type"
                      labelKey="name"
                      valueKey="id"
                      value={form.types}
                      multiple
                      closeOnChange={false}
                      options={vendorTypes.list}
                      onChange={({ value: nextValue }) => {
                        setForm({
                          ...form,
                          types: nextValue,
                        });
                      }}
                    />
                  </FormField>

                  <FormField
                    margin={{ top: "32px" }}
                    error={errors.features && errors.features[0]}
                    label={
                      <Text>
                        Дополнительные услуги
                      </Text>
                    }
                    htmlFor="vendor-feature"
                  >
                    <FormSelect
                      id="vendor-feature"
                      labelKey="name"
                      valueKey="id"
                      value={form.features}
                      multiple
                      closeOnChange={false}
                      options={vendorFeatures.list}
                      onChange={({ value: nextValue }) => {
                        setForm({
                          ...form,
                          features: nextValue,
                        });
                      }}
                    />
                  </FormField>
                </Box>
              </div>
            </div>
          </Box>
        </div>
        <div className="grid-wrapper">
          <div className="grid-content">
            <Box justify="between" direction="row" margin={{ top: "medium" }}>
              <Box width="48%">
                <GrayButton onClick={() => history.goBack()} label="Вернуться" />
              </Box>
              <Box width="48%">
                <Button
                  onClick={() => handleSubmit()}
                  primary
                  disabled={create.loading}
                  label="Сохранить"
                />
              </Box>
            </Box>
          </div>
        </div>
      </div>
    </Box>
  );
};

const mapToStateProps = (state) => ({
  vendorTypes: state.vendor.type,
  vendorFeatures: state.vendor.features,
  regions: state.vendor.regions,
  create: state.vendor.create,
});

export default connect(mapToStateProps, {
  getVendorTypes,
  getRegions,
  getVendorFeatures,
  createVendor,
  reset,
})(RoomCreate);
