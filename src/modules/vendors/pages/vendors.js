import {useState, useEffect, useCallback} from "react";
import { withRouter, useRouteMatch } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import {
  Table,
  TableBody,
  TableCell,
  TableHeader,
  TableRow as GTableRow,
  Text,
  CheckBox,
  Box,
  TextInput,
  Heading,
  Button,
  Grid,
  Select,
  Spinner
} from "grommet";
import {EditIcon, SortArrowIcon} from "../../../ui/Common/Icon";
import {getVendors, updateVendorStatus} from "../actions";
import VendorItem from "../components/VendorItem";
import { FormSelect } from "ui/Common/Form/Selects";
import Pagination from "ui/Common/Table/Pagination";
import * as types from "../constants";

const TableRow = styled(GTableRow)`
  background: ${(props) =>
    props.checked ? "rgba(47, 128, 237, 0.1)" : "#fff"};

  &:hover {
    background: rgba(47, 128, 237, 0.1);
    cursor: pointer;
  }
`;

const statusOptions = [
  {status: 'active', label: 'Активный'},
  {status: 'inactive', label: 'Не активный'},
];

const pageOptions = [10, 20, 30];

const TableHeadCell = ({label, onSort, children}) => {
  const [sort, setSort] = useState();
  const [asc, setAsc] = useState();
  const [desc, setDesc] = useState();

  const sortHandler = useCallback(() => {
    if (sort === 'asc') {
      setSort('desc');
      setDesc('#2F80ED');
      setAsc();
    } else {
      setSort('asc');
      setAsc('#2F80ED');
      setDesc();
    }

    onSort && onSort(sort);
  }, [sort, setSort]);

  return (
      <Grid
        style={{width: '100%'}}
        rows={['auto', 'auto']}
        columns={['auto', '30px']}
        gap={{row: '24px', column: '5px'}}
        areas={[
          {name: 'label', start: [0, 0], end: [0, 0]},
          {name: 'sort', start: [1, 0], end: [1, 0]},
          {name: 'filters', start: [0, 1], end: [1, 1]},
        ]}
      >
        <Text gridArea="label">{label}</Text>
        {onSort && <Box
          flex
          direction="row"
          justify="center"
          align="center"
          gridArea="sort"
          onClick={sortHandler}
        >
          <SortArrowIcon color={asc}/>
          <SortArrowIcon color={desc} inverted/>
        </Box>}
        <Box gridArea="filters" align="end">
          {children}
        </Box>
      </Grid>
  )
}


const Vendors = ({ history }) => {
  const { vendor, loading } = useSelector((state) => state.vendor);
  const dispatch = useDispatch();
  const [filters, setFilters] = useState({});
  const [statusTitle, setStatusTitle] = useState();

  const {current_page, per_page} = vendor.meta;

  useEffect(() => {
    window.title = 'Агентства | Pingo travel';
  }, []);

  useEffect(() => {
    dispatch(getVendors({...filters, page: current_page, per_page}));
  }, [dispatch, filters, current_page, per_page]);

  let { url } = useRouteMatch();

  return (
    <Box animation={[{ type: "fadeIn", duration: 300 }]}>
      <Box justify="between" direction="row">
        <Box>
          <Heading size="large" level={2} margin={{ top: "xsmall" }}>
            Агентства
          </Heading>
        </Box>
        <Box>
          <Button
            size="large"
            primary
            type="submit"
            label="Зарегистрировать агентство"
            onClick={() => history.push(`${url}/create`)}
          ></Button>
        </Box>
      </Box>

      <Table style={{borderRadius: '10px', overflow: 'hidden'}}>
        <TableHeader>
          <TableRow>
            <TableCell
              style={{
                padding: '24px 0 0 50px',
                verticalAlign: 'top',
                borderRight: 'none'
              }}
              plain
            >
              <Text>№</Text>
            </TableCell>
            <TableCell style={{borderLeft: 'none'}} plain >
              <TableHeadCell label="Наименование">
                <TextInput
                  onChange={e => setFilters({...filters, name: e.target.value})}
                  placeholder="Наименование"
                  value={filters.name}
                />
              </TableHeadCell>
            </TableCell>
            <TableCell align="start">
              <TableHeadCell label="Регион">
                <TextInput
                  onChange={e => setFilters({...filters, location: e.target.value})}
                  placeholder="Регион"
                  value={filters.location}
                />
              </TableHeadCell>
            </TableCell>
            <TableCell align="start">
              <TableHeadCell label="Телефон">
                <TextInput
                  onChange={e => setFilters({...filters, phone: e.target.value})}
                  placeholder="Телефон"
                  value={filters.phone}
                />
              </TableHeadCell>
            </TableCell>
            <TableCell style={{borderRight: 'none'}} plain>
              <TableHeadCell label="Статус">
                <FormSelect
                  options={statusOptions.map(so => so.label)}
                  activeOptionIndex={
                    filters.status
                      ? statusOptions.findIndex(so => so.label === filters.status) + 1
                      : undefined
                  }
                  value={statusTitle}
                  placeholder="Статус"
                  onChange={({option}) => {
                    const so = statusOptions.find(
                      so => so.label === option
                    );
                    setStatusTitle(option);
                    setFilters({ ...filters, status: so && so.status });
                  }}
                />
              </TableHeadCell>
            </TableCell>
            {/*<TableCell style={{ borderLeft: 'none',  }} plain/>*/}
          </TableRow>
        </TableHeader>
        <TableBody>
          {
            vendor.loading ? (
              <GTableRow style={{ background: '#fff' }}>
                <TableCell align="center" colSpan={100}>
                  <Spinner />
                </TableCell>
              </GTableRow>
            ) : !(vendor && vendor.list && vendor.list.length) ? (
              <GTableRow style={{ background: '#fff' }}>
                <TableCell align="center" colSpan={100}>
                  <Text>Отсутствуют записи</Text>
                </TableCell>
              </GTableRow>
            ): vendor?.list
              ?.slice()
              .sort((v1, v2) => v1.id - v2.id)
              .map((item, i) => (
                  <VendorItem
                      key={item.id}
                      vendor={item}
                      onStatusChange={status => dispatch(updateVendorStatus(item.id, status))}
                  />
              ))
          }
        </TableBody>
      </Table>
      <Box flex direction="row" justify="end" align="center" margin={{vertical: '20px'}}>
        <Box flex={{grow: 0, shrink: 1}} direction="row" align="center" width="fit-content">
          <span>Отображать</span>
          <FormSelect
            style={{width: '50px'}}
            margin={{horizontal: '10px'}}
            options={pageOptions}
            activeOptionIndex={pageOptions.indexOf(per_page) + 1}
            value={per_page}
            onChange={({option}) => dispatch({type: types.SET_PER_PAGE, payload: option}  )}
          />
          <span>записей на странице</span>
        </Box>
        <Pagination
          margin={{horizontal: '20px'}}
          numberItems={vendor.meta.total}
          page={current_page}
          step={per_page}
          onChange={(e) => dispatch({type: types.SET_PAGE, payload: e.page})}
        />
      </Box>
    </Box>
  );
};

export default withRouter(Vendors);
