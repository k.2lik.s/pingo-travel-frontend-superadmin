import React, {useEffect, useState} from 'react';
import { TableCell, TableRow as GTableRow, Text} from "grommet";
import { FormSelect } from "ui/Common/Form/Selects";
import styled from "styled-components";

const statusOptions = [
  {status: 'active', label: 'Активный'},
  {status: 'inactive', label: 'Не активный'},
];

const TableRow = styled(GTableRow)`
  background: #fff;
`;

const VendorItem = ({vendor, onStatusChange}) => {
  const [status, setStatus] = useState(statusOptions.find(so => so.status === vendor.status));

  useEffect(() => {
      if (vendor.status !== status.status) {
          console.log(status.status);
          onStatusChange(status.status);
      }
  }, [status])

  return (
    <TableRow>
      <TableCell
        style={{
          padding: '24px 0 24px 50px',
          borderRight: 'none'
        }}
        plain
      >
        <Text>{vendor.id}</Text>
      </TableCell>
      <TableCell style={{borderLeft: 'none'}} plain>
        <Text>{vendor.name}</Text>
      </TableCell>
      <TableCell align="start">
        <Text>{vendor.location}</Text>
      </TableCell>
      <TableCell align="center">
        <Text>{vendor.phone}</Text>
      </TableCell>
      <TableCell style={{borderRight: 'none', textAlign: 'center'}} plain>
        <FormSelect
          options={statusOptions}
          labelKey="label"
          valueKey="status"
          value={status}
          onChange={({ value }) => {
            setStatus(value);
          }}
        />
      </TableCell>
      {/*<TableCell style={{borderLeft: 'none',}} plain>*/}
      {/*  <EditIcon/>*/}
      {/*</TableCell>*/}
    </TableRow>
  )
}

export default VendorItem;