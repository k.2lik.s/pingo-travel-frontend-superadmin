import produce from "immer";
import * as types from "../constants";

const INITAL_STATE = {
  success: false,
  nextStep: false,
  vendorId: null,
  loading: false,
  errors: {},
};

export const create = produce((draft, action) => {
  switch (action.type) {
    case types.CREATE_LOADING:
      draft.loading = true;
      return;
    case types.CREATE_SUCCESS:
      draft.success = true;
      draft.vendorId = action.payload.data.id;
      draft.loading = false;
      return;
    case types.CREATE_FAILURE:
      draft.errors = action.payload.errors;
      draft.loading = false;
      return;

    case types.RESET_CREATE_VENDOR:
      return INITAL_STATE;
  }
}, INITAL_STATE);
