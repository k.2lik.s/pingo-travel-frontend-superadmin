import produce from "immer";
import * as types from "../constants";

const INITAL_STATE = {
  loading: false,
  success: false,
  errors: {},
};

export const update = produce((draft, action) => {
  switch (action.type) {
    case types.SET_VENDOR_STATUS:
      draft.loading = true;
      return;
    case types.SET_VENDOR_STATUS_SUCCESS:
      draft.success = true;
      draft.loading = false;
      return;
    case types.SET_VENDOR_STATUS_FAILURE:
      draft.errors = action.payload.errors;
      draft.loading = false;
      return;
  }
}, INITAL_STATE);
