import { combineReducers } from "redux";
import { vendor } from "./vendor";
import { create } from './create';
import { features } from './features';
import { type } from './type';
import { regions } from "./regions";

export default combineReducers({
    vendor,
    create,
    features,
    type,
    regions,
});
