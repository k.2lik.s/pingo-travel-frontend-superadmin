const prefix = `Vendors`;

export const GET_VENDORS = `${prefix}/GET_VENDORS`;
export const GET_VENDORS_SUCCESS = `${prefix}/GET_VENDORS_SUCCESS`;
export const GET_VENDORS_FAILURE = `${prefix}/GET_VENDORS_FAILURE`;

export const GET_VENDOR_TYPES = `${prefix}/GET_VENDOR_TYPES`;
export const GET_VENDOR_TYPES_SUCCESS = `${prefix}/GET_VENDOR_TYPES_SUCCESS`;
export const GET_VENDOR_TYPES_FAILURE = `${prefix}/GET_VENDOR_TYPES_FAILURE`;

export const GET_VENDOR_FEATURES = `${prefix}/GET_VENDOR_FEATURES`;
export const GET_VENDOR_FEATURES_SUCCESS = `${prefix}/GET_VENDOR_FEATURES_SUCCESS`;
export const GET_VENDOR_FEATURES_FAILURE = `${prefix}/GET_VENDOR_FEATURES_FAILURE`;

export const GET_REGIONS = `${prefix}/GET_REGIONS`;
export const GET_REGIONS_SUCCESS = `${prefix}/GET_REGIONS_SUCCESS`;
export const GET_REGIONS_FAILURE = `${prefix}/GET_REGIONS_FAILURE`;

export const SET_VENDOR_STATUS = `${prefix}/SET_VENDOR_STATUS`;
export const SET_VENDOR_STATUS_SUCCESS = `${prefix}/SET_VENDOR_STATUS_SUCCESS`;
export const SET_VENDOR_STATUS_FAILURE = `${prefix}/SET_VENDOR_STATUS_FAILURE`;

export const SET_PAGE = `${prefix}/SET_PAGE`;
export const SET_PER_PAGE = `${prefix}/SET_PER_PAGE`;

export const CREATE_LOADING = `${prefix}/CREATE_LOADING`;
export const CREATE_SUCCESS = `${prefix}/CREATE_SUCCESS`;
export const CREATE_FAILURE = `${prefix}/CREATE_FAILURE`;

export const RESET_CREATE_VENDOR = `${prefix}/RESET_CREATE_VENDOR`;

