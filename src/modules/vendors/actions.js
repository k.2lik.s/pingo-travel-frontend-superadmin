import * as types from "./constants";
import sendRequest from "settings/sendRequest";
import Nprogress from "nprogress";

export const getVendors = (filters) => (dispatch) => {
  dispatch({ type: types.GET_VENDORS });
  Nprogress.start();
  sendRequest(`/vendors`, {
    method: "GET",
    params: filters,
  })
    .then(({ data }) => {
      dispatch({ type: types.GET_VENDORS_SUCCESS, payload: data });
    })
    .catch((error) => {
      dispatch({ type: types.GET_VENDORS_FAILURE, payload: error });
    })
    .finally(() => Nprogress.done());
};

export const createVendor = (form) => (dispatch) => {
  dispatch({ type: types.CREATE_LOADING });

  sendRequest(`/vendors`, {
    method: "POST",
    data: JSON.stringify(form),
  })
    .then(({ data }) => {
      dispatch({ type: types.CREATE_SUCCESS, payload: data });
    })
    .catch(({ response }) => {
      dispatch({ type: types.CREATE_FAILURE, payload: response.data });
    })
    .finally(() => Nprogress.done());
};

export const getVendorTypes = () => (dispatch) => {
  dispatch({ type: types.GET_VENDOR_TYPES });
  Nprogress.start();
  return sendRequest(`/vendor-types`, {
    method: "GET"
  })
    .then(({data}) => {
      dispatch({ type: types.GET_VENDOR_TYPES_SUCCESS, payload: data });
    })
    .catch((error) => {
      dispatch({ type: types.GET_VENDOR_TYPES_FAILURE, payload: error });
    })
    .finally(() => Nprogress.done());
};

export const getVendorFeatures = () => (dispatch) => {
  dispatch({ type: types.GET_VENDOR_FEATURES });
  Nprogress.start();
  return sendRequest(`/vendor-features`, {
    method: "GET"
  })
    .then(({data}) => {
      dispatch({ type: types.GET_VENDOR_FEATURES_SUCCESS, payload: data });
    })
    .catch((error) => {
      dispatch({ type: types.GET_VENDOR_FEATURES_FAILURE, payload: error });
    })
    .finally(() => Nprogress.done());
};

export const getRegions = () => (dispatch) => {
  dispatch({ type: types.GET_REGIONS });
  Nprogress.start();
  return sendRequest(`/regions`, {
    method: "GET"
  })
      .then(({data}) => {
        dispatch({ type: types.GET_REGIONS_SUCCESS, payload: data });
      })
      .catch((error) => {
        dispatch({ type: types.GET_REGIONS_FAILURE, payload: error });
      })
      .finally(() => Nprogress.done());
};

export const updateVendorStatus = (vendor, status) => (dispatch) => {
  dispatch({ type: types.SET_VENDOR_STATUS });
  Nprogress.start();
  return sendRequest(`/vendors/${vendor}`, {
    method: "patch",
    params: { status }
  })
      .then(({data}) => {
        dispatch({ type: types.SET_VENDOR_STATUS_SUCCESS, payload: data });
      })
      .catch((error) => {
        dispatch({ type: types.SET_VENDOR_STATUS_FAILURE, payload: error });
      })
      .finally(() => Nprogress.done());
};

export const reset = () => dispatch => dispatch({ type: types.RESET_CREATE_VENDOR });