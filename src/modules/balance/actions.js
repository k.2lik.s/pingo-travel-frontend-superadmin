import * as types from "./constants";
import sendRequest from "settings/sendRequest";
import Nprogress from "nprogress";

export const getWallets = (vendorId) => (dispatch) => {
    dispatch({ type: types.GET_WALLETS });
    Nprogress.start();
    return sendRequest(`/vendors/${vendorId}/wallets`, {
        method: "GET",
    })
        .then(({ data }) => {
            dispatch({ type: types.GET_WALLETS_SUCCESS, payload: data });
            Nprogress.done();
        })
        .catch((error) => {
            dispatch({ type: types.GET_WALLETS_FAILURE, payload: error });
        });
};

export const getTransaction = (vendorId, data) => (dispatch) => {
  dispatch({ type: types.GET_TRANSACTIONS });
  Nprogress.start();
  return sendRequest(`/vendors/${vendorId}/transactions?${data.wallet ? 'wallet=' : ''}${data.wallet}&per_page=${data.perPage}`, {
      method: "GET",
  })
      .then(({data}) => {
          dispatch({ type: types.GET_TRANSACTIONS_SUCCESS, payload: data });
          Nprogress.done();
      })
      .catch((error) => {
          dispatch({ type: types.GET_TRANSACTIONS_FAILURE, payload: error });
      });
};

export const getVendorAccounts = (vendorId) => (dispatch) => {
    dispatch({ type: types.GET_VENDOR_ACCOUNTS });
    Nprogress.start();
    return sendRequest(`/vendor-accounts/${vendorId}`, {
        method: "GET"
    })
        .then(({data}) => {
            dispatch({ type: types.GET_VENDOR_ACCOUNTS_SUCCESS, payload: data });
            Nprogress.done();
        })
        .catch((error) => {
            dispatch({ type: types.GET_VENDOR_ACCOUNTS_FAILURE, payload: error });
        });
};

export const payout = (vendorId, form) => (dispatch) => {
    dispatch({ type: types.PAYOUT_LOADING });

    sendRequest(`/vendors/${vendorId}/payout`, {
        method: "POST",
        data: JSON.stringify(form),
    })
        .then(({ data }) => {
            dispatch({ type: types.PAYOUT_SUCCESS, payload: data });
        })
        .catch(({ response }) => {
            dispatch({ type: types.PAYOUT_FAILURE, payload: response.data });
        });
};