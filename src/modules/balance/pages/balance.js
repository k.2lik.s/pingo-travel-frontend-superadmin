import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { format } from "date-fns";
import {
    Heading,
    Box,
    Text,
    Button,
    TextInput,
} from "grommet";
import styled from "styled-components";
import { useTranslation } from "react-i18next";
import { getWallets, getTransaction, getVendorAccounts, payout } from "modules/balance/actions";
import {
    PassiveCheckboxIcon,
    MainBalanceIcon,
    ActiveCheckboxIcon,
    ReserveBalanceIcon,
    SmallMainBalanceIcon,
    SmallReserveBalanceIcon,
    MyBalanceNotCheckedIcon,
    MyBalanceCheckedIcon,
    PrintIcon,
    DownloadInvoiceIcon,
    SendEmailIcon,
} from "ui/Common/Icon";
import Modal from "ui/Common/Modal";
import useModal from "ui/Common/Modal/useModal";

const Balance = () => {
  const { t } = useTranslation('common');
  const [checkMain, setCheckMain] = useState(false);
  const [checkReserve, setCheckReserve] = useState(false);

  const [disable, setDisable] = useState(false);

  const [perPage, setPerPage] = useState(10);

  const [wallet, setWallet] = useState('');

  const [legalAccount, setLegalAccount] = useState(false);
  const [cardAccount, setCardAccount] = useState(false);
  const [payAmount, setPayAmount] = useState(10000);
  const [iin, setIin] = useState(null);
  const [accountNumber, setAccountNumber] = useState(null);

  const { vendor } = useSelector((state) => state.common);
  const { balance, transaction, vendorAccount } = useSelector((state) => state.balance);
  const dispatch = useDispatch();
  const [isShowing1, toggle1] = useModal();
  const currencyFormatter = Intl.NumberFormat('ru-RU');


  useEffect(() => {
    if (!vendor.loading) {
        dispatch(getWallets(vendor.currentVendor.id));
        dispatch(getVendorAccounts(vendor.currentVendor.id));
    }
  }, [vendor]);

  useEffect(() => {
      if (!vendor.loading) {
          dispatch(getTransaction(vendor.currentVendor.id, {
              wallet: wallet,
              perPage: perPage,
          }));
      }
  }, [vendor, perPage, wallet]);

    const handlePayoutError = () => {
        const checkAmount = 10000;
        let errorText = "";
        if (amountOfMainBalance <= checkAmount) {
            errorText = "Если доступная сумма не превышает 10 000, то для выплаты доступна только вся сумма"
        } else if (amountOfMainBalance > checkAmount && payAmount < checkAmount) {
            errorText = "Минимальная сумма выплаты равна 10 000"
        } else if (payAmount > amountOfMainBalance) {
            errorText = "Указанная сумма выплаты превышает доступную сумму"
        }
        return errorText
    };

    const amountOfMainBalance = balance.list[0]?.balance;

    const transactionType = (transactionType, metaType) => {
    if (metaType === 'order_refund') {
        return 'Возврат'
    } else if (metaType === 'transfer' && transactionType === 'withdraw') {
        return 'Перевод'
    }

    return transactionType === 'deposit' ? 'Пополнение' : 'Снятие';

  };

    const handleSubmitLegal = () => {
        payout(vendor.currentVendor.id, {
            account: 'legal',
            amount: payAmount
        });
        toggle1();
        setLegalAccount(false);
        setCardAccount(false);
    };

    const handleSubmitCard = () => {
        payout(vendor.currentVendor.id, {
            account: 'card',
            bin: iin,
            account_number: accountNumber,
            amount: payAmount,
        });
        toggle1();
        setLegalAccount(false);
        setCardAccount(false);
    };

  return (
    <>
        <Box animation={[{ type: "fadeIn", duration: 300 }]}>

            <Heading size="large" level={2} margin={{ top: "xsmall" }}>
                Мой Баланс
            </Heading>
            <Box
                direction="row"
                align="top"
            >
                <HeaderBox
                    background="#fff"
                    justify="between"
                    direction="row"
                    align="center"
                    pad="32px"
                    width="24%"
                    margin={{right: '32px'}}
                    onClick={() => {
                        setCheckMain(!checkMain);
                        setCheckReserve(false);
                        if (wallet === '' || wallet === 'temp') {
                            setWallet('default')
                        } else {
                            setWallet('')
                        }
                    }}
                    type="main"
                >
                    <Box
                        direction="row"
                        align="center"
                    >
                        <MainBalanceIcon height="48" width="48" />
                        <Box margin={{left:'16px'}}>
                            <HeaderBoxTitle>
                                Основной баланс
                            </HeaderBoxTitle>
                            <HeaderBoxValue>
                                {`${currencyFormatter.format(balance.list[0]?.balance)} KZT`}
                            </HeaderBoxValue>
                        </Box>
                    </Box>
                    {
                        checkMain
                            ? <ActiveCheckboxIcon height="24" width="24"/>
                            : <PassiveCheckboxIcon height="24" width="24"/>
                    }
                </HeaderBox>
                <HeaderBox
                    background="#fff"
                    justify="between"
                    direction="row"
                    align="center"
                    pad="32px"
                    width="24%"
                    margin={{right: '32px'}}
                    onClick={() => {
                        setCheckReserve(!checkReserve);
                        setCheckMain(false);
                        if (wallet === '' || wallet === 'default') {
                            setWallet('temp')
                        } else {
                            setWallet('')
                        }
                    }}
                    type="reserve"
                >
                    <Box
                        direction="row"
                        align="center"
                    >
                        <ReserveBalanceIcon height="51" width="60" />
                        <Box margin={{left:'16px'}}>
                            <HeaderBoxTitle>
                                Резервный баланс
                            </HeaderBoxTitle>
                            <HeaderBoxValue>
                                {`${currencyFormatter.format(balance?.list[1]?.balance)} KZT`}
                            </HeaderBoxValue>
                        </Box>
                    </Box>
                    {
                        checkReserve
                            ? <ActiveCheckboxIcon height="24" width="24"/>
                            : <PassiveCheckboxIcon height="24" width="24"/>
                    }
                </HeaderBox>
                <HeaderBox
                    background="#fff"
                    pad="32px"
                    width="24%"
                    justify="center"
                    align="center"
                    type="payment"
                    onClick={toggle1}
                >
                    <PaymentRequest>
                        {t('common:payment_request')}
                    </PaymentRequest>
                </HeaderBox>
            </Box>
            <TableTitle>Транзакции по балансу</TableTitle>
            <Box direction="row">
                <TableBox
                    background="#fff"
                >
                    <CustomBox animation="slideDown">
                        {transaction.list.map((item) => (
                            <Box direction="row" align="center" animation="slideDown">
                                <Box
                                    width="27%"
                                    pad={{bottom:'16px'}}
                                    direction="row"
                                    align="center"
                                >
                                    {item.wallet?.slug === 'default'
                                        ? <SmallMainBalanceIcon width="48" height="48"/>
                                        : <SmallReserveBalanceIcon width="48" height="48"/>
                                    }
                                    <div>
                                        <DateText>
                                            {item?.created_at
                                                ? format(new Date(item?.created_at), 'dd MMM')
                                                : null
                                            }
                                        </DateText>
                                        <TimeText>
                                            {item?.created_at
                                                ? format(new Date(item?.created_at), 'p')
                                                : null
                                            }
                                        </TimeText>
                                    </div>
                                </Box>
                                <Box width="27%" pad={{bottom:'16px'}}>
                                    <StatusTypeText status={transactionType(item.type, item.meta.type)}>
                                        {transactionType(item.type, item.meta.type)}
                                    </StatusTypeText>
                                </Box>
                                <Box width="27%" pad={{bottom:'16px'}}>
                                    <AmountText status={transactionType(item.type, item.meta.type)}>
                                        {item?.type === 'deposit'
                                            ? `+${currencyFormatter.format(item.amount)} KZT`
                                            : `${currencyFormatter.format(item.amount)} KZT`
                                        }
                                    </AmountText>
                                </Box>
                                <Box width="19%" pad={{bottom:'16px'}}>
                                    <MetaTypeText>
                                        {item?.meta?.title}
                                    </MetaTypeText>
                                </Box>
                            </Box>
                        ))}
                    </CustomBox>
                    <LoadMoreButton
                        onClick={(e) => {
                            e.preventDefault();
                            setPerPage(perPage + 10);
                            setDisable(true);
                            setTimeout(() => setDisable(false), 1500);
                        }}
                        disabled={disable}
                    >
                        Загрузить еще
                    </LoadMoreButton>
                </TableBox>
                <Box pad={{left: '40px'}}>
                    <Box pad={{bottom: "25px"}} direction="row" align="center">
                        <PrintIcon width="32px" height="32px" />
                        <AdditionalOptionsText>Распечатать</AdditionalOptionsText>
                    </Box>
                    <Box pad={{bottom: "25px"}} direction="row" align="center">
                        <DownloadInvoiceIcon width="32px" height="32px" />
                        <AdditionalOptionsText>Скачать выписку</AdditionalOptionsText>
                    </Box>
                    <Box direction="row" align="center">
                        <SendEmailIcon width="32px" height="32px" />
                        <AdditionalOptionsText>Отправить на E-mail</AdditionalOptionsText>
                    </Box>
                </Box>
            </Box>
        </Box>
        <Modal
            isShowing={isShowing1}
            hide={toggle1}
            background="#000"
            opacity=".5"
            width={'750px'}
            margin="11.75rem auto"
        >
            <Box
                pad="none"
            >
                <ModalTitleText>Заявка на выплату</ModalTitleText>

                <ModalBlocks first>
                    <ModalCardsBlock
                        onClick={() => {
                            setLegalAccount(!legalAccount)
                            setCardAccount(false)
                        }}
                    >
                        {legalAccount
                            ? <MyBalanceCheckedIcon width="18" height="18" />
                            : <MyBalanceNotCheckedIcon width="18" height="18" />
                        }
                        <ModalCardText>
                            На юридический счёт
                        </ModalCardText>
                    </ModalCardsBlock>
                    {legalAccount
                        ? <>
                            <ModalBlock>
                                <ModalFieldTitle>Название агентства</ModalFieldTitle>
                                <TextInput
                                    value="TVOYA KOMANDA"
                                    disabled
                                />
                            </ModalBlock>
                            <ModalBlock>
                                <ModalFieldTitle>Юридический адрес</ModalFieldTitle>
                                <TextInput
                                    value={vendorAccount?.list?.kbe ? vendorAccount?.list?.kbe : ""}
                                    disabled
                                />
                            </ModalBlock>
                            <ModalBlock>
                                <ModalFieldTitle>БИН / ИИН</ModalFieldTitle>
                                <TextInput
                                    value={vendorAccount?.list?.iin ? vendorAccount?.list?.iin : ""}
                                    disabled
                                />
                            </ModalBlock>
                            <ModalBlock>
                                <ModalFieldTitle>№ счета</ModalFieldTitle>
                                <TextInput
                                    value={vendorAccount?.list?.account_number ? vendorAccount?.list?.account_number : ""}
                                    disabled
                                />
                            </ModalBlock>
                            <ModalBlock>
                                <ModalFieldTitle>БИК</ModalFieldTitle>
                                <TextInput
                                    value={vendorAccount?.list?.bin ? vendorAccount?.list?.bin : ""}
                                    disabled
                                />
                            </ModalBlock>
                            <ModalBlock>
                                <Text size="medium">

                                </Text>
                                <ModalFieldTitle>
                                    <b>Укажите сумму </b>
                                    <Text color="red">*</Text>
                                </ModalFieldTitle>
                                <Box direction="row" align="center" round="xsmall" border>
                                    <TextInput
                                        plain
                                        type="number"
                                        step="1000"
                                        max={amountOfMainBalance}
                                        value={amountOfMainBalance <= 10000 ? amountOfMainBalance : payAmount}
                                        disabled={amountOfMainBalance < 10000}
                                        onChange={(e) => setPayAmount(e.target.value)}
                                    />
                                    <Box
                                        background="#F2F2F2"
                                        round={{ corner: "right", size: "xsmall" }}
                                        border={{ side: "left" }}
                                        pad={{ vertical: "small", horizontal: "medium" }}
                                    >
                                        <Text size="small">KZT</Text>
                                    </Box>
                                </Box>
                            </ModalBlock>
                            {payAmount === ""
                                ? null
                                : <ModalPayoutError>
                                    {handlePayoutError()}
                                  </ModalPayoutError>
                            }
                            <ModalBlock isButton>
                                <ModalSendButton
                                    fill
                                    onClick={() => handleSubmitLegal()}
                                    disabled={payAmount >= amountOfMainBalance || 10000 > payAmount ? true : false}
                                    primary
                                    label="Отправить"
                                />
                            </ModalBlock>
                        </>
                        : null
                    }
                </ModalBlocks>
                <ModalBlocks>
                    <ModalCardsBlock
                        onClick={() => {
                            setCardAccount(!cardAccount)
                            setLegalAccount(false)
                        }}
                    >
                        {cardAccount
                            ? <MyBalanceCheckedIcon width="18" height="18" />
                            : <MyBalanceNotCheckedIcon width="18" height="18" />
                        }
                        <ModalCardText>
                            На карточный счёт
                        </ModalCardText>
                    </ModalCardsBlock>
                    {cardAccount
                        ? <>
                            <ModalBlock>
                                <Text size="medium">

                                </Text>
                                <ModalFieldTitle>
                                    <b>ИИН/БИН </b>
                                    <Text color="red">*</Text>
                                </ModalFieldTitle>
                                <TextInput
                                    type="number"
                                    placeholder="Напишите ИИН/БИН"
                                    value={iin}
                                    onChange={(e) => setIin(e.target.value)}
                                />
                            </ModalBlock>
                            <ModalBlock>
                                <Text size="medium">

                                </Text>
                                <ModalFieldTitle>
                                    <b>№ счета </b>
                                    <Text color="red">*</Text>
                                </ModalFieldTitle>
                                <TextInput
                                    type="number"
                                    placeholder="Напишите реквизиты"
                                    value={accountNumber}
                                    onChange={(e) => setAccountNumber(e.target.value)}
                                />
                            </ModalBlock>
                            <ModalBlock>
                                <Text size="medium">

                                </Text>
                                <ModalFieldTitle>
                                    <b>Укажите сумму </b>
                                    <Text color="red">*</Text>
                                </ModalFieldTitle>
                                <Box direction="row" align="center" round="xsmall" border>
                                    <TextInput
                                        plain
                                        type="number"
                                        step="1000"
                                        max={amountOfMainBalance}
                                        value={amountOfMainBalance <= 10000 ? amountOfMainBalance : payAmount}
                                        disabled={amountOfMainBalance < 10000}
                                        onChange={(e) => setPayAmount(e.target.value)}
                                    />
                                    <Box
                                        background="#F2F2F2"
                                        round={{ corner: "right", size: "xsmall" }}
                                        border={{ side: "left" }}
                                        pad={{ vertical: "small", horizontal: "medium" }}
                                    >
                                        <Text size="small">KZT</Text>
                                    </Box>
                                </Box>
                            </ModalBlock>
                            {payAmount === ""
                                ? null
                                : <ModalPayoutError>
                                    {handlePayoutError()}
                                  </ModalPayoutError>
                            }
                            <ModalBlock isButton>
                                <ModalSendButton
                                    fill
                                    onClick={() => handleSubmitCard()}
                                    disabled={payAmount >= amountOfMainBalance || 10000 > payAmount ? true : false}
                                    primary
                                    label="Отправить"
                                />
                            </ModalBlock>
                        </>
                        : null
                    }
                </ModalBlocks>

            </Box>
        </Modal>
    </>
  );
};

const AdditionalOptionsText = styled(Text)`
  padding-left: 16px;
`;

const ModalBlocks = styled.div`
  margin: 32px 32px 0 32px;
  padding-bottom: 32px;
  border-bottom: ${({first}) => first ? "1px solid #E0E0E0" : "none"};
`;

const ModalBlock = styled.div`
  margin-top: ${({isButton}) => isButton ? '32px' : '24px' };
`;

const ModalPayoutError = styled.div`
  margin-top: 8px;
  margin-left: 12px;
  color: #EB5757;
`;

const ModalSendButton = styled(Button)`
  height: 56px;
`;

const ModalFieldTitle = styled(Text)`
  font-weight: 600;
  font-size: 16px;
  line-height: 24px;
  padding-bottom: 8px;
  margin: 0;
  color: #333333; 
`;

const ModalCardsBlock = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
`;

const ModalCardText = styled(Text)`
  font-weight: normal;
  font-size: 18px;
  line-height: 24px;
  color: #333333;
  margin: 0;
  padding-left: 10px;
  -moz-user-select: none;
  -khtml-user-select: none;
  -webkit-user-select: none;
  user-select: none;
`;

const ModalTitleText = styled(Text)`
  font-weight: bold;
  font-size: 24px;
  line-height: 32px;
  color: #333333;
  margin: 0 32px 0 32px;
  padding-bottom: 24px;
  border-bottom: 1px solid #E0E0E0;
`

const HeaderBox = styled(Box)`
  border-radius: 16px;
  height: 136px;
  background: ${({type}) => {
    if (type === 'main') {
      return "linear-gradient(96.34deg, #65A7FF 0%, #2F80ED 100%), #2F80ED"
    } else if (type === 'reserve') {
      return "linear-gradient(96.34deg, #FFAF67 0%, #F2994A 100%), #F2994A"

    } else if (type === 'payment') {
        return "#219653"
    }  else {
        return null
    }
  }};
  box-shadow: 0 4px 6px rgba(18, 132, 247, 0.04);
`;

const CustomBox = styled(Box)`
  padding: 32px 32px 16px 32px;
`;

const LoadMoreButton = styled(Button)`
  font-weight: 600;
  font-size: 16px;
  line-height: 24px;
  text-align: center;
  color: #2F80ED;
  padding: 32px;
  border-top: 1px solid #E0E0E0;
`;

const TableBox = styled(Box)`
  width: 752px;
  box-shadow: 0 4px 6px rgba(62, 73, 84, 0.04);
  border-radius: 12px;
`;

const DateText = styled.p`
  font-weight: 600;
  font-size: 16px;
  line-height: 24px;
  margin: 0;
  color: #333333;
  padding-left: 16px;
`;

const TimeText = styled(Text)`
  font-weight: normal;
  font-size: 14px;
  line-height: 24px;
  margin: 0;
  color: #828282;
  padding-left: 16px;
`;

const AmountText = styled(Text)`
  font-weight: normal;
  font-size: 16px;
  line-height: 24px;
  margin: 0;
  color: ${(props) => {
    if (props.status === 'Пополнение') {
      return "#219653"
    } else if (props.status === 'Возврат') {
      return "#EB5757"
    } else {
      return "#333333"
    }
  }}
`;

const HeaderBoxTitle = styled(Text)`
  font-weight: normal;
  font-size: 14px;
  line-height: 16px;
  margin: 0;
  color: #FFFFFF;
  padding-bottom: 8px;
`;

const HeaderBoxValue = styled(Text)`
  font-weight: bold;
  font-size: 24px;
  line-height: 32px;
  margin: 0;
  color: #FFFFFF;
`;

const PaymentRequest = styled(Text)`
  font-weight: bold;
  font-size: 24px;
  line-height: 32px;
  margin: 0;
  color: #FFFFFF;
`;

const TableTitle = styled(Text)`
  font-weight: 800;
  font-size: 24px;
  line-height: 32px;
  color: #333333;
  margin: 0;
  padding-bottom: 32px;
  padding-top: 32px;
`;

const StatusTypeText = styled(Text)`
  font-weight: normal;
  font-size: 16px;
  line-height: 24px;
  margin: 0;
  color: ${(props) => {
      if (props.status === 'Пополнение') {
          return "#219653"
      } else if (props.status === 'Возврат') {
          return "#EB5757"
      } else {
          return "#333333"
      }
  }}
`;

const MetaTypeText = styled(Text)`
  font-weight: normal;
  font-size: 16px;
  line-height: 24px;
  text-align: right;
  color: #828282;
  margin: 0;
`;

export default Balance;
