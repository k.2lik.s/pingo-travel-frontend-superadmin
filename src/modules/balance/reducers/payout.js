import produce from "immer";
import * as types from "../constants";

const INITAL_STATE = {
    success: false,
    nextStep: false,
    loading: false,
    errors: {},
};

export const payout = produce((draft, action) => {
    switch (action.type) {
        case types.PAYOUT_LOADING:
            draft.loading = true;
            return;
        case types.PAYOUT_SUCCESS:
            draft.success = true;
            draft.nextStep = true;
            draft.loading = false;
            return;
        case types.PAYOUT_FAILURE:
            draft.errors = action.payload.errors;
            draft.loading = false;
            return;
    }
}, INITAL_STATE);
