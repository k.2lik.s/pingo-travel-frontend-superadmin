import { combineReducers } from "redux";
import { balance } from "./balance";
import { transaction } from "./transactions";
import { vendorAccount } from "./vendorAccount";

export default combineReducers({
    balance,
    transaction,
    vendorAccount,
});
