import * as types from "./constants";
import sendRequest from "settings/sendRequest";
import Nprogress from "nprogress";

export const getVendorBookings = (vendorId) => (dispatch) => {
    dispatch({ type: types.GET_VENDOR_BOOKINGS });
    Nprogress.start();
    return sendRequest(`/vendors/${vendorId}/room-numbers?status=active&condition=free`, {
        method: "GET"
    })
        .then(({data}) => {
            dispatch({ type: types.GET_VENDOR_BOOKINGS_SUCCESS, payload: data });
            Nprogress.done();
        })
        .catch((error) => {
            dispatch({ type: types.GET_VENDOR_BOOKINGS_FAILURE, payload: error });
        });
};