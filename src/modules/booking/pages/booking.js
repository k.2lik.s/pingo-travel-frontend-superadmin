import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { withRouter } from "react-router-dom";
import styled from "styled-components";
import {
    Heading,
    Box,
    TableHeader,
    TableCell,
    Text,
    TextInput,
    TableBody,
    Table,
    TableRow, CheckBox, Button
} from "grommet";
import { getVendorBookings } from "../action";
import { FormSelect } from "ui/Common/Form/Selects";

const Bookings = ({ history }) => {
    const { vendor } = useSelector((state) => state.common);
    const dispatch = useDispatch();

    useEffect(() => {
        if (!vendor.loading) {
            dispatch(getVendorBookings(vendor.currentVendor.id));
        }
    }, [vendor]);

    const { bookings } = useSelector((state) => state.booking);
    console.log(bookings)
    const onCheck = (id) => {
        return null;
    };

    return (
        <Box animation={[{ type: "fadeIn", duration: 300 }]}>
            <Heading size="large" level={2} margin={{ top: "xsmall" }}>
                Бронирование
            </Heading>
            <Box pad={{ bottom: "32px" }} justify="between" direction="row">
                <Box width="344px" pad={{ top: "medium", left: "none" }} align="center">
                    <FormSelect
                        placeholder="Статус оплаты"
                        options={[1, 2, 3]}
                        value={3}
                        // onChange={({value: nextValue}) => {
                        //     setStatusPaymentTitle(nextValue);
                        //     setStatusPaymentValue(StatusPayment.filter(({title}) => title === nextValue));
                        // }}
                        clear={{
                            label: 'Очистить'
                        }}
                    />
                </Box>
                <Box width="344px" pad={{ top: "medium", left: "none" }} align="center">
                    <FormSelect
                        placeholder="Статус оплаты"
                        options={[1, 2, 3]}
                        value={2}
                        // onChange={({value: nextValue}) => {
                        //     setStatusPaymentTitle(nextValue);
                        //     setStatusPaymentValue(StatusPayment.filter(({title}) => title === nextValue));
                        // }}
                        clear={{
                            label: 'Очистить'
                        }}
                    />
                </Box>
                <Box width="344px" pad={{ top: "medium", left: "none" }} align="center">
                    <FormSelect
                        placeholder="Статус оплаты"
                        options={[1, 2, 3]}
                        value={1}
                        // onChange={({value: nextValue}) => {
                        //     setStatusPaymentTitle(nextValue);
                        //     setStatusPaymentValue(StatusPayment.filter(({title}) => title === nextValue));
                        // }}
                        clear={{
                            label: 'Очистить'
                        }}
                    />
                </Box>
                <Box width="344px" pad={{ top: "medium", left: "none" }} align="center">
                    <Button
                        // size="large"
                        fill
                        primary
                        type="submit"
                        label="Поиск"
                        // onClick={() => history.push(`${url}/create/information`)}
                    ></Button>
                </Box>
            </Box>
            <Table>
                <TableHeader>
                    <TableCell align="start">
                        <Box justify="between" direction="row">
                            <Box pad={{ right: "small" }}>
                                <CheckBox
                                    label="№"
                                    // checked={data.every((item) => item.checked === true)}
                                    // indeterminate={data.every((item) => item.checked !== true)}
                                    // onChange={onCheckAll}
                                />
                            </Box>
                            <Text>комнат</Text>
                        </Box>
                        <Box width="200px" pad={{ top: "medium", left: "medium" }} align="center">
                            <TextInput
                                placeholder="№ комнат"
                                // onChange={(e) => setOrderNumber(e.target.value)}
                            />
                        </Box>
                    </TableCell>
                    <TableCell align="start">
                        <Text>ТИП КОМНАТЫ</Text>
                        <Box width="200px" pad={{ top: "medium", left: "none" }} align="center">
                            <TextInput
                                placeholder="Тип комнаты"
                                // onChange={(e) => setOrderNumber(e.target.value)}
                            />
                        </Box>
                    </TableCell>
                    <TableCell align="start">
                        <Text>ЭТАЖ</Text>
                        <Box width="240px" pad={{ top: "medium", left: "none" }} align="center">
                            <TextInput
                                type="number"
                                placeholder="Этаж"
                                // onChange={(e) => setOrderNumber(e.target.value)}
                            />
                        </Box>
                    </TableCell>
                    <TableCell align="start">
                        <Text>ВЗРОСЛЫЕ</Text>
                        <Box width="inherit" pad={{ top: "medium", left: "none" }} align="center">
                            <TextInput
                                type="number"
                                placeholder="Взрослые"
                                // onChange={(e) => setTotalPaid(e.target.value)}
                            />
                        </Box>
                    </TableCell>
                    <TableCell align="start">
                        <Text>ДЕТИ</Text>
                        <Box width="inherit" pad={{ top: "medium", left: "none" }} align="center">
                            <TextInput
                                type="number"
                                placeholder="Дети"
                                // onChange={(e) => setTotalPaid(e.target.value)}
                            />
                        </Box>
                    </TableCell>
                    <TableCell align="start">
                        <Text>ЦЕНА</Text>
                        <Box width="inherit" pad={{ top: "medium", left: "none" }} align="center">
                            <TextInput
                                type="number"
                                placeholder="Цена"
                                // onChange={(e) => setTotalPaid(e.target.value)}
                            />
                        </Box>
                    </TableCell>
                    <TableCell align="start">
                        <Text>СТАТУС</Text>
                        <Box width="inherit" pad={{ top: "medium", left: "none" }} align="center">
                            <TextInput
                                plain
                            />
                        </Box>
                    </TableCell>
                </TableHeader>
                <TableBody>
                    {bookings.list.map((booking) => (
                        <TableRow
                            checked={true}
                            onClick={(e) => onCheck(1)}
                            // key={item.id}
                            // onClick={() => history.push(`${url}/detail/${item.id}`, {
                            //     id: item.id,
                            // })}
                        >
                            <TableCell>
                                <Box direction="row" justify="between">
                                    <Box direction="row" justify="between" width="xxsmall">
                                        <CheckBox />
                                    </Box>
                                    <Box width="small" pad={{ left: "medium" }}>
                                        <Text>
                                            {/*{item.name}*/}
                                            {booking.room_number}
                                        </Text>
                                    </Box>
                                </Box>
                            </TableCell>
                            <TableCell>
                                <Text>
                                    {/*{`#${item?.order_number}`}*/}
                                    {booking.room.name}
                                </Text>
                            </TableCell>
                            <TableCell align="center">
                                <Text>
                                    {/*{item.created_at*/}
                                    {/*    ? format(new Date(item.created_at), 'dd.MM.yyyy')*/}
                                    {/*    : null*/}
                                    {/*}*/}
                                    {booking.floor}
                                </Text>
                            </TableCell>
                            <TableCell align="center">
                                <Text>
                                    {/*{`*/}
                                    {/*    ${format(new Date(item?.bookings[0]?.started_at), 'dd.MM.yyyy')} */}
                                    {/*    - */}
                                    {/*    ${format(new Date(item?.bookings[0]?.ended_at), 'dd.MM.yyyy')}*/}
                                    {/*    `}*/}
                                    {booking.room.adults}
                                </Text>
                            </TableCell>
                            <TableCell align="center">
                                <Text>
                                    {/*{`${currencyFormatter.format(item?.total_paid)} KZT`}*/}
                                </Text>
                            </TableCell>
                            <TableCell align="center">
                                <Text>
                                    {/*{orderStatus(item?.status)}*/}
                                    {booking.room.children}
                                </Text>
                            </TableCell>
                            <TableCell align="center">
                                <StatusText>
                                    {/*{paymentState(item?.active_purchase?.status)}*/}
                                    {booking.status}
                                </StatusText>
                            </TableCell>
                            {/*{refundButton(item?.active_purchase?.status, item?.active_purchase?.operation_type, item?.bookings[0]?.ended_at)*/}
                            {/*    ? <TableCell align="center">*/}
                            {/*        <Refund*/}
                            {/*            name="Возврат"*/}
                            {/*            modal={toggle1}*/}
                            {/*            id={item.id}*/}
                            {/*        />*/}
                            {/*    </TableCell>*/}
                            {/*    : null*/}
                            {/*}*/}

                        </TableRow>
                    ))}
                </TableBody>
            </Table>
            <Box pad={{ top: "150px" }} direction="row" align="center" justify="end">
                <Text>Выбран <span style={{fontWeight: 'bold'}}>1</span> номер за <span style={{fontWeight: 'bold'}}>56 154 KZT</span></Text>
                <Box width="344px" height="48px" pad={{ left: "32px" }} align="center">
                    <Button
                        // size="large"
                        fill
                        primary
                        type="submit"
                        label="Продолжить"
                        // onClick={() => history.push(`${url}/create/information`)}
                    ></Button>
                </Box>
            </Box>
        </Box>
    );
};

const StatusText = styled(Text)`
  color: #219653;
`;

export default withRouter(Bookings);

