import { Box, Heading } from "grommet";
import { useHistory } from "react-router-dom";
import { CheckIcon } from "ui/Common/Icon";
import styled from "styled-components";

const StepBox = styled(Box)`
  background-color: ${(props) =>
    props.isActive ? "rgba(47, 128, 237, 0.1)" : "rgba(255, 255, 255, 0.1)"};
  cursor: ${(props) => (props.isDisable ? "not-allowed" : "pointer")};
  font-weight: 600;
  font-size: 18px;
  line-height: 32px;
  margin-bottom: 4px;
  align-items: flex-start;
  color: #828282;
  border-left: 8px solid #bdbdbd;
  ${(props) =>
    props.isActive && "color:#2F80ED; border-left: 8px solid #2F80ED"};
  ${(props) =>
    props.isFinished && "color:#219653; border-left: 8px solid #219653"};
`;

const StyledCheckIcon = styled(CheckIcon)`
  position: absolute;
  right: 20px;
  display: ${(props) => (props.isFinished ? "block" : "none")};
`;

const StickyBox = styled(Box)`
  position: sticky;
  top: 20px;
`;

const Navigation = ({ stepId, infoSuccess }) => {
  const history = useHistory();

  return (
    <StickyBox background="#fff" height={{ max: "616px" }}>
      <Box border={{ side: "bottom", color: "#E0E0E0" }} pad="medium">
        <Heading margin="0" level={2} size="small">
          Новая команата
        </Heading>
      </Box>
      <Box pad={{ vertical: "small", right: "0" }}>
        <StepBox
          onClick={() => history.push(`/admin/managers/create/information`)}
          isActive={stepId === "information"}
          isFinished={infoSuccess}
          pad={{ vertical: "medium", left: "medium" }}
        >
          Информация
          <StyledCheckIcon isFinished={infoSuccess} width={32} height={32} />
        </StepBox>
        <StepBox
          isActive={stepId === "vendor"}
          isDisable={!infoSuccess}
          onClick={() =>
            infoSuccess && history.push(`/admin/managers/create/vendor`)
          }
          pad={{ vertical: "medium", left: "medium" }}
        >
          Агентство
          <StyledCheckIcon width={32} height={32} />
        </StepBox>
      </Box>
    </StickyBox>
  );
};

export default Navigation;
