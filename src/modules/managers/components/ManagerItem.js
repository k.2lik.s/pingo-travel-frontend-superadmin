import React, {useState} from 'react';
import { TableCell, TableRow as GTableRow, Text} from "grommet";
import { FormSelect } from "ui/Common/Form/Selects";
import styled from "styled-components";


const TableRow = styled(GTableRow)`
  background: #fff;
`;

const ManagerItem = ({vendor}) => {
  return (
    <TableRow>
      <TableCell
        style={{
          padding: '24px 0 24px 50px',
          borderRight: 'none'
        }}
        plain
      >
        <Text>{vendor.id}</Text>
      </TableCell>
      <TableCell style={{borderLeft: 'none'}} plain>
        <Text>{vendor.name}</Text>
      </TableCell>
      <TableCell align="start">
        <Text>{vendor.surname}</Text>
      </TableCell>
      <TableCell align="center">
        <Text>{vendor.email}</Text>
      </TableCell>
      {/*<TableCell style={{borderLeft: 'none',}} plain>*/}
      {/*  <EditIcon/>*/}
      {/*</TableCell>*/}
    </TableRow>
  )
}

export default ManagerItem;