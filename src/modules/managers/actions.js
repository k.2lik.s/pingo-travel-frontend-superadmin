import * as types from "./constants";
import sendRequest from "settings/sendRequest";
import Nprogress from "nprogress";

export const getVendors = () => (dispatch) => {
  dispatch({ type: types.GET_VENDORS });
  Nprogress.start();
  sendRequest(`/vendors`, {
    method: "GET",
  })
    .then(({ data }) => {
      dispatch({ type: types.GET_VENDORS_SUCCESS, payload: data });
    })
    .catch((error) => {
      dispatch({ type: types.GET_VENDORS_FAILURE, payload: error });
    })
    .finally(() => Nprogress.done());
};

export const getManagers = (vendor, filters) => (dispatch) => {
  dispatch({ type: types.GET_MANAGERS });
  Nprogress.start();
  return sendRequest(`/vendors/${vendor}/managers`, {
    method: "GET",
    params: filters,
  })
    .then(({data}) => {
      dispatch({ type: types.GET_MANAGERS_SUCCESS, payload: data });
    })
    .catch((error) => {
      dispatch({ type: types.GET_MANAGERS_FAILURE, payload: error });
    })
    .finally(() => Nprogress.done());
};

export const createManager = (vendorId, form) => (dispatch) => {
  dispatch({ type: types.CREATE_LOADING });

  sendRequest(`/vendors/${vendorId}/managers`, {
    method: "POST",
    data: JSON.stringify(form),
  })
      .then(({ data }) => {
        dispatch({ type: types.CREATE_SUCCESS, payload: data });
      })
      .catch(({ response }) => {
        dispatch({ type: types.CREATE_FAILURE, payload: response.data });
      });
};

export const reset = () => dispatch => dispatch({ type: types.RESET_CREATE_VENDOR });