import produce from "immer";
import * as types from "../constants";

const INITAL_STATE = {
  list: [],
  meta: {
    current_page: 1,
    per_page: 10
  },
  loading: true,
  error: {},
};

export const manager = produce((draft, action) => {
  switch (action.type) {
    case types.GET_MANAGERS:
      draft.loading = true;
      return;
    case types.GET_MANAGERS_SUCCESS:
      draft.list = action.payload.data;
      draft.meta = action.payload.meta;
      draft.loading = false;
      return;
    case types.GET_MANAGERS_FAILURE:
      draft.error = action.payload.error;
      draft.loading = false;
      return;
    case types.SET_PAGE:
      draft.meta.current_page = action.payload;
      return;
    case types.SET_PER_PAGE:
      draft.meta.per_page = action.payload;
      return;
  }
}, INITAL_STATE);
