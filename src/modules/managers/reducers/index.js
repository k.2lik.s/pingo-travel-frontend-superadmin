import { combineReducers } from "redux";
import { manager } from './manager';
import { vendor } from "./vendor";
import { create } from './create';

export default combineReducers({
    manager,
    vendor,
    create,
});
