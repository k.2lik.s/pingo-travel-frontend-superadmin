import produce from "immer";
import * as types from "../constants";

const INITAL_STATE = {
  list: [],
  loading: true,
  error: {},
};

export const vendor = produce((draft, action) => {
  switch (action.type) {
    case types.GET_VENDORS:
      draft.loading = true;
      return;
    case types.GET_VENDORS_SUCCESS:
      draft.list = action.payload.data;
      draft.meta = action.payload.meta;
      draft.loading = false;
      return;
    case types.GET_VENDORS_FAILURE:
      draft.error = action.payload.error;
      draft.loading = false;
      return;
  }
}, INITAL_STATE);
