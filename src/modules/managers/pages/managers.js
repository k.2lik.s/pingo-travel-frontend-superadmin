import {useState, useEffect, useCallback} from "react";
import { withRouter, useRouteMatch } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import {
  Table,
  TableBody,
  TableCell,
  TableHeader,
  TableRow as GTableRow,
  Text,
  CheckBox,
  Box,
  TextInput,
  Heading,
  Button,
  Grid,
  Select,
  Spinner
} from "grommet";
import {EditIcon, SortArrowIcon} from "../../../ui/Common/Icon";
import {getManagers, getVendors} from "../actions";
import ManagerItem from "../components/ManagerItem";
import { FormSelect } from "ui/Common/Form/Selects";
import Pagination from "ui/Common/Table/Pagination";
import * as types from "../constants";

const TableRow = styled(GTableRow)`
  background: ${(props) =>
    props.checked ? "rgba(47, 128, 237, 0.1)" : "#fff"};

  &:hover {
    background: rgba(47, 128, 237, 0.1);
    cursor: pointer;
  }
`;

const statusOptions = [
  {status: 'active', label: 'Активный'},
  {status: 'inactive', label: 'Не активный'},
];

const pageOptions = [10, 20, 30];

const TableHeadCell = ({label, onSort, children}) => {
  const [sort, setSort] = useState();
  const [asc, setAsc] = useState();
  const [desc, setDesc] = useState();

  const sortHandler = useCallback(() => {
    if (sort === 'asc') {
      setSort('desc');
      setDesc('#2F80ED');
      setAsc();
    } else {
      setSort('asc');
      setAsc('#2F80ED');
      setDesc();
    }

    onSort && onSort(sort);
  }, [sort, setSort]);

  return (
      <Grid
        style={{width: '100%'}}
        rows={['auto', 'auto']}
        columns={['auto', '30px']}
        gap={{row: '24px', column: '5px'}}
        areas={[
          {name: 'label', start: [0, 0], end: [0, 0]},
          {name: 'sort', start: [1, 0], end: [1, 0]},
          {name: 'filters', start: [0, 1], end: [1, 1]},
        ]}
      >
        <Text gridArea="label">{label}</Text>
        {onSort && <Box
          flex
          direction="row"
          justify="center"
          align="center"
          gridArea="sort"
          onClick={sortHandler}
        >
          <SortArrowIcon color={asc}/>
          <SortArrowIcon color={desc} inverted/>
        </Box>}
        <Box gridArea="filters" align="end">
          {children}
        </Box>
      </Grid>
  )
}


const Managers = ({ history }) => {
  const { manager, vendor } = useSelector((state) => state.manager);
  const dispatch = useDispatch();
  const [filters, setFilters] = useState({name: '', email: ''});
  const [currentVendor, setCurrentVendor] = useState(vendor?.list?.[0]?.id);

  const {current_page, per_page} = manager.meta;

  useEffect(() => {
    dispatch(getVendors());
    window.title = 'Менеджеры | Pingo travel';
  }, []);

  useEffect(() => {
    if (currentVendor) {
      dispatch(getManagers(currentVendor, {...filters, page: current_page, per_page}));
    } else if (vendor?.list[0]) {
      setCurrentVendor(vendor?.list[0].id);
    }
  }, [filters, currentVendor, vendor, current_page, per_page]);

  let { url } = useRouteMatch();


  return (
    <Box animation={[{ type: "fadeIn", duration: 300 }]}>
      <Box justify="between" direction="row">
        <Box direction="row" align="center" margin={{top: 'xsmall', bottom: 'medium'}}>
          <Heading size="large" level={2} margin="none">
            Менеджеры
          </Heading>
          <FormSelect
              margin={{left: 'medium'}}
              labelKey="name"
              valueKey="id"
              value={vendor?.list?.find(v => v.id === currentVendor)}
              options={vendor?.list}
              onChange={({ value }) => setCurrentVendor(value.id)}
          />
        </Box>
        <Box>
          <Button
            size="large"
            primary
            type="submit"
            label="Зарегистрировать менеджера"
            onClick={() => history.push(`${url}/create/information`)}
          ></Button>
        </Box>
      </Box>

      <Table style={{borderRadius: '10px', overflow: 'hidden'}}>
        <TableHeader>
          <TableRow>
            <TableCell
              style={{
                padding: '24px 0 0 50px',
                verticalAlign: 'top',
                borderRight: 'none'
              }}
              plain
            >
              <Text>№</Text>
            </TableCell>
            <TableCell style={{borderLeft: 'none'}} plain >
              <TableHeadCell label="Имя">
                <TextInput
                  onChange={e => setFilters({...filters, name: e.target.value})}
                  placeholder="Имя"
                  value={filters.name}
                />
              </TableHeadCell>
            </TableCell>
            <TableCell align="start">
              <TableHeadCell label="Фамилия">
                <TextInput
                  onChange={e => setFilters({...filters, name: e.target.value})}
                  placeholder="Фамилия"
                  value={filters.name}
                />
              </TableHeadCell>
            </TableCell>
            <TableCell align="start">
              <TableHeadCell label="E-mail">
                <TextInput
                  onChange={e => setFilters({...filters, email: e.target.value})}
                  placeholder="E-mail"
                  value={filters.email}
                />
              </TableHeadCell>
            </TableCell>
            {/*<TableCell style={{ borderLeft: 'none',  }} plain/>*/}
          </TableRow>
        </TableHeader>
        <TableBody>
          {
            vendor.loading && manager.loading ? (
              <GTableRow style={{ background: '#fff' }}>
                <TableCell align="center" colSpan={100}>
                  <Spinner />
                </TableCell>
              </GTableRow>
            ) : !(manager && manager.list && manager.list.length) ? (
              <GTableRow style={{ background: '#fff' }}>
                <TableCell align="center" colSpan={100}>
                  <Text>Отсутствуют записи</Text>
                </TableCell>
              </GTableRow>
            ): manager?.list
              ?.slice()
              .sort((v1, v2) => v1.id - v2.id)
              .map((item, i) => <ManagerItem key={item.id} vendor={item}/>)
          }
        </TableBody>
      </Table>
      <Box flex direction="row" justify="end" align="center" margin={{vertical: '20px'}}>
        <Box flex={{grow: 0, shrink: 1}} direction="row" align="center" width="fit-content">
          <span>Отображать</span>
          <FormSelect
            style={{width: '50px'}}
            margin={{horizontal: '10px'}}
            options={pageOptions}
            activeOptionIndex={pageOptions.indexOf(per_page) + 1}
            value={per_page}
            onChange={({option}) => dispatch({type: types.SET_PER_PAGE, payload: option})}
          />
          <span>записей на странице</span>
        </Box>
        <Pagination
          margin={{horizontal: '20px'}}
          numberItems={manager.meta.total}
          page={current_page}
          step={per_page}
          onChange={(e) => dispatch({type: types.SET_PAGE, payload: e.page})}
        />
      </Box>
    </Box>
  );
};

export default withRouter(Managers);
