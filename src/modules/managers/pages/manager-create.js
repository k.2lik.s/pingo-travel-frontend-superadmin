import { useState, useEffect } from "react";
import { connect } from "react-redux";
import { useParams, useHistory } from "react-router-dom";
import {
  Heading,
  Grid,
  Box,
  Button,
  FormField,
  CheckBoxGroup,
  CheckBox,
  Text,
  Form as GrommetForm, TextInput, TextArea,
} from "grommet";

import {
  getVendors,
  createManager,
  reset,
} from "../actions";
import { FormSelect } from "ui/Common/Form/Selects";
import { GrayButton } from "ui/Common/Form/Button";
import Navigation from "../components/Navigation";
import routes from "../../../routes";

const roleOptions = [
  {label: 'Администратор', value: 'admin'},
  {label: 'Менеджер', value: 'manager'},
];

const RoomCreate = ({
  getVendors,
  createManager,
  create,
  vendor,
  reset,
}) => {
  const { stepId } = useParams();

  console.log(stepId);
  const [form, setForm] = useState({
    name: "",
    surname: "",
    email: "",
    role: "",
  });
  const [vendorId, setVendorId] = useState();

  const errors = create.errors;

  useEffect(() => {
    getVendors();

    return () => {
      reset();
    };
  }, []);

  const history = useHistory();

  useEffect(() => {
    if (!create.nextStep) {
      history.push(`/admin/managers/create/information`);
    }
    if (create.nextStep) {
      history.push(`/admin/managers/create/vendor`);
    }
  }, [create]);

  const handleChange = (event, stateKey) => {
    if (stateKey === "information") {
      setForm({
        ...form,
        [event.target.name]: event.target.value,
      });
    }
  };

  const handleSubmit = () => {
    if (stepId === "information") {
      history.push(`/admin/managers/create/vendor`);
    }
    if (stepId === "vendor") {
      createManager(vendorId, form);
    }
  };

  return (
    <Box animation={[{ type: "fadeIn", duration: 300 }]}>
      <div className="grid-wrapper">
        <Heading size="large" level={2} margin={{ top: "xsmall" }}>
          Добавить агентство
        </Heading>

        <div className="grid-content">
          {stepId === 'information' ? (
            <Box
              animation={[{ type: "fadeIn", duration: 300 }]}
              height={{ min: "100%" }}
              background="#fff"
            >
              <Box pad="medium" border={{ side: "bottom", color: "#E0E0E0" }}>
                <Heading size="medium" margin="0" level={2}>
                  Основная информация
                </Heading>
              </Box>
              <div className="grid-wrapper--content">
                <div className="grid-form-layout">
                  <Box pad="medium">
                    <FormField
                      margin={{ top: "32px" }}
                      error={errors.name && errors.name[0]}
                      label={
                        <Text>
                          Имя
                          <Text color="red"> *</Text>
                        </Text>
                      }
                      htmlFor="name"
                    >
                      <TextInput
                        onChange={(e) => handleChange(e, "information")}
                        value={form.name}
                        name="name"
                        id="name"
                      />
                    </FormField>

                    <FormField
                      margin={{ top: "32px" }}
                      error={errors.surname && errors.surname[0]}
                      label={
                        <Text>
                          Фамилия
                          <Text color="red"> *</Text>
                        </Text>
                      }
                      htmlFor="surname"
                    >
                      <TextInput
                          onChange={(e) => handleChange(e, "information")}
                          value={form.surname}
                          name="surname"
                          id="surname"
                      />
                    </FormField>

                    <FormField
                        margin={{ top: "32px" }}
                        error={errors.email && errors.email[0]}
                        label={
                          <Text>
                            E-Mail
                            <Text color="red"> *</Text>
                          </Text>
                        }
                        htmlFor="email"
                    >
                      <TextInput
                          onChange={(e) => handleChange(e, "information")}
                          value={form.email}
                          name="email"
                          id="email"
                      />
                    </FormField>

                    <FormField
                        margin={{ top: "32px" }}
                        error={errors.role && errors.role[0]}
                        label={
                          <Text>
                            Роль
                            <Text color="red"> *</Text>
                          </Text>
                        }
                        htmlFor="role"
                    >
                      <FormSelect
                          id="role"
                          margin={{left: 'medium'}}
                          labelKey="label"
                          valueKey="value"
                          value={roleOptions.find(r => r.value === form.role)}
                          options={roleOptions}
                          onChange={({ value }) => setForm(value.value)}
                      />
                    </FormField>
                  </Box>
                </div>
              </div>
            </Box>
          ): stepId === 'vendor' ? (
            <Box
                animation={[{ type: "fadeIn", duration: 300 }]}
                height={{ min: "100%" }}
                background="#fff"
            >
              <Box pad="medium" border={{ side: "bottom", color: "#E0E0E0" }}>
                <Heading size="medium" margin="0" level={2}>
                  Основная информация
                </Heading>
              </Box>
              <div className="grid-wrapper--content">
                <div className="grid-form-layout">
                  <Box pad="medium">
                    <FormField
                        margin={{ top: "32px" }}
                        error={errors.name && errors.name[0]}
                        label={
                          <Text>
                            Агентство
                            <Text color="red"> *</Text>
                          </Text>
                        }
                        htmlFor="vendor"
                    >
                      <FormSelect
                        id="vendor"
                        margin={{left: 'medium'}}
                        labelKey="name"
                        valueKey="id"
                        value={vendor?.list?.find(v => v.id === vendorId)}
                        options={vendor?.list}
                        onChange={({ value }) => setVendorId(value.id)}
                      />
                    </FormField>
                  </Box>
                </div>
              </div>
            </Box>
          ): (<></>)}
        </div>
        <div className="grid-navigation">
          <Navigation
            infoSuccess={create.success}
            stepId={stepId}
          />
        </div>
        <div className="grid-wrapper">
          <div className="grid-content">
            <Box justify="between" direction="row" margin={{ top: "medium" }}>
              <Box width="48%">
                <GrayButton label="Вернуться" />
              </Box>
              <Box width="48%">
                <Button
                  onClick={() => handleSubmit()}
                  primary
                  disabled={create.loading}
                  label="Продолжить"
                />
              </Box>
            </Box>
          </div>
        </div>
      </div>
    </Box>
  );
};

const mapToStateProps = (state) => ({
  vendor: state.manager.vendor,
  create: state.vendor.create,
});

export default connect(mapToStateProps, {
  getVendors,
  createManager,
  reset,
})(RoomCreate);
