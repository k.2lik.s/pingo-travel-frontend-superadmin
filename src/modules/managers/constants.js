const prefix = `Managers`;


export const GET_MANAGERS = `${prefix}/GET_MANGERS`;
export const GET_MANAGERS_SUCCESS = `${prefix}/GET_MANAGERS_SUCCESS`;
export const GET_MANAGERS_FAILURE = `${prefix}/GET_MANAGERS_FAILURE`;

export const GET_VENDORS = `${prefix}/GET_VENDORS`;
export const GET_VENDORS_SUCCESS = `${prefix}/GET_VENDORS_SUCCESS`;
export const GET_VENDORS_FAILURE = `${prefix}/GET_VENDORS_FAILURE`;

export const SET_PAGE = `${prefix}/SET_PAGE`;
export const SET_PER_PAGE = `${prefix}/SET_PER_PAGE`;

export const CREATE_LOADING = `${prefix}/CREATE_LOADING`;
export const CREATE_SUCCESS = `${prefix}/CREATE_SUCCESS`;
export const CREATE_FAILURE = `${prefix}/CREATE_FAILURE`;

export const RESET_CREATE_VENDOR = `${prefix}/RESET_CREATE_VENDOR`;

